
package critvalplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationAction;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.utils.MDLog;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CritAnnotation extends NMAction implements AnnotationAction {
    
    private Element elm;
    private Element derReqElm;
    private Element taskElm;
    private String requiredVal;
    private String actualVal;
    private Project project;
    String[] options = { "OK", "Suggested Solution"};
    
    //Example of the action that can be performed on multiple targets.
    public CritAnnotation(Project project, Element element, Element dervReq, Element task, String currentTargetValue, String value) {
        super("PROBLEMATIC_ELEMENT", "Problem with criticality!", null);
        //mClassifier = classifier;
        elm = element;
        derReqElm = dervReq;
        taskElm = task;
        requiredVal = currentTargetValue;
        actualVal = value;
        this.project = project;
    }
    
    //Executes the action.
    //@param e event caused execution.
    public void actionPerformed(ActionEvent e) {
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        
        
        try {
            DiagramPresentationElement diagram = project.getActiveDiagram();

            if (diagram.getHumanType().equals("BPMN Process Diagram")) {
                //emfanizw minimata se kathe periptwsi
                messageFrame2("<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 1px;}</style><center><b>Problem with criticality!</b></center><br>"
                    + "<table><tr><th>Your task:</th><td>"
                    + taskElm.getHumanName()
                    + "</td></tr><tr><th>Your criticality:</th><td>"
                    + derReqElm.getHumanName()
                    + "</td></tr><tr><th>The problem:</th><td>"
                    + "Your desired criticality level<br/>" + "cannot be reached by<br/>" + "the current system configuration."
                    + "</td></tr></html>", 
                    "<html>Another system configuration should be chosen!<br/>"
                        + "Please <u>consult the system designer</u> to change the configuration.<br/>" 
                        + "This way, system components properties' will obtain different new values.<br/>"
                        + "These values may result to a better criticality level,<br/>"
                        + "leading to its satisfaction!</html>");
            }
            else {
                //emfanizw minimata se kathe periptwsi
                messageFrame("<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 1px;}</style><center><b>Problem with criticality verification!</b></center><br>"
                    + "<table><tr><th>System criticality:</th><td>" 
                    + elm.getHumanName() 
                    + "</td></tr><tr><th>Human criticality:</th><td>"
                    + derReqElm.getHumanName()
                    + "</td></tr><tr><th>Human task:</th><td>"
                    + taskElm.getHumanName()
                    + "</td></tr><tr><th>Desired level:</th><td>"
                    + requiredVal 
                    + "</td></tr><tr><th>Computed level:</th><td>"
                    + actualVal
                    + "</td></tr><tr><th>Detected problem:</th><td>"
                    + "Criticality level values are not equal!"
                    + "</td></tr></html>", 
                    "<html>Another system configuration should be chosen!<br/>"
                        + "Different components properties' values may<br/>"
                        + "result to equal or better level, leading to<br/>"
                        + "the criticalities verification!</html>");
            }
            
            
                
//            if ((satElm == null) && (derReqElm == null) && (taskElm == null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm == null) && (derReqElm == null) && (taskElm != null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + taskElm.getHumanName()
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm == null) && (derReqElm != null) && (taskElm == null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + derReqElm.getHumanName()
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm != null) && (derReqElm == null) && (taskElm != null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + taskElm.getHumanName()
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm == null) && (derReqElm != null) && (taskElm != null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + derReqElm.getHumanName()
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + taskElm.getHumanName()
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm != null) && (derReqElm != null) && (taskElm == null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + derReqElm.getHumanName()
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if((satElm != null) && (derReqElm == null) && (taskElm == null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + "<b>NOT CONNECTED</b>"
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
//            else if ((satElm != null) && (derReqElm != null) && (taskElm != null)) {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + derReqElm.getHumanName()
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + taskElm.getHumanName()
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>");
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }
            
//            if (flag) {
//                messageFrame("<html>You must validate the criticalities and re-verify them!<br/>Criticalities should be connected with corresponding model elements.</html>", true);
//            }
//            else {
//                int result = javax.swing.JOptionPane.showOptionDialog(null, 
//                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {text-align: left;padding: 2px;}</style><center><b>Problem with criticality verification!</b></center><br>"
//                            + "<table><tr><th>System criticality</th><td>" 
//                            + elm.getHumanName() 
//                            + "</td></tr><tr><th>Human criticality</th><td>"
//                            + derReqElm.getHumanName()
//                            + "</td></tr><tr><th>Human task</th><td>"
//                            + taskElm.getHumanName()
//                            + "</td></tr><tr><th>Desired level</th><td>"
//                            + requiredVal 
//                            + "</td></tr><tr><th>Computed level</th><td>"
//                            + actualVal
//                            + "</td></tr></html>", "Criticality verification - Details",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
//                //otan patithei to suggest solution button
//                if (result == JOptionPane.NO_OPTION) {
//                    messageFrame("<html>You can choose another system configuration!<br/>Different components properties' values<br/>may result to equal or better level,<br/>leading to the criticality verification!</html>", false);
//                    //Application.getInstance().getGUILog().log("You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
//                }
//            }

            
        }
        catch (Exception exc) {
            MDLog.getGUILog().error("", exc);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }
    
    //Executes the action on specified targets.
    //@param annotations action targets.
    public void execute(Collection<Annotation> annotations) {
        if (annotations == null || annotations.isEmpty()) {
            return;
        }
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        try {
        }
        catch (Exception e) {
            MDLog.getGUILog().error("", e);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }
    
    public boolean canExecute(Collection<Annotation> annotations) {
        return true;
    }
    //methodos gia na emfanizontai ta minimata sto sxediasti
    void messageFrame (String message, String solutionMessage) {
        //create frame
        final JFrame jFrame = new JFrame();
        JPanel jpn = new JPanel();
        jpn.setLayout(new BoxLayout(jpn, BoxLayout.PAGE_AXIS));
        jpn.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn1 = new JPanel();
        JPanel jpn2 = new JPanel();
        JPanel jpn3 = new JPanel();

        jpn1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Recommended solution"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf = new JLabel(message);
        jpn1.add(inf);
        
        JLabel solution = new JLabel(solutionMessage);
        jpn2.add(solution);

        JButton jpFin = new JButton("OK");
        jpFin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false);
            }
        });
        jpn3.add(jpFin);

        jpn.add(jpn1);
        jpn.add(jpn2);
        jpn.add(jpn3);

        //if parameter is isVal, then show respective message
        javax.swing.JOptionPane.showOptionDialog(jFrame, jpn, "Problem with criticality - Details", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
    
    void messageFrame2 (String message, String solutionMessage) {
        //create frame
        final JFrame jFrame = new JFrame();
        JPanel jpn = new JPanel();
        jpn.setLayout(new BoxLayout(jpn, BoxLayout.PAGE_AXIS));
        jpn.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn1 = new JPanel();
        JPanel jpn2 = new JPanel();
        JPanel jpn3 = new JPanel();

        jpn1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("What happened?"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("What can I do?"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(""), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf = new JLabel(message);
        jpn1.add(inf);
        
        JLabel solution = new JLabel(solutionMessage);
        jpn2.add(solution);

        JButton jpFin = new JButton("OK");
        jpFin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false);
            }
        });
        jpn3.add(jpFin);

        jpn.add(jpn1);
        jpn.add(jpn2);
        jpn.add(jpn3);

        //if parameter is isVal, then show respective message
        javax.swing.JOptionPane.showOptionDialog(jFrame, jpn, "Details", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
}

