
package critvalplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.uml.valueprovider.SmartListenerConfigurationProvider;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralString;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.PropertyNames;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CritValPlugin implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    private List<NMAction> actions;
   
    public CritValPlugin() {
    }

    @Override
    public void init(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint) {
    }
    
    //Returns a map of classes and smart listener configurations.
    //@return smart listener configurations.
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        SmartListenerConfig config = new SmartListenerConfig();
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        return configMap;
    }

    //Executes the rule.
    //@param project a project of the constraint.
    //@param constraint constraint which defines validation rules.
    //@param elements collection of elements that have to be validated.
    //@return a set of <code>Annotation</code> objects which specifies invalid objects.
    @Override
    public Set<Annotation> run(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint, Collection<? extends com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element> elements) {
     
        Set<Annotation> result = new HashSet<Annotation>();
        
        for (Element element : elements) {
            String currentTargetValue = null;
            Element verificationRequirementDataElement;
            Element derivedRequirementElement = null;
            Abstraction verify = null;
            Abstraction satisfy = null;
            Abstraction derive = null;
            String[] ownedPropertyName;
            String value = null;
            Property property = null;
            Element task = null;
            Abstraction absSat = null;
            Stereotype verifyStereotype = StereotypesHelper.getStereotype(project, "Verify");
            Stereotype satisfyStereotype = StereotypesHelper.getStereotype(project, "Satisfy");
            Stereotype deriveStereotype = StereotypesHelper.getStereotype(project, "DeriveReqt");
            List<String> tagList = null;
            Collection<DirectedRelationship> dirRel = element.get_directedRelationshipOfTarget();
            Collection<DirectedRelationship> dirSatRel = element.get_directedRelationshipOfTarget();
            Collection<DirectedRelationship> dirDervRel = element.get_directedRelationshipOfTarget();
            boolean hasSat = false;
            boolean hasDer = false;
            boolean hasDerReq = false;
            boolean hasTask = false;
            boolean hasCurVal = false;
            boolean hasVer = false;
            boolean hasVerData = false;
            boolean hasVal = false;
            
            //pairnw satisfy connection
            for (java.util.Iterator dirSatRelIt = dirSatRel.iterator(); dirSatRelIt.hasNext();) {
                Element satRel = (Element) dirSatRelIt.next(); 

                if (satRel.getHumanName().equals(satisfyStereotype.getName())) {
                    satisfy = (Abstraction) satRel;
                    hasSat = true;
                }
            }
            
            for (java.util.Iterator directedDeriveRelationIt = dirDervRel.iterator(); directedDeriveRelationIt.hasNext();) {
                Element deriveRelation = (Element) directedDeriveRelationIt.next(); 
                //an vrethei derive stereotype
                if (deriveRelation.getHumanName().equals(deriveStereotype.getName())) {
                    derive = (Abstraction) deriveRelation;
                    hasDer = true;
                    //vriskw to derived requirement
                    derivedRequirementElement = derive.getSource().iterator().next();
                    hasDerReq = true;
                    //relations of derived requirement
                    Collection<DirectedRelationship> dirRelsIn = derivedRequirementElement.get_directedRelationshipOfTarget();
 
                    for (java.util.Iterator dirRelInIt = dirRelsIn.iterator(); dirRelInIt.hasNext();) {
                        Element relIn = (Element) dirRelInIt.next();

                        if (relIn.getHumanName().equals(satisfyStereotype.getName())) {
                            absSat = (Abstraction) relIn;
                            //find connected task
                            task = absSat.getSource().iterator().next();
                            hasTask = true;
                        }
                    }
                }
            }
            Collection<Stereotype> stereotypes = StereotypesHelper.getStereotypesHierarchy(element);
                    
            for (Stereotype parentSter : stereotypes) {
                //pare to levels value apo to gradedquantreq
                if (parentSter.getName().equals("GradedQuantReq")) {
                    tagList = StereotypesHelper.getStereotypePropertyValue(element, parentSter, "levels");
                    break;
                }
            }
            
            for (String tag : tagList) {
                currentTargetValue = tag;
                hasCurVal = true;
            }
            
            for (java.util.Iterator dirRelIt = dirRel.iterator(); dirRelIt.hasNext();) {
                Element verRel = (Element) dirRelIt.next(); 
                
                if (verRel.getHumanName().equals(verifyStereotype.getName())) {
                    verify = (Abstraction) verRel;
                    hasVer = true;
                    verificationRequirementDataElement = verify.getSource().iterator().next();
                    hasVerData = true;
                    Collection<Element> ownedProperties = verificationRequirementDataElement.getOwnedElement();
                    
                    for (Element ownedProperty : ownedProperties) {
                        ownedPropertyName = ownedProperty.getHumanName().split(" ");
                        
                        if (ownedProperty.getHumanType().equals("Value Property")) {
                            
                            if (ownedPropertyName[2].equals("output")) {
                                property = (Property) ownedProperty;
                                
                                if (property.getDefaultValue() != null) {
                                    ValueSpecification valueSpecificationValue = property.getDefaultValue();
                                    value = ((LiteralString) valueSpecificationValue).getValue();
                                    
                                    if (value != null) {
                                        hasVal = true;
                                    
                                        if (hasSat && hasDer && hasDerReq && hasTask && hasCurVal && hasVer && hasVerData && hasVal) {
                                            
                                            if (!currentTargetValue.equals(value)) {
                                                actions = new ArrayList<NMAction>();
                                                NMAction critAnnotation = new CritAnnotation(project, element, derivedRequirementElement, task, currentTargetValue, value);
                                                actions.add(critAnnotation);
                                                Annotation critAnnAction = new Annotation(element, constraint, actions);
                                                Annotation critAnnAction2 = new Annotation(satisfy, constraint, actions);
                                                Annotation critAnnAction3 = new Annotation(derivedRequirementElement, constraint, actions);
                                                Annotation critAnnAction4 = new Annotation(derive, constraint, actions);
                                                Annotation critAnnAction5 = new Annotation(task, constraint, actions);
                                                Annotation critAnnAction6 = new Annotation(property, constraint, actions);
                                                result.add(critAnnAction);
                                                result.add(critAnnAction2);
                                                result.add(critAnnAction3);
                                                result.add(critAnnAction4);
                                                result.add(critAnnAction5);
                                                result.add(critAnnAction6);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
//            Collection<Stereotype> stereotypes = StereotypesHelper.getStereotypesHierarchy(element);
//            //pare to levels value apo to gradedquantreq
//            for (Stereotype parentSter : stereotypes) {
//                
//                if (parentSter.getName().equals("GradedQuantReq")) {
//                    tagList = StereotypesHelper.getStereotypePropertyValue(element, parentSter, "levels");
//                    break;
//                }
//            }
//            
//            for (String tag : tagList) {
//                currentTargetValue = tag;
//            }
//            
//            for (java.util.Iterator relsIt = dirRel.iterator(); relsIt.hasNext();) {
//                Element rel = (Element) relsIt.next();   
//                //pairnw satisfy connection
//                if (rel.getHumanName().equals(satisfySter.getName())) {
//                    isSat = true;
//                    satisfy = (Abstraction) rel;
//                }
//                //pairnw verify connection 
//                if (rel.getHumanName().equals(verifySter.getName())) {
//                    isVer = true;
//                }
//                //pairnw derive connection kai task
//                if (rel.getHumanName().equals(deriveSter.getName())) {
//                    isDer = true;
//                    derive = (Abstraction) rel;
//                    //vriskw to derived requirement
//                    derivedRequirementElement = derive.getSource().iterator().next();
//                    //relations of derived requirement
//                    Collection<DirectedRelationship> dirRelsIn = derivedRequirementElement.get_directedRelationshipOfTarget();
//
//                    for (java.util.Iterator dirRelInIt = dirRelsIn.iterator(); dirRelInIt.hasNext();) {
//                        Element relIn = (Element) dirRelInIt.next();
//
//                        if (relIn.getHumanName().equals(satisfyStereotype.getName())) {
//                            absSat = (Abstraction) relIn;
//                            //find connected task
//                            task = absSat.getSource().iterator().next();
//                        }
//                    }
//                }
//
//                if (rel.getHumanName().equals(refineSter.getName())) {
//                    Abstraction refAbs = (Abstraction) rel;
//                    tmpElm = refAbs.getSource().iterator().next();
//                    String[] tmpElmName = tmpElm.getHumanName().split(" ", 2);
//
//                    if (tmpElmName[0].equals("VerificationCritFormula")) {
//                        isRef = true;
//                    }
//                }
//            }
//            
//            if (!isSat || !isVer || !isDer || !isRef) {
//                flag = true;
//                actions = new ArrayList<NMAction>();
//                NMAction critAnnotation = new CritAnnotation(element, null, null, null, null, null, flag);
//                actions.add(critAnnotation);
//                Annotation critAnnAction = new Annotation(element, constraint, actions);
//                result.add(critAnnAction);
//            }
//            else if (isSat && isVer && isDer && isRef) {
//                
//                for (java.util.Iterator dirRelIt = dirRel.iterator(); dirRelIt.hasNext();) {
//                    Element verRel = (Element) dirRelIt.next(); 
//
//                    if (verRel.getHumanName().equals(verifyStereotype.getName())) {
//                        verify = (Abstraction) verRel;
//                        verificationRequirementDataElement = verify.getSource().iterator().next();
//                        Collection<Element> ownedProperties = verificationRequirementDataElement.getOwnedElement();
//
//                        for (Element ownedProperty : ownedProperties) {
//                            ownedPropertyName = ownedProperty.getHumanName().split(" ");
//
//                            if (ownedProperty.getHumanType().equals("Value Property")) {
//
//                                if (ownedPropertyName[2].equals("output")) {
//                                    property = (Property) ownedProperty;
//
//                                    if (property.getDefaultValue() != null) {
//                                        ValueSpecification valueSpecificationValue = property.getDefaultValue();
//                                        value = ((LiteralString) valueSpecificationValue).getValue();
//
//                                        if (value != null) {
//                                            
//                                            if (!currentTargetValue.equals(value)) {
//                                                flag = false;
//                                                actions = new ArrayList<NMAction>();
//                                                NMAction critAnnotation = new CritAnnotation(element, derivedRequirementElement, task, satisfy, currentTargetValue, value, flag);
//                                                actions.add(critAnnotation);
//                                                Annotation critAnnAction = new Annotation(element, constraint, actions);
//                                                Annotation critAnnAction2 = new Annotation(satisfy, constraint, actions);
//                                                Annotation critAnnAction3 = new Annotation(derivedRequirementElement, constraint, actions);
//                                                Annotation critAnnAction4 = new Annotation(task, constraint, actions);
//                                                result.add(critAnnAction);
//                                                result.add(critAnnAction2);
//                                                result.add(critAnnAction3);
//                                                result.add(critAnnAction4);
//                                                break;
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }


/*for (Element element : elements) {  
            
            String currentTargetValue = null;
            Element verificationRequirementDataElement;
            Element derivedRequirementElement = null;
            Element costBlck;
            Element form;
            Element data;
            Abstraction verify = null;
            Abstraction refine = null;
            Abstraction satisfy = null;
            Abstraction derive = null;
            String[] ownedPropName;
            String value = null;
            Property property = null;
            Element task = null;
            Abstraction absSat = null;
            Stereotype verifyStereotype = StereotypesHelper.getStereotype(project, "Verify");
            Stereotype refineStereotype = StereotypesHelper.getStereotype(project, "Refine");
            Stereotype satisfyStereotype = StereotypesHelper.getStereotype(project, "Satisfy");
            List<String> tagList = null;
            Collection<DirectedRelationship> dirRel = element.get_directedRelationshipOfTarget();
            Collection<DirectedRelationship> dirSatRel = element.get_directedRelationshipOfTarget();
            boolean hasSat = false;
            boolean hasDer = false;
            boolean hasDerReq = false;
            boolean hasTask = false;
            boolean hasCurVal = false;
            boolean hasVer = false;
            boolean hasVerData = false;
            boolean hasVal = false;
            boolean hasRef = false;
            
            //pairnw satisfy connection
            for (java.util.Iterator dirSatRelIt = dirSatRel.iterator(); dirSatRelIt.hasNext();) {
                Element satRel = (Element) dirSatRelIt.next(); 

                if (satRel.getHumanName().equals(satisfyStereotype.getName())) {
                    satisfy = (Abstraction) satRel;
                    hasSat = true;
                    costBlck = satisfy.getSource().iterator().next();
                    Collection<Element> ownedProps = costBlck.getOwnedElement();
                    
                    for (Element ownedProp : ownedProps) {
                        javax.swing.JOptionPane.showMessageDialog(null, "cost block's : " + ownedProp.getHumanName());
                    }
                }
            }
            
            for (java.util.Iterator dirSatRelIt = dirSatRel.iterator(); dirSatRelIt.hasNext();) {
                Element refRel = (Element) dirSatRelIt.next(); 

                if (refRel.getHumanName().equals(refineStereotype.getName())) {
                    refine = (Abstraction) refRel;
                    hasRef = true;
                    form = verify.getSource().iterator().next();
                    Collection<Element> ownedProps2 = form.getOwnedElement();
                    
                    for (Element ownedProp2 : ownedProps2) {
                        javax.swing.JOptionPane.showMessageDialog(null, "formula's : " + ownedProp2.getHumanName());
                    }
                }
            }
            
            for (java.util.Iterator dirSatRelIt = dirSatRel.iterator(); dirSatRelIt.hasNext();) {
                Element verRel = (Element) dirSatRelIt.next(); 

                if (verRel.getHumanName().equals(verifyStereotype.getName())) {
                    verify = (Abstraction) verRel;
                    hasVer = true;
                    data = verify.getSource().iterator().next();
                    Collection<Element> ownedProps3 = data.getOwnedElement();
                    
                    for (Element ownedProp3 : ownedProps3) {
                        javax.swing.JOptionPane.showMessageDialog(null, "data's : " + ownedProp3.getHumanName());
                    }
                }
            }*/



        }
        return result;
    }

    public void dispose() {
    }
}