
package critcheckplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.uml.valueprovider.SmartListenerConfigurationProvider;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.PropertyNames;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CritCheckPlugin implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    private List<NMAction> actions;
   
    public CritCheckPlugin() {
    }

    @Override
    public void init(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint) {
    }
    
    //Returns a map of classes and smart listener configurations.
    //@return smart listener configurations.
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        SmartListenerConfig config = new SmartListenerConfig();
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        return configMap;
    }

    //Executes the rule.
    //@param project a project of the constraint.
    //@param constraint constraint which defines validation rules.
    //@param elements collection of elements that have to be validated.
    //@return a set of <code>Annotation</code> objects which specifies invalid objects.
    @Override
    public Set<Annotation> run(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint, Collection<? extends com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element> elements) {
        Set<Annotation> result = new HashSet<Annotation>();
        
        for (Element element : elements) {  
            Collection<DirectedRelationship> dirRel = element.get_directedRelationshipOfTarget();
            Stereotype verifySter = StereotypesHelper.getStereotype(project, "Verify");
            Stereotype satisfySter = StereotypesHelper.getStereotype(project, "Satisfy");
            Stereotype refineSter = StereotypesHelper.getStereotype(project, "Refine");
            Stereotype deriveSter = StereotypesHelper.getStereotype(project, "DeriveReqt");
            Collection<Stereotype> sterHier = StereotypesHelper.getStereotypesHierarchy(element);
            Collection<Stereotype> listSter;
            Element tmpElm;
            boolean isSat = false;
            boolean isSat2 = false;
            boolean isGen = false;
            boolean isVer = false;
            boolean isRef = false;
            boolean isDer = false;
            boolean isHuman = false;
            int flag = 0;
            int flag2 = 0;
            
            listSter = StereotypesHelper.getStereotypes(element);
            //vriskw stereotype hierarchy gia na dw an einai human criticality
            for (Stereotype ster : sterHier) {
                String[] sterHierName = ster.getHumanName().split(" ", 2);
                //an einai vgainw apo to loop
                if (sterHierName[1].equals("HumanCriticality")) {
                    isHuman = true;
                    break;
                }
            }
            //an einai human criticality
            if (isHuman) {
                //vriskw tis sindeseis pou tou erxontai, dld satisfy kai generate kai vlepw an einai true ta flags tous
                for (java.util.Iterator relsIt = dirRel.iterator(); relsIt.hasNext();) {
                    Element rel = (Element) relsIt.next(); 

                    if (rel.getHumanName().equals(satisfySter.getName())) {
                        isSat2 = true;
                    }
                    Stereotype sterGen = StereotypesHelper.getAppliedStereotypeByString(rel, "generate");
                    
                    if (sterGen != null) {
                        isGen = true;
                    }
                }
                //pairnw periptwseis pou kapoio den einai true, dld den iparxei sindesi
                if (isSat2 && !isGen) {
                    flag2 = 1;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag2, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat2 && isGen) {
                    flag2 = 2;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag2, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat2 && !isGen) {
                    flag2 = 3;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag2, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                isSat2 = false;
                isGen = false;
            }
            //an den einai human criticality
            if (!isHuman) {
                //vriskw tis sindeseis pou tou erxontai, dld satisfy, refine, derive kai verify kai vlepw an einai true ta flags tous
                for (java.util.Iterator relsIt = dirRel.iterator(); relsIt.hasNext();) {
                    Element rel = (Element) relsIt.next();   

                    if (rel.getHumanName().equals(satisfySter.getName())) {
                        isSat = true;
                    }

                    if (rel.getHumanName().equals(verifySter.getName())) {
                        isVer = true;
                    }
                    
                    if (rel.getHumanName().equals(deriveSter.getName())) {
                        isDer = true;
                    }

                    if (rel.getHumanName().equals(refineSter.getName())) {
                        Abstraction refAbs = (Abstraction) rel;
                        tmpElm = refAbs.getSource().iterator().next();
                        String[] tmpElmName = tmpElm.getHumanName().split(" ", 2);
                        
                        if (tmpElmName[0].equals("VerificationCritFormula")) {
                            isRef = true;
                        }
                    }
                }
                //pairnw periptwseis pou kapoio den einai true, dld den iparxei sindesi
                if (isSat && isVer && isRef && !isDer) { 
                    flag = 1;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && isVer && !isRef && isDer) { 
                    flag = 2;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && !isVer && isRef && isDer) { 
                    flag = 3;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && isVer && isRef && isDer) {
                    flag = 4;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && isVer && !isRef && !isDer) { 
                    flag = 5;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && !isVer && !isRef && isDer) { 
                    flag = 6;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && !isVer && isRef && !isDer) { 
                    flag = 7;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && isVer && !isRef && isDer) { 
                    flag = 8;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && isVer && isRef && !isDer) { 
                    flag = 9;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && !isVer && isRef && isDer) { 
                    flag = 10;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && !isVer && !isRef && isDer) { 
                    flag = 11;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && !isVer && isRef && !isDer) { 
                    flag = 12;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && isVer && !isRef && !isDer) { 
                    flag = 13;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (isSat && !isVer && !isRef && !isDer) { 
                    flag = 14;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                else if (!isSat && !isVer && !isRef && !isDer) { 
                    flag = 15;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element,flag, isHuman);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                }
                isSat = false;
                isVer = false;
                isRef = false;
            }
            isHuman = false;
        }
        return result;
    }

    public void dispose() {
    }
}
