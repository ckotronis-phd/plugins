
package humancritcheckplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.uml.valueprovider.SmartListenerConfigurationProvider;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.PropertyNames;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HumanCritCheckPlugin implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    private List<NMAction> actions;
   
    public HumanCritCheckPlugin() {
    }

    @Override
    public void init(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint) {
    }
    
    //Returns a map of classes and smart listener configurations.
    //@return smart listener configurations.
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        SmartListenerConfig config = new SmartListenerConfig();
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        return configMap;
    }

    //Executes the rule.
    //@param project a project of the constraint.
    //@param constraint constraint which defines validation rules.
    //@param elements collection of elements that have to be validated.
    //@return a set of <code>Annotation</code> objects which specifies invalid objects.
    @Override
    public Set<Annotation> run(Project project, com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint constraint, Collection<? extends com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element> elements) {
     
        Set<Annotation> result = new HashSet<Annotation>();
        ArrayList<Boolean> critsList = new ArrayList<Boolean>();
        boolean foundSystemCrit = false;
        
        for (Element element : elements) { 
            Abstraction satisfy = null;
            Abstraction generate = null; 
            Stereotype satisfySter = StereotypesHelper.getStereotype(project, "Satisfy");
            Collection<DirectedRelationship> dirRel = element.get_directedRelationshipOfTarget();
            boolean isSat = false;
            boolean isGen = false;
            int flag = 0;
            Collection<Stereotype> sterHier = StereotypesHelper.getStereotypesHierarchy(element);
            Collection<Element> colEl;
            Collection<Stereotype> listSter;
            String[] sterName = null;
            boolean isHuman = false;
            Element tmpElm;
             
//            listSter = StereotypesHelper.getStereotypes(element);
//                        
////            for (Stereotype st : listSter) {
////                sterName = st.getHumanName().split(" ");
////            }
//            
//            //javax.swing.JOptionPane.showMessageDialog(null, sterName[1]);
//            
//            for (Stereotype ster : sterHier) {
//                String[] sterHierName = ster.getHumanName().split(" ", 2);
//                            
//                if (sterHierName[1].equals("HumanCriticality")) {
//                    //javax.swing.JOptionPane.showMessageDialog(null, "einai human to crit" + sterHierName[1]);
//                    isHuman = true;
//                    break;
//                }
//            }
//            critsList.add(isHuman);

            //if (!isHuman) {
                for (java.util.Iterator relsIt = dirRel.iterator(); relsIt.hasNext();) {
                    Element rel = (Element) relsIt.next(); 

                    if (rel.getHumanName().equals(satisfySter.getName())) {
                        isSat = true;
                    }

                    Stereotype sterGen = StereotypesHelper.getAppliedStereotypeByString(rel, "generate");
                    
                    if (sterGen != null) {
                        isGen = true;
                    }
                }

//                if (isSat && isVer && isRef) { //&& hasValue) {//&& isRefCrit) {
//                    javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " ola ok!");
//                }
//                else 
                if (isSat && !isGen) { //&& hasValue) {// && isRefCrit) {
                    flag = 1;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " needs a refine!");
                }
                else if (!isSat && isGen) { //&& hasValue) {// && isRefCrit) { 
                    flag = 2;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " needs a satisfy!");
                }
                else if (!isSat && !isGen) { //&& !hasValue) {// && isRefCrit) {
                    flag = 3;
                    actions = new ArrayList<NMAction>();
                    NMAction critAnnotation = new CritAnnotation(element, flag);
                    actions.add(critAnnotation);
                    Annotation critAnnAction = new Annotation(element, constraint, actions);
                    result.add(critAnnAction);
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " needs a verify and a satisfy!");
                }
                isSat = false;
                isGen = false;
            }
           // isHuman = false;

       // }
        //==========================================================================================================================
//        for (int i = 0; i<critsList.size(); i++) {
//            //javax.swing.JOptionPane.showMessageDialog(null, critsList.get(i));
//
//            if (critsList.get(i) == false) {
//                foundSystemCrit = true;
//                break;
//            }
//        }
//        
//        if (foundSystemCrit) {
//            ;;
//        }
//        else {
//            javax.swing.JOptionPane.showMessageDialog(null, "den exw system");
//        }
//        foundSystemCrit = false;
        
        return result;
        
//                    Collection<DiagramPresentationElement> dpes = prjct.getDiagrams();
//                    for (DiagramPresentationElement dpe : dpes) {
//                        //javax.swing.JOptionPane.showMessageDialog(null, dpe.getElement().getOwner().getHumanName() + " | " + dpe.getElement().getOwner().getHumanType());
//                        if (dpe.getElement().getOwner().getHumanType().equals("BPMN Process")) {
//                            Collection<Element> dpees = dpe.getElement().getOwner().getOwnedElement();
//                            for (Element dpee : dpees) {
//                                //javax.swing.JOptionPane.showMessageDialog(null, dpee.getHumanName() + " | " + dpee.getHumanType());
//                                if (dpee.getHumanType().equals("Task")) {
//                                    javax.swing.JOptionPane.showMessageDialog(null, dpee.getHumanName());
//                                    String[] taskName = dpee.getHumanName().split(" ", 2);
    }

    public void dispose() {
    }
}
