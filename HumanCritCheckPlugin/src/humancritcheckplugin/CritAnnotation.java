
package humancritcheckplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationAction;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.utils.MDLog;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CritAnnotation extends NMAction implements AnnotationAction {
    
    private Element elm;
    private int flag;
    //String[] options = { "OK", "Suggested Solution"};
    
    //Example of the action that can be performed on multiple targets.
    public CritAnnotation(Element element, int flag) {
        super("PROBLEMATIC_ELEMENT", "Warning for human criticality validation!", null);
        //mClassifier = classifier;
        elm = element;
        this.flag = flag;
    }
    
    //Executes the action.
    //@param e event caused execution.
    public void actionPerformed(ActionEvent e) {
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        
        try {
            switch (this.flag) {
                case 1:
                    messageFrame("<html>Element \"" + elm.getHumanName() + "\" is missing <b>\"Generate\"</b> relationship!<br/>It should be generated by a Concern element.</html>");
                    break;
                case 2:
                    messageFrame("<html>Element \"" + elm.getHumanName() + "\" is missing <b>\"Satisfy\"</b> relationship!<br/>It should be satisfied by a Task element.</html>");
                    break;
                case 3:
                    messageFrame("<html>Element \"" + elm.getHumanName() + "\" is missing <b>\"Generate\"</b> and <b>\"Satisfy\"</b> relationships!<br/>It should be generated by a Concern element and satisfied by a Task element.</html>");
                    break;
            }
            //javax.swing.JOptionPane.showMessageDialog(null, "You can choose another system configuration! Different components properties' values may result to equal or better level, leading to the criticality verification!");
        }
        catch (Exception exc) {
            MDLog.getGUILog().error("", exc);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }
    
    //Executes the action on specified targets.
    //@param annotations action targets.
    public void execute(Collection<Annotation> annotations) {
        if (annotations == null || annotations.isEmpty()) {
            return;
        }
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        try {
        }
        catch (Exception e) {
            MDLog.getGUILog().error("", e);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }
    
    public boolean canExecute(Collection<Annotation> annotations) {
        return true;
    }
    
    void messageFrame (String message) {
        final JFrame fr1 = new JFrame();
        JPanel jpn1 = new JPanel();
        jpn1.setLayout(new BoxLayout(jpn1, BoxLayout.PAGE_AXIS));
        jpn1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn11 = new JPanel();
        JPanel jpn61 = new JPanel();

        jpn11.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn61.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf1 = new JLabel(message);
        inf1.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
        inf1.setIconTextGap(10);
        jpn11.add(inf1);

        JButton jpFin1 = new JButton("OK");
        jpFin1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fr1.setVisible(false);
            }
        });
        jpn61.add(jpFin1);

        jpn1.add(jpn11);
        jpn1.add(jpn61);

        javax.swing.JOptionPane.showOptionDialog(fr1, jpn1, "Criticality connections error", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
}

