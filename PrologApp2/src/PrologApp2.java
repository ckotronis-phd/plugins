
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jpl7.Atom;
import org.jpl7.Compound;
import org.jpl7.Query;
import org.jpl7.Term;
import org.jpl7.Variable;

public class PrologApp2 {

    private static String ruleFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_rules.pl";
    private static String questionFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_question.pl";
    private static String resultsFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_results.pl";
    
    public static void main(String[] args) throws FileNotFoundException {
        
        String question = "";
        File questionFile = new File(questionFileName);
        Scanner fileReader = new Scanner(questionFile);
        while (fileReader.hasNextLine()) {
          question = fileReader.nextLine();
        }
        fileReader.close();
        
        File resultsFile = new File(resultsFileName);
        try {
            resultsFile.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(PrologApp2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FileWriter resultsFileWriter = null;
        try {
            resultsFileWriter = new FileWriter(resultsFile);
        } catch (IOException ex) {
            Logger.getLogger(PrologApp2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String rules = "consult(" + "'" + ruleFileName  + "')";
        Query ruleQuery = new Query(rules);
        System.out.println(rules + " " + (ruleQuery.hasSolution() ? "succeeded" : "failed"));
//        String question = "configure([A, B, C, D], 'shortly', 'occasionally', 'expensive', 'real-time', 'best_effort', 'best_effort', 'weak')";
        Query questionQuery = new Query(question);
        String resultsOutput = "";
        if (questionQuery.hasSolution()) {
            Map<String, Term> responseMap = questionQuery.oneSolution();
            int responseCount = responseMap.size();
            for (String key: responseMap.keySet()) {
                String value = responseMap.get(key).toString();
                value = value.replace("(", ",").replace(")", "");
                String[] elements = value.split(",");
                String blockName = elements[0];
                resultsOutput += blockName + "(";
                String propValue = "";
                for (int propIndex=1; propIndex<elements.length; propIndex++) {
                    propValue = elements[propIndex];
                    propValue = propValue.replace(" ", "");
                    if (propValue.startsWith("_")) {
                        propValue = "NA";
                    }
                    resultsOutput += propValue;
                    if (propIndex < elements.length - 1) {
                        resultsOutput += ", ";
                    }
                }
                resultsOutput += ")";
                responseCount--;
                if (responseCount > 0) {
                    resultsOutput += "\n";
                }
            }
        }
        System.out.println(resultsOutput);
        try {
            resultsFileWriter.write(resultsOutput);
        } catch (IOException ex) {
            Logger.getLogger(PrologApp2.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            resultsFileWriter.flush();
            resultsFileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(PrologApp2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
