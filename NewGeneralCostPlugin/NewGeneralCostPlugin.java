
package newgeneralcostplugin;

import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.DiagramContextAMConfigurator;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.actions.DefaultDiagramAction;
import com.nomagic.magicdraw.uml.DiagramTypeConstants;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Association;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import java.util.*;
import java.awt.event.*;
import com.nomagic.magicdraw.uml.symbols.*;
import com.nomagic.magicdraw.uml.symbols.paths.PathElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Relationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification;
import com.nomagic.uml2.impl.ElementsFactory;
import java.awt.Rectangle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class NewGeneralCostPlugin extends com.nomagic.magicdraw.plugins.Plugin {               

    private DefaultDiagramAction validateCost;
    private DefaultDiagramAction computeCost;
    
    ArrayList<Element> visitedNodes;
    ArrayList<Element> computedNodes;
    
    public void init() {
        ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance();    
        
        validateCost = new DefaultDiagramAction("Validate", "Validate", null, null) {
            public void actionPerformed(ActionEvent e) { 
                SessionManager.getInstance().createSession("validate");                
                Project project = Application.getInstance().getProject();
                Profile costProfile = (Profile) StereotypesHelper.getProfile(project, "CostProfile");
                Diagram diagram = project.getActiveDiagram().getDiagram();   
                Boolean foundBlock = false;
                String[] blockElementName = null;
                String[] connectedCostElementStereotypeName = null;
                visitedNodes = new ArrayList<Element>();

                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {  
                        //pairnw to pressed (clicked by designer) cost element kai to onoma tou
                        Element costElement = presentationElement.getElement();
                        String[] costElementName = costElement.getHumanName().split(" ", 2);
                        //pairnw to stereotupo tou cost element + onoma stereotupou + ola ta stereotupa sto profle
                        Stereotype costElementStereotype = StereotypesHelper.getStereotypes(costElement).get(0);
                        String[] costElementStereotypeName = costElementStereotype.getHumanName().split(" ", 2);
                        //pairnw ta stereotupa pou exei to profile
                        java.util.List<Stereotype> costProfileStereotypes1 = StereotypesHelper.getStereotypesByProfile(costProfile);                      
                        java.util.List<Stereotype> costProfileStereotypes2 = StereotypesHelper.getStereotypesByProfile(costProfile);
                        //ftiaxnw listes gia na mpoun ola ta stereotupa gia kathe katigoria kostous
                        java.util.List<Stereotype> xList = new ArrayList<Stereotype>();
                        java.util.List<Stereotype> newXList = new ArrayList<Stereotype>();
                        java.util.List<Stereotype> subXList  = new ArrayList<Stereotype>();
                        //gemizw ti lista (xList) me stereotypes antistoixa tou cost element 
                        for (Stereotype costProfileStereotype1 : costProfileStereotypes1) {
                            String[] costProfileStereotypeName1 = costProfileStereotype1.getHumanName().split(" ", 2);
                            
                            if (costProfileStereotypeName1[1].equals(costElementStereotypeName[1])) {
                                xList.add(costProfileStereotype1);
                                //an einai capex i opex to diwxnw apo ti lista
                                if (costProfileStereotypeName1[1].equals("CapExCost") || costProfileStereotypeName1[1].equals("OpExCost")) {
                                    xList.remove(costProfileStereotype1);
                                    List xCostElementStereotypeOwnedAttributes = costProfileStereotype1.getOwnedAttribute();
                                    //pairnw ta properties tou stereotype pou exw vrei
                                    for (int i = xCostElementStereotypeOwnedAttributes.size() - 1; i >= 0; --i) {
                                        Property xProperty = (Property) xCostElementStereotypeOwnedAttributes.get(i);

                                        for (Stereotype costProfileStereotype2 : costProfileStereotypes2) {
                                            String[] costProfileStereotypeName2 = costProfileStereotype2.getHumanName().split(" ", 2);

                                            if (costProfileStereotype2.getHumanName().equals(xProperty.getType().getHumanName())) {
                                                //vazw ta stereotupa-paidia apo to profile pou einai paidia tou capex i opex sti lista
                                                if (costProfileStereotypeName2[0].equals("Stereotype")) {
                                                    xList.add(costProfileStereotype2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //mia nea lista pairnei tin palia
                        newXList.addAll(xList);
                        //sindeseis tou cost element
                        Collection<DirectedRelationship> costElementDirectedRelationship = costElement.get_directedRelationshipOfSource();                        

//                        if (!costElement.has_directedRelationshipOfSource() && !costElement.has_directedRelationshipOfTarget() && !costElement.has_relationshipOfRelatedElement()) {
//                            javax.swing.JOptionPane.showMessageDialog(null, "No connections have been found for the cost entity!");
//                        }
//                        else {
                            
                        for (java.util.Iterator it = costElementDirectedRelationship.iterator(); it.hasNext();) {
                            Element cost2BlockDirectedRelationship = (Element) it.next();
                            Stereotype connSter = StereotypesHelper.getStereotypes(cost2BlockDirectedRelationship).get(0);
                            String[] connSterName = connSter.getHumanName().split(" ", 2);

                            if (connSterName[1].equals("Estimation")) {
                                Abstraction abstraction = (Abstraction) cost2BlockDirectedRelationship;
                                //pairnw to connected block element pou exei kostos/einai enwmeno me cost entity
                                Element blockElement = abstraction.getTarget().iterator().next();
                                blockElementName = blockElement.getHumanName().split(" ", 2);
                                //pairnw to istoriko tou block entity (gia na dw an einai block)
                                Collection<Stereotype> blockElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(blockElement);

                                for (Stereotype blockElementHierarchyStereotype : blockElementHierarchyStereotypes) {
                                    String[] blockElementHierarchyStereotypeName = blockElementHierarchyStereotype.getHumanName().split(" ", 2);
                                    //vriskw an einai ontws block
                                    if (blockElementHierarchyStereotypeName[1].endsWith("Block")) {
                                        foundBlock = true;
                                        break;
                                    }
                                }
                                //an einai block
                                if (foundBlock) {
                                    //to vazw sto visited nodes list kai proxwraw
                                    visitedNodes.add(blockElement);
                                    //sindeseis tou block element me to antistoixo cost element / na dw an exei ki alla estimations apo alla cost elements
                                    Collection<DirectedRelationship> blockElementDirectedRelationship = blockElement.get_directedRelationshipOfTarget();

                                    if (blockElementDirectedRelationship != null) {

                                        for (java.util.Iterator it2 = blockElementDirectedRelationship.iterator(); it2.hasNext();) {
                                            Element block2CostDirectedRelationship = (Element) it2.next();
                                            Stereotype connSter2 = StereotypesHelper.getStereotypes(block2CostDirectedRelationship).get(0);
                                            String[] connSterName2 = connSter2.getHumanName().split(" ", 2);

                                            if (connSterName2[1].equals("Estimation")) {
                                                Abstraction abstraction2 = (Abstraction) block2CostDirectedRelationship;
                                                //pairnw to connected cost element kai to stereotupo tou - an iparxei mpainei sti lista me ta ipoloipa
                                                Element connectedCostElement = abstraction2.getSource().iterator().next();
                                                Stereotype connectedCostElementStereotype = StereotypesHelper.getStereotypes(connectedCostElement).get(0);
                                                //to vazw sti nea sublist
                                                if (xList.contains(connectedCostElementStereotype)) {
                                                    subXList.add(connectedCostElementStereotype);
                                                }
                                            } 
                                            else {
                                                continue;
                                            }
                                        }
                                        //kai afairw ti lista auti apo tin xlist
                                        xList.removeAll(subXList);
                                        //gia oti iparxei sti xlist ftiaxnw to antistoixo element
                                        for (int i = xList.size() - 1; i >= 0; --i) {  
                                            javax.swing.JOptionPane.showMessageDialog(null, xList.get(i).getHumanName());
                                            createElements(project, costProfile, diagram, blockElement, blockElementName, xList.get(i).getHumanName().split(" ", 2), i);
                                        }
                                    }
                                    //episkeptomai ta paidia pou mporei na exei to block gia na dw an exoun kosti
                                    visitChildren(blockElement, newXList);
                                    messageFrame("<html>\"" + costElementStereotypeName[1] + ": " + costElementName[1] + "\" cost block has been validated.<br/>It can <strong>now</strong> be computed!</html>", true, false);
                                }
                                else {
                                    javax.swing.JOptionPane.showMessageDialog(null, "The cost enity is not connected to a \"block\" entity!");
                                }  
                            }
                            else {
                                continue;
                            }
                        }
                    }
                }
                SessionManager.getInstance().closeSession();
            } 

            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Diagram diagram = project.getActiveDiagram().getDiagram();   
                DiagramPresentationElement diagramPresentationElement = project.getDiagram(diagram); 

                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {     
                        Element costElement = presentationElement.getElement();
                        Collection<Stereotype> costElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(costElement);
                      
                        for (Stereotype costElementHierarchyStereotype : costElementHierarchyStereotypes) {
                            String[] costElementHierarchyStereotypeName = costElementHierarchyStereotype.getHumanName().split(" ", 2);
                            
                            if (costElementHierarchyStereotypeName[1].equals("Cost")) {
                                setEnabled(true);
                            }
                        } 
                    }
                }
            }
        };
        
        computeCost = new DefaultDiagramAction("Compute", "Compute", null, null) {
            public void actionPerformed(ActionEvent e) { 
                SessionManager.getInstance().createSession("compute");  
                Project project = Application.getInstance().getProject();
                Profile costProfile = (Profile) StereotypesHelper.getProfile(project, "CostProfile");
                Diagram diagram = project.getActiveDiagram().getDiagram();   
                DiagramPresentationElement diagramPresentationElement = project.getDiagram(diagram); 
                computedNodes = new ArrayList<Element>();
//                Stereotype tmpCostElementStereotype = null;
                Stereotype costElementStereotype = null;
                Boolean foundBlock = false;
                long summedValue = 0;
                long genSumVal = 0;
                Property property = null;
                Stereotype elementStereotype;
                Element newCostElement = null;
                Element tmpElement;
                boolean hasValueProp = false;
                List tagValues = null;
                List tagValues2 = null;
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {  
                        //edw einai to pressed (clicked apo to sxediasti) cost element kai to name tou
                        Element costElement = presentationElement.getElement(); 
                        String[] costElementName = costElement.getHumanName().split(" ", 2);
                        //to stereotupo tou cost element
                        Stereotype tmpCostElementStereotype = StereotypesHelper.getStereotypes(costElement).get(0); 
                        String[] costElementStereotypeName = tmpCostElementStereotype.getHumanName().split(" ", 2);
                        java.util.List<Stereotype> costProfileStereotypes1 = StereotypesHelper.getStereotypesByProfile(costProfile);
                        java.util.List<Stereotype> costProfileStereotypes2 = StereotypesHelper.getStereotypesByProfile(costProfile);
                        //listes gia na mpoun ola ta stereotupa gia kathe katigoria kostous
                        java.util.List<Stereotype> xList = new ArrayList<Stereotype>();
                        java.util.List<Stereotype> newXList = new ArrayList<Stereotype>();
                        java.util.List<Stereotype> subXList  = new ArrayList<Stereotype>();
                        
                        //gemizw ti lista (exList) me stereotypes antistoixa tou cost element 
                        for (Stereotype costProfileStereotype1 : costProfileStereotypes1) {
                            String[] costProfileStereotypeName1 = costProfileStereotype1.getHumanName().split(" ", 2);
                            
                            if (costProfileStereotypeName1[1].equals(costElementStereotypeName[1])) {
                                xList.add(costProfileStereotype1);
                                if (costProfileStereotypeName1[1].equals("CapExCost") || costProfileStereotypeName1[1].equals("OpExCost")) {
                                    xList.remove(costProfileStereotype1);
                                    
                                    List xCostElementStereotypeOwnedAttributes = costProfileStereotype1.getOwnedAttribute();
                                    
                                    for (int i = xCostElementStereotypeOwnedAttributes.size() - 1; i >= 0; --i) {
                                        Property xProperty = (Property) xCostElementStereotypeOwnedAttributes.get(i);

                                        for (Stereotype costProfileStereotype2 : costProfileStereotypes2) {
                                            String[] costProfileStereotypeName2 = costProfileStereotype2.getHumanName().split(" ", 2);

                                            if (costProfileStereotype2.getHumanName().equals(xProperty.getType().getHumanName())) {

                                                if (costProfileStereotypeName2[0].equals("Stereotype")) {
                                                    xList.add(costProfileStereotype2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //ta pateres-stereotupa tou cost element
                        Collection<Stereotype> costElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(costElement);
                        
                        for (Stereotype costElementHierarchyStereotype : costElementHierarchyStereotypes) {
                            String[] costElementHierarchyStereotypeName = costElementHierarchyStereotype.getHumanName().split(" ", 2);
                            
                            if (costElementStereotypeName[1].equals(costElementHierarchyStereotypeName[1])) {
                                //to stereotupe tou element pou stelnw stin anadromi
                                costElementStereotype = costElementHierarchyStereotype;
                                break;
                            }
                        }
                        Collection<DirectedRelationship> costElementDirectedRelationship = costElement.get_directedRelationshipOfSource(); 

                        for (java.util.Iterator it = costElementDirectedRelationship.iterator(); it.hasNext();) {
                            Element cost2BlockDirectedRelationship = (Element) it.next();
                            Stereotype connSter = StereotypesHelper.getStereotypes(cost2BlockDirectedRelationship).get(0);
                            String[] connSterName = connSter.getHumanName().split(" ", 2);

                            if (connSterName[1].equals("Estimation")) {
                                Abstraction abstraction = (Abstraction) cost2BlockDirectedRelationship;
                                //to connected block element
                                Element blockElement = abstraction.getTarget().iterator().next();
                                //ta pateres-stereotupa tou block element
                                Collection<Stereotype> blockElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(blockElement);

                                for (Stereotype blockElementHierarchyStereotype : blockElementHierarchyStereotypes) {
                                    String[] blockElementHierarchyStereotypeName = blockElementHierarchyStereotype.getHumanName().split(" ", 2);

                                    if (blockElementHierarchyStereotypeName[1].endsWith("Block")) {
                                        foundBlock = true;
                                        break;
                                    }
                                }

                                if (foundBlock) {
                                    //tha mpei gia athroisma
                                    //if capex vres cost entities gia block m for gia kathe paidi
                                    //alliws xsana o kwdikas gia ts ipoloipa
                                    if (costElementStereotypeName[1].equals("CapExCost") || costElementStereotypeName[1].equals("OpExCost")) {
                                        Collection<DirectedRelationship> capExBlockOtherCostEntities = blockElement.get_directedRelationshipOfTarget(); 
                        
                                        for (java.util.Iterator it3 = capExBlockOtherCostEntities.iterator(); it3.hasNext();) {
                                            Element relationship = (Element) it3.next();
                                            Stereotype connSter2 = StereotypesHelper.getStereotypes(relationship).get(0);
                                            String[] connSterName2 = connSter2.getHumanName().split(" ", 2);

                                            if (connSterName[1].equals("Estimation")) {
                                                Abstraction abstraction3 = (Abstraction) relationship;
                                                //to connected block element
                                                newCostElement = abstraction3.getSource().iterator().next();
                                                elementStereotype = StereotypesHelper.getStereotypes(newCostElement).get(0);
                                                String[] elementStereotypeName = elementStereotype.getHumanName().split(" ", 2);
                                                if (elementStereotypeName[1].equals(costElementStereotypeName[1])) {
                                                    continue;
                                                }
                                                
                                                if (xList.contains(elementStereotype)) {
                                                    subXList.add(elementStereotype);
                                                }
                                            }
                                            else {
                                                continue;
                                            }
                                        }
                                        xList.retainAll(subXList);
                                        
                                        for (int i = xList.size() - 1; i >= 0; --i) {
                                            computedNodes = new ArrayList<Element>();
                                            computedNodes.add(blockElement);
                                            tmpElement = StereotypesHelper.getExtendedElements(xList.get(i)).get(0);
                                            //anadromi gia na parei to teliko athroisma apo ta paidia
                                            summedValue = computeChildren(blockElement, xList.get(i), summedValue);

                                            if (summedValue == 0) {
                                                summedValue = Long.parseLong(StereotypesHelper.getStereotypePropertyValue(tmpElement, xList.get(i), "value").get(0).toString());
                                            }
                                            //thetei tin teliki timi ston patera cost element
                                            genSumVal = genSumVal + summedValue;
                                            StereotypesHelper.setStereotypePropertyValue(tmpElement, xList.get(i), "value", summedValue);
                                            summedValue = 0; 
                                        }
                                        StereotypesHelper.setStereotypePropertyValue(costElement, tmpCostElementStereotype, "value", genSumVal);
                                        messageFrame("<html>\"" + costElementStereotypeName[1] + ": " + costElementName[1] + "\" cost block has been computed!</html>", false, true);
                                        genSumVal = 0;
                                    }
                                    else {
                                        computedNodes.add(blockElement);
                                        //anadromi gia na parei to teliko athroisma apo ta paidia
                                        summedValue = computeChildren(blockElement, costElementStereotype, summedValue);
                                        
                                        StereotypesHelper.setStereotypePropertyValue(costElement, tmpCostElementStereotype, "value", summedValue);
                                        messageFrame("<html>\"" + costElementStereotypeName[1] + ": " + costElementName[1] + "\" cost block has been computed!</html>", false, true);
                                    }
                                    
                                    Collection<Element> elemEls = costElement.getOwnedElement();
                                    List<Stereotype> sters = StereotypesHelper.getStereotypes(costElement);  

                                    for (Stereotype ster : sters) {

                                        for (Element elemEl : elemEls) {
                                            
                                            if (elemEl.getHumanName().equals("Property base_Class")) {
                                                hasValueProp = true;
                                                break;
                                            }
                                        }

                                        if (hasValueProp) {
                                            
                                            for (Element elemEl : elemEls) {
                                                String[] elemElName = elemEl.getHumanName().split(" ", 2);
                                                
                                                if (elemElName[0].equals("Value")) {
                                                
                                                    for (Stereotype costElHierSter : costElementHierarchyStereotypes) {
                                                        String[] costElHierSterName = costElHierSter.getHumanName().split(" ", 2);

                                                        if (costElHierSterName[1].equals("Cost")) {
                                                            List<Property> costElHierSterOwnedAttrs = costElHierSter.getOwnedAttribute();

                                                            for (Property costElHierSterOwnedAttr : costElHierSterOwnedAttrs) {
                                                                
                                                                if (elemElName[1].equals(costElHierSterOwnedAttr.getHumanName())) {
                                                                    Property prop = (Property) elemEl;
                                                                    tagValues = StereotypesHelper.getStereotypePropertyValue(costElement, tmpCostElementStereotype, costElHierSterOwnedAttr);
                                                                    
                                                                    for (int i = 0; i < tagValues.size(); i++) {
                                                                        ValueSpecification valueSpecification = ModelHelper.createValueSpecification(project, prop.getType(), tagValues.get(i), null);
                                                                        prop.setDefaultValue(valueSpecification);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            return;
                                        }
                                        else {
                                            for (Stereotype costElHierSter : costElementHierarchyStereotypes) {
                                                String[] costElHierSterName = costElHierSter.getHumanName().split(" ", 2);

                                                if (costElHierSterName[1].equals("Cost")) {
                                                    List<Property> costElHierSterOwnedAttrs = costElHierSter.getOwnedAttribute();

                                                    for (Property costElHierSterOwnedAttr : costElHierSterOwnedAttrs) {
                                                        property = project.getElementsFactory().createPropertyInstance(); //ftiaxnw nea properties me vasi ta parapanw
                                                        property.setName(costElHierSterOwnedAttr.getName());
                                                        property.setType(costElHierSterOwnedAttr.getType());
                                                        tagValues = StereotypesHelper.getStereotypePropertyValue(costElement, tmpCostElementStereotype, costElHierSterOwnedAttr);

                                                        try {
                                                            ModelElementsManager.getInstance().addElement(property, costElement);
                                                        } catch (ReadOnlyElementException ex) {
                                                            Logger.getLogger(NewGeneralCostPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                                        }

                                                        for (int i = 0; i < tagValues.size(); i++) {
                                                            ValueSpecification valueSpecification = ModelHelper.createValueSpecification(project, property.getType(), tagValues.get(i), null);
                                                            property.setDefaultValue(valueSpecification);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                continue;
                            }
                        }
                    }
                }
                computedNodes.clear();
                SessionManager.getInstance().closeSession();
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Diagram diagram = project.getActiveDiagram().getDiagram();   
                DiagramPresentationElement diagramPresentationElement = project.getDiagram(diagram); 

                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {     
                        Element costElement = presentationElement.getElement();
                        String[] costElementName = costElement.getHumanName().split(" ", 2);
                        Collection<Stereotype> costElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(costElement);
                      
                        for (Stereotype costElementHierarchyStereotype : costElementHierarchyStereotypes) {
                            String[] costElementHierarchyStereotypeName = costElementHierarchyStereotype.getHumanName().split(" ", 2);
                            
                            if (costElementHierarchyStereotypeName[1].equals("Cost")) {
                                setEnabled(true);
                            }
                        } 
                    }
                }
            }
        };
        
        ActionsConfiguratorsManager.getInstance().addDiagramContextConfigurator(DiagramTypeConstants.UML_CLASS_DIAGRAM, new DiagramContextAMConfigurator() {
            public void configure(ActionsManager manager, DiagramPresentationElement diagram, PresentationElement selected[], PresentationElement requestor) {  
                ActionsCategory actions = new ActionsCategory(null, "Cost Menu");
                List actionsInCategory = actions.getActions();
                manager.addCategory(actions);
                actions.setActions(actionsInCategory);
                actions.setNested(true);
                actions.addAction(validateCost);
                actions.addAction(computeCost);
            }            
            public int getPriority() {
                return AMConfigurator.MEDIUM_PRIORITY;
            }
        });
    }
    
    void visitChildren(Element parentBlockElement, java.util.List<Stereotype> xList2) {
        Project project = Application.getInstance().getProject();           
        Profile costProfile = (Profile) StereotypesHelper.getProfile(project, "CostProfile");    
        Diagram diagram = project.getActiveDiagram().getDiagram(); 
        String[] childBlockElementName = null;
        String[] costElementStereotypeName = null;
        //sineseis tou parent block element gia ta associations me ta block-paidia
        Collection<Relationship> parentBlockElementDirectedRelationship = parentBlockElement.get_relationshipOfRelatedElement();
        //voithitikes listes me stereotupa gia kathe kathigoria kostous
        java.util.List<Stereotype> newXList2 = new ArrayList<Stereotype>();
        java.util.List<Stereotype> subXList2 = new ArrayList<Stereotype>();
        newXList2.addAll(xList2);
        List<Stereotype> estList = new ArrayList<Stereotype>();
        
        for (java.util.Iterator it = parentBlockElementDirectedRelationship.iterator(); it.hasNext();) {
            Element block2BlockDirectedRelationship = (Element) it.next();
            
            if (StereotypesHelper.getStereotypes(block2BlockDirectedRelationship).isEmpty()) {
                Association association = (Association) block2BlockDirectedRelationship;
                Property propertyBlockElement = ModelHelper.getFirstMemberEnd(association);
                String[] propertyBlockElementName = propertyBlockElement.getType().getHumanName().split(" ", 2);
                //pairnw ola ta elements sto diagramma
                List presentationElements = project.getActiveDiagram().getPresentationElements();

                for (int i = presentationElements.size() - 1; i >= 0; --i) {
                    PresentationElement presentationElement = (PresentationElement)
                    presentationElements.get(i);
                    Element childBlockElement = presentationElement.getElement();
                    //pairnw tous pateres-stereotupa tou block-child element na dw an einai ontws block
                    Collection<Stereotype> childBlockElementStereotypes = StereotypesHelper.getStereotypesHierarchy(childBlockElement);

                    for (Stereotype childBlockElementStereotype : childBlockElementStereotypes) {
                        String[] childBlockElementStereotypeName = childBlockElementStereotype.getHumanName().split(" ", 2);

                        if (childBlockElementStereotypeName[1].endsWith("Block")) {
                            childBlockElementName = childBlockElement.getHumanName().split(" ", 2);

                            if (childBlockElementName[1].equals(propertyBlockElementName[1])) {

                                if (!visitedNodes.contains(childBlockElement)) {
                                    visitedNodes.add(childBlockElement);
                                    Collection<Relationship> childBlockElementDirectedRelationship = childBlockElement.get_relationshipOfRelatedElement();
                                    
//                                    boolean isLeaf = true;
//                                    for (java.util.Iterator it2 = childBlockElementDirectedRelationship.iterator(); it2.hasNext();) {
//                                        Element block2ChildBlockDirectedRelationship = (Element) it2.next();
//                                        
//                                        if (StereotypesHelper.getStereotypes(block2ChildBlockDirectedRelationship).isEmpty()) {
//                                            Association association2 = (Association) block2ChildBlockDirectedRelationship;
//                                            Property propertyChildBlockElement = ModelHelper.getFirstMemberEnd(association2);
//                                            String[] propertyChildBlockElementName = propertyChildBlockElement.getType().getHumanName().split(" ", 2);
//                                            if (propertyChildBlockElement != null && !childBlockElementName[1].equals(propertyChildBlockElementName[1])) {
//                                                isLeaf = false;
//                                                break;
//                                            }
//                                        }
//                                        else {
//                                            continue;
//                                        }
//                                    }
//                                    psaxnw an exei paidi - an exei sinexizei, an oxi continue;
//                                    if (isLeaf) {
//                                        continue;
//                                    }

                                    //pairnw tis sindeseis tou block-paidiou, thelw na vrw estimation me cost element
                                    Collection<DirectedRelationship> connectedElementDirectedRelationship = childBlockElement.get_directedRelationshipOfTarget();

                                    if (connectedElementDirectedRelationship != null) {

                                        for (java.util.Iterator it3 = connectedElementDirectedRelationship.iterator(); it3.hasNext();) {
                                            Element block2CostDirectedRelationship = (Element) it3.next();
                                            Stereotype connSter3 = StereotypesHelper.getStereotypes(block2CostDirectedRelationship).get(0);
                                            String[] connSterName3 = connSter3.getHumanName().split(" ", 2);
                                            
                                            if (connSterName3[1].equals("Estimation")) {
                                                estList.add(connSter3);
                                                Abstraction abstraction2 = (Abstraction) block2CostDirectedRelationship;
                                                //pairnw to connected cost element
                                                Element costElement = abstraction2.getSource().iterator().next();
                                                Stereotype costElementStereotype = StereotypesHelper.getStereotypes(costElement).get(0);

                                                if (xList2.contains(costElementStereotype)) {
                                                    subXList2.add(costElementStereotype);
                                                }
                                            }
                                            else {
                                                continue;
                                            }
                                        }
                                        xList2.removeAll(subXList2);

                                        for (int j = xList2.size() - 1; j >= 0; --j) {   
                                            createElements(project, costProfile, diagram, childBlockElement, childBlockElementName, xList2.get(j).getHumanName().split(" ", 2), j);
                                        }
                                    }
                                    visitChildren(childBlockElement, newXList2);
                                }
                            }
                        }
                    }
                }
            }
            else {
                continue;
            } 
        }
    }
    
    long computeChildren(Element parentBlockElement, Stereotype costElementStereotype, Long summedValue) {
        Project project = Application.getInstance().getProject();    
        String[] childBlockElementName = null;
        Element costElement = null;
        boolean isSame = false;
        List<String> costStereotypePropertyValueString = null; 
        List<Long> costStereotypePropertyValueInteger = null;
        //pare oles tis sindeseis tou parent block element gia na deis an iparxoun paidia blocks
        Collection<Relationship> parentBlockElementDirectedRelationship = parentBlockElement.get_relationshipOfRelatedElement();
        //to stereotype tou parent cost element
        String[] costElementStereotypeName = costElementStereotype.getHumanName().split(" ", 2);
        List<Stereotype> evSterList = new ArrayList<Stereotype>();
        boolean isEv = false;
                
        if (parentBlockElementDirectedRelationship != null) {
            
            for (java.util.Iterator it = parentBlockElementDirectedRelationship.iterator(); it.hasNext();) {
                Element block2BlockDirectedRelationship = (Element) it.next();

                if (StereotypesHelper.getStereotypes(block2BlockDirectedRelationship).isEmpty()) {
                    Association association = (Association) block2BlockDirectedRelationship;
                    //vriskw to property pou prokiptei (einai to block-paidi) logw tis association
                    Property propertyBlockElement = ModelHelper.getFirstMemberEnd(association);
                    String[] propertyBlockElementName = propertyBlockElement.getType().getHumanName().split(" ", 2);
                    //pairnw ola ta elements sto diagramma
                    List presentationElements = project.getActiveDiagram().getPresentationElements();

                    for (int i = presentationElements.size() - 1; i >= 0; --i) {
                        PresentationElement presentationElement = (PresentationElement) presentationElements.get(i);
                        Element childBlockElement = presentationElement.getElement();
                        //pairnw tous pateres-stereotupa tou block-child element na dw an einai ontws block
                        Collection<Stereotype> childBlockElementStereotypes = StereotypesHelper.getStereotypesHierarchy(childBlockElement);

                        for (Stereotype childBlockElementStereotype : childBlockElementStereotypes) {
                            String[] childBlockElementStereotypeName = childBlockElementStereotype.getHumanName().split(" ", 2);

                            if (childBlockElementStereotypeName[1].endsWith("Block")) {
                                childBlockElementName = childBlockElement.getHumanName().split(" ", 2);
                                
                                if (childBlockElementName[1].equals(propertyBlockElementName[1])) {
                                    
                                    if (!computedNodes.contains(childBlockElement)) {
                                        computedNodes.add(childBlockElement);
                                        summedValue = computeChildren(childBlockElement, costElementStereotype, summedValue);
                                        //pairnw tis sindeseis tou block-paidiou, thelw na vrw estimation me cost element
                                        Collection<DirectedRelationship> childBlockElementDirectedRelationship = childBlockElement.get_directedRelationshipOfTarget();

                                        if (childBlockElementDirectedRelationship != null) {

                                            for (java.util.Iterator it2 = childBlockElementDirectedRelationship.iterator(); it2.hasNext();) {
                                                Element block2CostDirectedRelationship = (Element) it2.next();
                                                Stereotype connSter3 = StereotypesHelper.getStereotypes(block2CostDirectedRelationship).get(0);
                                                String[] connSterName3 = connSter3.getHumanName().split(" ", 2);

                                                if (connSterName3[1].equals("Estimation")) {
                                                    Abstraction abstraction2 = (Abstraction) block2CostDirectedRelationship;
                                                    //pairnw to connected cost element kai tis sindeseis tou
                                                    costElement = abstraction2.getSource().iterator().next();
                                                    Collection<Stereotype> costElementHierarchyStereotypes = StereotypesHelper.getStereotypesHierarchy(costElement);

                                                    for (Stereotype costElementHierarchyStereotype : costElementHierarchyStereotypes) {
                                                        String[] costElementHierarchyStereotypeName = costElementHierarchyStereotype.getHumanName().split(" ", 2);
                                                        
                                                        if (costElementStereotypeName[1].equals(costElementHierarchyStereotypeName[1])) {
                                                            isSame = true;
                                                            break;
                                                        }
                                                    }
                                                    
                                                    if (isSame) {
                                                        if (!StereotypesHelper.getStereotypePropertyValue(costElement, costElementStereotype, "value").isEmpty()) {
                                                            costStereotypePropertyValueString = StereotypesHelper.getStereotypePropertyValue(costElement, costElementStereotype, "value"); 
                                                            costStereotypePropertyValueInteger = new ArrayList<Long>(costStereotypePropertyValueString.size()); 
                                                        }
                                                        else {
                                                            StereotypesHelper.setStereotypePropertyValue(costElement, costElementStereotype, "value", "0");
                                                            costStereotypePropertyValueString = StereotypesHelper.getStereotypePropertyValue(costElement, costElementStereotype, "value"); 
                                                            costStereotypePropertyValueInteger = new ArrayList<Long>(costStereotypePropertyValueString.size());
                                                        }
                                                        
                                                        for(String current : costStereotypePropertyValueString){
                                                            costStereotypePropertyValueInteger.add(Long.parseLong(current));
                                                        }
                                                        summedValue = summedValue + costStereotypePropertyValueInteger.get(0);
                                                    }
                                                }
                                                else {
                                                    continue;
                                                }
                                                isSame = false;
                                            }
                                        } 
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    continue;
                }
            } 
        }
        return summedValue;
    }
    
    void createElements(Project project, Profile costProfile, Diagram diagram, Element blockElement, String[] blockElementName, String[] costElementStereoypeName, Integer number) {
        //gia to position tou kathe cost element
        Random random = new Random();
        int maxValue = 80;
        int minValue = 8;
        int randomNumber = random.nextInt(maxValue - minValue + 1) + minValue;
        //create new element
        com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class costClass = project.getElementsFactory().createClassInstance();
        Stereotype costElementStereotype = StereotypesHelper.getStereotype(project, costElementStereoypeName[1], costProfile);
        StereotypesHelper.addStereotype(costClass, costElementStereotype);
        costClass.setName(blockElementName[1] + "_" + costElementStereoypeName[1].substring(0, 4) + "_Cost");

        try {
            ModelElementsManager.getInstance().addElement(costClass, project.getModel());
        } catch (ReadOnlyElementException ex) {
        }
        PresentationElementsManager presentationElementsManager = PresentationElementsManager.getInstance();
        DiagramPresentationElement diagramPresentationElement = project.getDiagram(diagram);
        ShapeElement shapeCostElement = null;

        try {
            shapeCostElement = presentationElementsManager.createShapeElement(costClass, diagramPresentationElement);
        } catch (ReadOnlyElementException ex) {
            Logger.getLogger(NewGeneralCostPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        shapeCostElement.setLocation(randomNumber, randomNumber);
        Rectangle classBounds = shapeCostElement.getBounds();
        
        Element costElement = (Element) costClass;
        PresentationElement presentationElement = project.getSymbolElementMap().getPresentationElement(costElement);
        ElementsFactory elementsFactory = Application.getInstance().getProject().getElementsFactory();
        Abstraction abstraction = elementsFactory.createAbstractionInstance();
        StereotypesHelper.addStereotype(abstraction, StereotypesHelper.getStereotype(project, "Estimation", costProfile));
        abstraction.setOwner(Application.getInstance().getProject().getModel());
        ModelHelper.setSupplierElement(abstraction, blockElement);
        ModelHelper.setClientElement(abstraction, costElement);
        PresentationElement supplierElement = project.getSymbolElementMap().getPresentationElement(blockElement);
        PresentationElement clientElement = project.getSymbolElementMap().getPresentationElement(costElement);

        try {
            PathElement pathElement = (PathElement) PresentationElementsManager.getInstance().createPathElement(abstraction, clientElement, supplierElement);
        } catch (ReadOnlyElementException ex) {
            Logger.getLogger(NewGeneralCostPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ModelElementsManager.getInstance().addElement(abstraction, project.getModel());
        } catch (ReadOnlyElementException ex) {
            Logger.getLogger(NewGeneralCostPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        StereotypesHelper.setStereotypePropertyValue(costClass, costElementStereotype, "value", Double.parseDouble("0.0"));
        StereotypesHelper.setStereotypePropertyValue(costClass, costElementStereotype, "measUnit", "Euro");
    }
    
    void messageFrame (String message, boolean isValidation, boolean isComputation) {
        //create frame
        final JFrame jFrame = new JFrame();
        //Create main panel
        JPanel jPanel = new JPanel();
        //Set main panel's layout
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        //Create subpanels
        JPanel jPanel1 = new JPanel();
        JPanel jPanel2 = new JPanel();
        JPanel jPanel3 = new JPanel();
        //Subpanel 1 (details) and 2 (actions) layouts
        jPanel1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        jPanel2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        jPanel3.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        //set label for subpanel 1 (details)
        JLabel jLabel = new JLabel(message);
        jLabel.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
        jLabel.setIconTextGap(10);
        //txt.setIcon(null);
        jPanel1.add(jLabel);
        //set finsih button to subpanel 3
        JButton jPFinish = new JButton("Finish");
        jPFinish.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false);
            }
        });
        jPanel3.add(jPFinish);
        //Add subpanels in main panel
        jPanel.add(jPanel1);
        jPanel.add(jPanel2);
        jPanel.add(jPanel3);
        //if parameter is isVal, then show respective message
        if (isValidation) {
            javax.swing.JOptionPane.showOptionDialog(jFrame, jPanel, "Validation successful!", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
        }
        //if parameter is isComp, then show respective message
        if (isComputation) {
            javax.swing.JOptionPane.showOptionDialog(jFrame, jPanel, "Computation successful!", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
        }
        isValidation = false;
        isComputation = false;
    }
    
    public boolean close() {
        return true;
    }
    
    public boolean isSupported() {
        return true;
    }
}