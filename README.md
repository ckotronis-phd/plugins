# THIS REPOSITORY

This repository contains the profiles, plugins and a short manual developed during my PhD research (2016-2021) titled:
"QoS-aware MBSD using SysML", while at the Department of Informatics and Telematics, School of Digital Technology, Harokopio University of Athens. The README will be updated in time with more details and visual aids regarding the use of the software.

![](Videos-Gifs/General_manual.gif)

# PROFILES

*Note: the following profile diagrams can be opened in `Cameo Systems Modeler` tool; other suites that have integrated the SysML profile can also be used.*

## CostProfile

It contains the `CostProfileDiagram` and `CostFunctionsLibraryDiagram` for defining complete cost models.
Note that there are older versions of the `CostProfile` (`CostProfileOLD` and `CostProfileOLD2`); the latter contains a beautified profile diagram for demonstration purposes.

## CostReqProfile

It contains the CostReqProfileDiagram for defining Graded Quantitative Requirements and requirement verification entities.
Regarding its title, it is the QoS profile that can be applied to a model to define QoS requirements and verify these requirements. 

## LoSLineStopSuite, LoSSuite, newSuite, TrainLoSSpaceSuite, rtsCostReqVal, ValidationForRTS, ValidationForRTSs

These are used as validation suites specifically for the Cameo Systems Modeler tool in RTS models;
they connect elements of another profile (like RTS profile's Line) with specific constraints, in order to be validated during model validation.

## REMSCritCheck, REMSCritVal, REMSHumanCritCheck, REMSVal

These are used as validation suites specifically for the Cameo Systems Modeler tool in CPHS REMS models;
they connect elements of another profile (like REMS profile's criticalities) with constraints, in order to be validated during model validation.

## TransportationModelProfile

It is the main profile for RTS.
It contains all RTS entities (Line, Train, Stop, etc.), simulation entities that receive simulation results and evaluate RTS entities, and specific requirements, for defining complete RTS models.

## REMSProfileProject

It is the main profile for CPHS REMS.
It contains all REMS entities (Device, Aggregator, etc.), and specific requirements, here named criticalities, for defining complete REMS models.

# PLUGINS

*Note: the following plugins are Java-based, implemented in Netbeans IDE. cThey can be applied to the plugins folder of Cameo Systems Modeler tool. Other Java-based modeling suites may be suitable for such plugins.*

## CritCheckPlugin, CritValPlugin, NewValidationPlugin, HumanCritCheckPlugin

* Java-based plugins that check whether CPHS models are complete and correct. 
They all contain constraints that can are applied to specific entities within the models.
	
* Main action is the analysis of system QoS. This action accommodates the verification (or not) of QoS requirements. In essence, the output of the analysis models (either simulation or mathematical equations) is the computed level of QoS requirements. When our plugin is activated, these levels are automatically compared against the desired ones (i.e. those defined with the requirements). When an obtained requirement level does not verify the desired one, 
the frame of the "defective" requirement as well as its connection with the respective system component(s) becomes red-colored, notifying the designer that there is a problem. 
In addition, the design tool exhibits appropriate information to system designers, as well as adjustments they can make.
The latter may include suggested actions or solutions (like reconsidering the structure of the system). 
This process can occur several times --dynamically-- in the system's design process, adjusting to changing operational circumstances. 
Thus, by assessing the design tool’s automated feedback, the designer can further improve system design.
		
* [Link - Google Doc](https://docs.google.com/document/d/1Q_pwqbh3G-P4BTS4fWTN4ULOtQeDnGi8lZAG6yjEnbU/edit?usp=sharing) (for depicting tables with all QoS constraints (validation rules)) 
	
## NewGeneralCostPlugin

* It automatically checks whether defined system components are properly connected to appropriate composite, aggregated or individual cost entities. In the case where a system component has a relation with a composite cost entity (like CapEx), the plugin also facilitates the creation of all its different-category sub-cost entities (i.e., acquisition and integration) to the same component, in an automated fashion; all cost entities are initialized with default values. 
The same applies to an aggregated cost entity (like acquisition) that is connected to a component, although same-category cost entities are created and connected only to other components that comprise the primary component. 
        
* The plugin automatically creates a SysML parametric diagram for each created cost entity; the designer then manually inserts a cost computation function and specific input / output properties. When the function is computed, the resulting cost value is automatically stored within the associated cost entity.
	   
* [Link - Google Doc](https://docs.google.com/document/d/1Q_pwqbh3G-P4BTS4fWTN4ULOtQeDnGi8lZAG6yjEnbU/edit?usp=sharing) (for depicting tables with all cost constraints (validation rules)

## PrologApp2 
	
* The system model, created by the designer, is contained within the design environment.
The designer specifies system components and their properties, requirements with their levels, and parametric diagrams that contain requirement constraints along with their formulas.
The system model, containing the aforementioned entities, is transformed into the rule-based decision support model to generate system design configurations, i.e. provide acceptable combinations of property values 
of system components, conforming to SysML requirements. The SysML system model-to-decision support model transformation process is  automated, by executing the following actions without requiring any involvement 
from the designer.(a)Validate the SysML system model. A critical prerequisite of  the  model transformation  process  is  the  validation  ofthe  correctness  and  completeness  of  the  SysML  system model.  
To  this  end,  the  plugin  contains  corresponding validation rules that capture important conditions, whichin  turn  must  be  checked  against  the  system  model.  
For example,  every  requirement  constraint  must  contain  a formula  and  be  connected  with  a  requirement  (via  a refine relation). 
Another rule may be that every parametric diagram  containing  a  requirement  constraint  must  also receive  required  input  from  system  components,  which in turn should hold specific properties.
(b) Generate the decision model. This model is implemented using the Prolog language, which enables the creation of knowledge  bases,  comprising  predicates  and  rules.  
The decision model is one such knowledge base, which stems from  the  SysML  model.  
Requirements,  constraints  and associations  between  requirements  and  system  components  are  transformed  into  Prolog  predicates  and  rules.  
Requirements  are  transformed  to  Prolog predicates.  
The  design  constraints  are  transformed  torespective  Prolog  rules.  
The  requirements-system  components associations (i.e. satisfy) are transformed to corresponding  Prolog  predicates.  
Prolog  elements  are  then combined into a general system configuration rule, based on  which  a  feasible  system  configuration  solution  is computed as a response to a Prolog query, and is finally produced.
(c)Ask for a configuration solution. A Prolog query, “asking” for  a  feasible  system  configuration  solution,  and  thedecision  model,  serve  as  inputs  to  the  external  tool,
which  executes  the  query.  The  outputs  of  the  execution are  system  design  suggestions  expressed  as  exact-value Prolog  predicates.  
If  a  feasible  solution  is  found,  the resulting  suggestions  are  incorporated  into  the  SysML system model (i.e., values are fed back into the properties of the associated components). 
The decision tool strives to satisfy all requirements based on the decision model; however, in some cases there may not be a solution satisfying all requirements. 
In any case, the plugin displays the yielded  (full  or  partial)  solution  to  the  system  designer, who can in turn choose if the solution is sufficient or not.

## REMSPlugin 

* Configuration. This action allows system designers to choose a specific system configuration. 
Such configurations facilitate the decisions of the designers by giving them the freedom of option, abstracting away the complexity of property values generation. 
When a designer chooses a certain configuration among others, specific properties of system components are populated with pre-defined values. 
Our plugin supports the exhibition of specific system configurations in the form of a menu. 
It is worth mentioning that the decision support configuration is encoded into the configurations menu and can help the designer choose the property values that correspond to the ``best'' system design.  
	
## RTSCostReqValPlugin

* Contains all actions that customize RTS models.

# BUILD OUTPUT DESCRIPTION

When you build an Java application project that has a main class, the IDE automatically copies all of the JAR files on the projects classpath to your projects dist/lib folder. 
The IDE also adds each of the JAR files to the Class-Path element in the application JAR files manifest file (MANIFEST.MF).

To run the project from the command line, go to the dist folder and type the following:
```
java -jar "CritValPlugin.jar" 
```
To distribute this project, zip up the dist folder (including the lib folder) and distribute the ZIP file.

Notes:

* If two JAR files on the project classpath have the same name, only the first JAR file is copied to the lib folder.
* Only JAR files are copied to the lib folder.
If the classpath contains other types of files or folders, these files (folders) are not copied.
* If a library on the projects classpath also has a Class-Path element
specified in the manifest,the content of the Class-Path element has to be on the projects runtime path.
* To set a main class in a standard Java project, right-click the project node in the Projects window and choose Properties. 
Then click Run and enter the class name in the Main Class field. Alternatively, you can manually type the class name in the manifest Main-Class element.

# MANUAL

## General manual 

Includes designer's actions (REMS model and respective profils and plugins are selected as case study).
[Link - Google Docs](https://docs.google.com/document/d/1o9he9XHptqxOz5ffsZRJXsOqGwVFxMwdu-fo7OaOdbg/edit?usp=sharing) (for depicting figures with text)  

## Cost profile and plugin manual

1. insert desired cost entity at BDD (class element with cost-related stereotype); 
it can be CapExCost or OpExCost entity according to the Cost profile, or one of their sub-cost entities (Acquisition or Integration of CapEx, Energy, Maintenance or Labor of OpEx);

2. connect cost entity with desired system component; “estimation” relationship must be used for their connection (from cost entity to the system component);

3. validate cost entity; this is enabled by the Cost plugin loaded to the modeling environment; 
    * if the cost entity is CapExCost or OpExCost, the validation method checks whether the connected component is also connected to other cost entities;
    	* in case it is connected, the method resets the cost entity’s “value” and “measUnit” properties’ values; 
    	it also informs the designer about a successful validation;

    	* in case it is not connected, the method creates CapEx’s or OpEx’s sub-cost entities (e.g., Acquisition and Integration) with a unique name and two value properties, i.e. “value” and “measUnit”; 
    	the values of these properties are initialized; these entities are connected to the system component via “estimation” relationships; afterwards, the method informs the designer that the validation was successful;
    
    	* note that a system component may comprise others (it may have children components, forming an hierarchy); 
    	the validation method also creates sub-cost entities for the children with respective value properties and connections;
    
    * if the cost entity is a sub-cost, i.e. Acquisition, Integration, etc., the validation method checks whether the children of the connected system component are connected to same-type cost entities;
    	* in case all children are connected, the method informs the designer that the validation was successful;
    	
    	* in case one or more children are not connected, same-type cost entities are created with initial value properties, and connected via “estimation” relationship to respective children;

4. compute composite costs; this is enabled by the Cost plugin loaded to the modeling environment; 
only a CapExCost or OpExCost entity is computed via this method;
	
	* the values of the “value” properties of the sub-costs are summed up; 
	this sum is stored as the “value” of the CapExCost or OpExCost entity; 
	in general, this method computes (adds) all different sub-costs of the same system component; 
	after computation the method informs the designer (“successful computation”);
	
	* note that this summation can be computed only if the “measUnit” value is the same at all sub-costs;

5. compute aggregated costs; 
this is enabled by the Cost plugin loaded to the modeling environment; 
only the sub-cost entities are computed via this method;
	
	* “value” properties’ values of same-type sub-costs connected to all children are summed up; 
	this sum is stored as the “value” of the same-type top cost entity connected to the father component; in general, this method computes (adds) same-type cost entities of different system components; 
	after computation the method informs the designer (“successful computation”);
	
	* note that this summation can be computed only if the “measUnit” value is the same at all sub-costs;

6. note that for both computation methods, each system component holds an “instances” property, indicating the number of the same system component defined at the BDD; 
the value of this property is taken into account during the summations, i.e. the respective cost value is multiplied by the number of the component instances;

7. the values of cost entities connected to leaf-children system components are computed via parametric diagrams; 
	
	* each connected cost entity is refined by a respective cost function, as defined in the cost profile; a complete function can be taken from the cost library that accompanies the cost profile; 
	
	* the designer should define additional cost properties that may participate to the function’s computation;
	
	* within the cost entity a parametric diagram is created by the designer, holding the function and respective properties; 
	properties for system components may also participate; after the function’s computation the computed value is stored as the cost entity’s cost value; 
	afterwards, these values are used in the summation during the composite or aggregated computation methods;

8. verify costing requirements; following the aforementioned computation methods, the CapExCost or OpExCost entities obtain a computed cost value; 
this value will be used to satisfy (or not) appropriate costing requirements; 
	
	* the designer defines requirements, regarding the appropriate a corresponding costing requirement can be defined, connected to the cost entity and satisfied by the system component (or not);
