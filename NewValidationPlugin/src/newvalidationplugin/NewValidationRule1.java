/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newvalidationplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.magicdraw.validation.SmartListenerConfigurationProvider;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdinterfaces.Interface;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.ext.magicdraw.mdusecases.Actor;
import com.nomagic.uml2.impl.PropertyNames;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Class;
import java.util.*;

/**
 *
 * @author Chris
 */

public class NewValidationRule1 implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    // Initialize LoS mappings
    //for Trains
    //public static final Map<String, double[]> TrainLoSDelayMap = Collections.unmodifiableMap(
    //new HashMap<String, double[]>() {{
        //put("A", new double[] {1.0, 3.0});
        //put("B", new double[] {3.0, 6.0});
    //}});
    
    public static final Map<String, double[]> TrainLoSDelayMap = new HashMap<String, double[]>();
    public static final ArrayList<String> LoSDelayOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
    
    /**
     * Solving actions.
     */
    private List<NMAction> actions;

    /**
     * Default constructor. Default constructor is required.
     */
    public NewValidationRule1() { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //mActions = new ArrayList<NMAction>();
        //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
        //NMAction action = new MyAction("REQUIREMENT_IS_NOT_VERIFIED", "Requirement is not verified", null);
        //mActions.add(action);
    }

    public void init(Project project, Constraint constraint) {
    }

    /**
     * Returns a map of classes and smart listener configurations.
     *
     * @return smart listener configurations.
     */
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        // initialize hasmap of smartlistener configurations
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        
        // listen to the value of the named field
        SmartListenerConfig config = new SmartListenerConfig();
        // TODO: check if we should listen to VALUE or TAG_VALUE
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        
        //SmartListenerConfig nested = config.listenToNested("ownedOperation");
        //config.listenTo(PropertyNames.OWNER);
        //nested.listenTo("name");
        //nested.listenTo("ownedParameter");

        
        //Collection<SmartListenerConfig> configs = Collections.singletonList(config);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class, configs);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.mdusecases.Actor.class, configs);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        //configMap.put(Interface.class, configs);
        
        return configMap;
    }

    /**
     * Executes the rule.
     *
     * @param project    a project of the constraint.
     * @param constraint constraint which defines validation rules.
     * @param elements   collection of elements that have to be validated.
     * @return a set of <code>Annotation</code> objects which specifies invalid objects.
     */
    public Set<Annotation> run(Project project, Constraint constraint, Collection<? extends Element> elements) {

//        // javax.swing.JOptionPane.showMessageDialog(null, "checking");
//        //javax.swing.JOptionPane.showMessageDialog(null, "checking element annotations");
//        Set<Annotation> result = new HashSet<Annotation>();
//        
//        Boolean LoSDelayStatusLeft = false;
//        Boolean LoSDelayStatusRight = false;
//        
//        String filename = "TrainLoSDelayLimits.txt";
//        String workingDirectory = System.getProperty("user.home");
//        String absoluteFilePath = "";
//        absoluteFilePath = workingDirectory + File.separator + filename;
//        BufferedReader br = null;
//        FileReader fr = null;
//        //String content = null;
//        String line = null;
//        double[] limits = new double[100];
//        //List<Integer> limits = new ArrayList<Integer>();
//
//        try {
//            FileReader fileReader = new FileReader(absoluteFilePath);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//            Scanner s = new Scanner(fileReader);
//            s.useLocale(Locale.US);
//            //Loop through the file if there is a next int to process it will continue
//            int i = 0;
//            while(s.hasNextDouble()){
//                //We store every int we read in the array
//                limits[i++] = s.nextDouble();                             
//            }
//            bufferedReader.close();
//            //javax.swing.JOptionPane.showMessageDialog(null, content);
//        } catch (IOException eve) {
//                eve.printStackTrace();
//        } finally {
//            try {
//                if (br != null)
//                        br.close();
//                if (fr != null)
//                        fr.close();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//        
//        TrainLoSDelayMap.put("A", new double[] {limits[0], limits[1]});
//        TrainLoSDelayMap.put("B", new double[] {limits[2], limits[3]});
//        TrainLoSDelayMap.put("C", new double[] {limits[4], limits[5]});
//        TrainLoSDelayMap.put("D", new double[] {limits[6], limits[7]});
//        TrainLoSDelayMap.put("E", new double[] {limits[8], limits[9]});
//        TrainLoSDelayMap.put("F", new double[] {limits[10], limits[11]});
//
//        // for loop is for calculating and checking the LoS per affected element
//        for (Element element : elements) {
//            Stereotype sterLine = StereotypesHelper.getAppliedStereotypeByString(element, "Line");
//            //javax.swing.JOptionPane.showMessageDialog(null, "current element in second loop name=" + element.getHumanName() +" type=" + element.getHumanType());
//            
//            if (sterLine != null) {
//                
//                LoSDelayStatusLeft = false;
//                LoSDelayStatusRight = false;
//                
//                //javax.swing.JOptionPane.showMessageDialog(null, "Constraint is: " + constraint.getName());
//                
//                // first, calculate the required LoS for this Line based on the relation
//                String requiredLoSDelay = null;
//                int requiredLoSDelayOrder = -1;
//               
//                // retrieve all directed relationships with source the to-be-verified element
//                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
//                
//                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
//                    // get the next relationship element
//                    Element relEl = (Element) it.next();
//                    
//                    // check if the relationship if of the verify type
//                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
//                    if (sterVerify != null) {
//                        Abstraction abs = (Abstraction) relEl;
//                        
//                        // get the target element of the directed verify relationship
//                        Element targetEl = abs.getTarget().iterator().next();
//                        
//                        // check if it is the correct stereotype
//                        Stereotype sterTrainLoSDelay = StereotypesHelper.getAppliedStereotypeByString(targetEl, "TrainLoSDelay");
//                        if (sterTrainLoSDelay != null) {
//                            
//                            requiredLoSDelay = getTagStringValue(targetEl, "TrainLoS-Delay", targetEl.getHumanType());
//                            //javax.swing.JOptionPane.showMessageDialog(null, "EIEIIII2 element " + targetEl.getHumanName() +" LoS=" + requiredLoSDelay);
//                            requiredLoSDelayOrder = LoSDelayOrderedList.indexOf(requiredLoSDelay);
//                            //javax.swing.JOptionPane.showMessageDialog(null, "Required LoS Delay is " + requiredLoSDelayOrder);
//                        }
//                    }
//                }
//                
//                String elementLoSDelayLeft = null;
//                String elementLoSDelayRight = null;
//                //javax.swing.JOptionPane.showMessageDialog(null, "Retrieving double tag value of " + element.getHumanName());
//                double statsAvgPassengerDelayOnLineL = getTagDoubleValue(element, "StatsAvgPassengerDelayOnLineL", element.getHumanType());
//                double statsAvgPassengerDelayOnLineR = getTagDoubleValue(element, "StatsAvgPassengerDelayOnLineR", element.getHumanType());
//
//                for (java.util.Iterator it = TrainLoSDelayMap.keySet().iterator(); it.hasNext();) {
//                    String currentLoSDelayLeft = it.next().toString();
//                    double leftValue = TrainLoSDelayMap.get(currentLoSDelayLeft)[0];
//                    double rightValue = TrainLoSDelayMap.get(currentLoSDelayLeft)[1];
//                    if ((statsAvgPassengerDelayOnLineL >= leftValue) && (statsAvgPassengerDelayOnLineL <= rightValue)){
//                      elementLoSDelayLeft = currentLoSDelayLeft;
//                      //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay is " + elementLoSDelay);
//                      break;
//                    }
//                }
//                for (java.util.Iterator it = TrainLoSDelayMap.keySet().iterator(); it.hasNext();) {
//                    String currentLoSDelayRight = it.next().toString();
//                    double leftValue = TrainLoSDelayMap.get(currentLoSDelayRight)[0];
//                    double rightValue = TrainLoSDelayMap.get(currentLoSDelayRight)[1];
//                    if ((statsAvgPassengerDelayOnLineR >= leftValue) && (statsAvgPassengerDelayOnLineR <= rightValue)){
//                      elementLoSDelayRight = currentLoSDelayRight;
//                      //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay is " + elementLoSDelay);
//                      break;
//                    }
//                }
//                    
//                if ((elementLoSDelayLeft != null) && LoSDelayOrderedList.contains(elementLoSDelayLeft)) {
//                    // Check if LoS is the required one or better
//                    int elementLoSDelayOrderLeft = LoSDelayOrderedList.indexOf(elementLoSDelayLeft);
//                    //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay Order is " + elementLoSDelay);
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Delay LoS " + elementLoSDelay + " and the required LoS is " + requiredLoSDelay);//requiredLoSDelay);
//                    if (elementLoSDelayOrderLeft <= requiredLoSDelayOrder) {
//                        LoSDelayStatusLeft = true;
//                    }
//                }
//                if ((elementLoSDelayRight != null) && LoSDelayOrderedList.contains(elementLoSDelayRight)) {
//                    // Check if LoS is the required one or better
//                    int elementLoSDelayOrderRight = LoSDelayOrderedList.indexOf(elementLoSDelayRight);
//                    //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay Order is " + elementLoSDelay);
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Delay LoS " + elementLoSDelay + " and the required LoS is " + requiredLoSDelay);//requiredLoSDelay);
//                    if (elementLoSDelayOrderRight <= requiredLoSDelayOrder) {
//                        LoSDelayStatusRight = true;
//                    }
//                }
//                
//                if ((!LoSDelayStatusLeft) && (!LoSDelayStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 0;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionDelayLeft = new ActionDelay(element, elementLoSDelayLeft, elementLoSDelayRight, requiredLoSDelay, "StatsAvgPassengerDelayOnLineL", "StatsAvgPassengerDelayOnLineR", statsAvgPassengerDelayOnLineL, statsAvgPassengerDelayOnLineR, flag);
//                    actions.add(actionDelayLeft);
//                    
//                    Annotation annotationDelayLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationDelayLeft);
//                }
//                else if (!LoSDelayStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 1;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionDelayLeft = new ActionDelay(element, elementLoSDelayLeft, null, requiredLoSDelay, "StatsAvgPassengerDelayOnLineL", null, statsAvgPassengerDelayOnLineL, 0.0, flag);
//                    actions.add(actionDelayLeft);
//                    
//                    Annotation annotationDelayLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationDelayLeft);
//                }
//                else if (!LoSDelayStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 2;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionDelayLeft = new ActionDelay(element, null, elementLoSDelayRight, requiredLoSDelay, null, "StatsAvgPassengerDelayOnLineR", 0.0, statsAvgPassengerDelayOnLineR, flag);
//                    actions.add(actionDelayLeft);
//                    
//                    Annotation annotationDelayLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationDelayLeft);
//                }
//                /*if (!LoSDelayStatus) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionDelay = new ActionDelay(element, elementLoSDelay, requiredLoSDelay, "StatsAvgPassengerDelayOnLineL", "StatsAvgPassengerDelayOnLineR", statsAvgPassengerDelayOnLineL, statsAvgPassengerDelayOnLineR);
//                    actions.add(actionDelay);
//                    
//                    Annotation annotationDelay = new Annotation(element, constraint, actions);
//                    result.add(annotationDelay);
//                }*/
//            
//            }
//        }
        return null;
    }

    /**
     * 
     */
    public void dispose() {
    }
    
    double getTagDoubleValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        double ret = -1.0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Double.parseDouble(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    String getTagStringValue(Element elm, String name, String type) { //gets the string tag value of an element <elm> of name <name> of stereotype <type>
        String ret = null;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = value.get(0).toString();
                break;
            }
        }
        return ret;
    }
}
