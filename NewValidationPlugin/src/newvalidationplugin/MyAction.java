/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newvalidationplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.ui.dialogs.MDDialogParentProvider;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 *
 * @author Chris
 */

public class MyAction extends NMAction
{
    public MyAction(String id, String name, KeyStroke stroke, String group)
    {
        super(id, name, stroke, group);
    }

    public MyAction(String id, String name, KeyStroke stroke)
    {
        super(id, name, stroke);
    }

    public MyAction(String id, String name, int mnemonic, String group)
    {
        super(id, name, mnemonic, group);
    }

    public MyAction(String id, String name, int mnemonic)
    {
        super(id, name, mnemonic);
    }

    public void actionPerformed(ActionEvent e)
    {
        javax.swing.JOptionPane.showMessageDialog(MDDialogParentProvider.getProvider().getDialogParent(), "Action " + getName() + " performed!");
    }
}
