/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newvalidationplugin;

import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.DiagramContextAMConfigurator;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.actions.DefaultDiagramAction;
import com.nomagic.magicdraw.uml.DiagramTypeConstants;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.magicdraw.validation.SmartListenerConfigurationProvider;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdinterfaces.Interface;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.ext.magicdraw.mdusecases.Actor;
import com.nomagic.uml2.impl.PropertyNames;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Class;
import java.util.*;
import javax.swing.JTextField;

/**
 *
 * @author Chris
 */

public class NewValidationRule2 implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    // Initialize LoS mappings
    //for Trains
    /*public static final Map<String, int[]> TrainLoSTrainsPerHourMap = Collections.unmodifiableMap(
    new HashMap<String, int[]>() {{
        put("A", new int[] {7, 9});
        put("B", new int[] {3, 6});
    }});*/
    public static final Map<String, int[]> TrainLoSTrainsPerHourMap = new HashMap<String, int[]>();

    public static final ArrayList<String> LoSTrainsPerHourOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));

    /**
     * Solving actions.
     */
    private List<NMAction> actions;

    /**
     * Default constructor. Default constructor is required.
     */
    public NewValidationRule2() {  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //mActions = new ArrayList<NMAction>();
        //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
        //NMAction action = new MyAction("REQUIREMENT_IS_NOT_VERIFIED", "Requirement is not verified", null);
        //mActions.add(action);
    }

    public void init(Project project, Constraint constraint) {
    }

    /**
     * Returns a map of classes and smart listener configurations.
     *
     * @return smart listener configurations.
     */
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        // initialize hasmap of smartlistener configurations
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        
        // listen to the value of the named field
        SmartListenerConfig config = new SmartListenerConfig();
        // TODO: check if we should listen to VALUE or TAG_VALUE
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        
        //SmartListenerConfig nested = config.listenToNested("ownedOperation");
        //config.listenTo(PropertyNames.OWNER);
        //nested.listenTo("name");
        //nested.listenTo("ownedParameter");

        
        //Collection<SmartListenerConfig> configs = Collections.singletonList(config);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class, configs);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.mdusecases.Actor.class, configs);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        //configMap.put(Interface.class, configs);
        
        return configMap;
    }

    /**
     * Executes the rule.
     *
     * @param project    a project of the constraint.
     * @param constraint constraint which defines validation rules.
     * @param elements   collection of elements that have to be validated.
     * @return a set of <code>Annotation</code> objects which specifies invalid objects.
     */
    public Set<Annotation> run(Project project, Constraint constraint, Collection<? extends Element> elements) {

//        // javax.swing.JOptionPane.showMessageDialog(null, "checking");
//        //javax.swing.JOptionPane.showMessageDialog(null, "checking element annotations");
//        Set<Annotation> result = new HashSet<Annotation>();
//        
//        Boolean LoSTrainsPerHourStatusLeft = false;
//        Boolean LoSTrainsPerHourStatusRight = false;
//        
//        String filename = "TrainLoSTrainsPerHourLimits.txt";
//        String workingDirectory = System.getProperty("user.home");
//        String absoluteFilePath = "";
//        absoluteFilePath = workingDirectory + File.separator + filename;
//        BufferedReader br = null;
//        FileReader fr = null;
//        //String content = null;
//        String line = null;
//        int[] limits = new int[100];
//        //List<Integer> limits = new ArrayList<Integer>();
//
//        try {
//            FileReader fileReader = new FileReader(absoluteFilePath);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//            Scanner s = new Scanner(fileReader);
//            //Loop through the file if there is a next int to process it will continue
//            for(int i = 0; s.hasNextInt(); i++){
//                //We store every int we read in the array
//                limits[i] = s.nextInt();                             
//            }
//            bufferedReader.close();
//            //javax.swing.JOptionPane.showMessageDialog(null, content);
//        } catch (IOException eve) {
//                eve.printStackTrace();
//        } finally {
//            try {
//                if (br != null)
//                        br.close();
//                if (fr != null)
//                        fr.close();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//        
//        TrainLoSTrainsPerHourMap.put("A", new int[] {limits[0], limits[1]});
//        TrainLoSTrainsPerHourMap.put("B", new int[] {limits[2], limits[3]});
//        TrainLoSTrainsPerHourMap.put("C", new int[] {limits[4], limits[5]});
//        TrainLoSTrainsPerHourMap.put("D", new int[] {limits[6], limits[7]});
//        TrainLoSTrainsPerHourMap.put("E", new int[] {limits[8], limits[9]});
//        TrainLoSTrainsPerHourMap.put("F", new int[] {limits[10], limits[11]});
//        
//        // for loop is for calculating and checking the LoS per affected element
//        for (Element element : elements) {
//            
//            Stereotype sterLine = StereotypesHelper.getAppliedStereotypeByString(element, "Line");
//
//            if (sterLine != null) {
//                
//                LoSTrainsPerHourStatusLeft = false;
//                LoSTrainsPerHourStatusRight = false;
//
//                // first, calculate the required LoS for this Line based on the relation
//                String requiredLoSTrainsPerHour = null;
//                int requiredLoSTrainsPerHourOrder = -1;
//               
//                // retrieve all directed relationships with source the to-be-verified element
//                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
//                
//                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
//                    // get the next relationship element
//                    Element relEl = (Element) it.next();
//                    
//                    // check if the relationship if of the verify type
//                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
//                    if (sterVerify != null) {
//                        Abstraction abs = (Abstraction) relEl;
//                        
//                        // get the target element of the directed verify relationship
//                        Element targetEl = abs.getTarget().iterator().next();
//                        
//                        // check if it is the correct stereotype
//                        Stereotype sterTrainLoSTrainsPerHour = StereotypesHelper.getAppliedStereotypeByString(targetEl, "TrainLoSTrainsPerHour");
//                        if (sterTrainLoSTrainsPerHour != null) {
//                            requiredLoSTrainsPerHour = getTagStringValue(targetEl, "TrainLoS-TrainsPerHour", targetEl.getHumanType());
//                            //javax.swing.JOptionPane.showMessageDialog(null, "EIEIIII3 element " + targetEl.getHumanName() +" LoS=" + requiredLoSTrainsPerHour);
//                            requiredLoSTrainsPerHourOrder = LoSTrainsPerHourOrderedList.indexOf(requiredLoSTrainsPerHour);
//                        }
//                    }
//                }
//
//                String elementLoSTrainsPerHourLeft = null;
//                String elementLoSTrainsPerHourRight = null;
//                int statsAvgTrainsPerHourL = (int) getTagIntValue(element, "StatsAvgTrainsPerHourL", element.getHumanType());
//                int statsAvgTrainsPerHourR = (int) getTagIntValue(element, "StatsAvgTrainsPerHourR", element.getHumanType());
//                
//                //javax.swing.JOptionPane.showMessageDialog(null, "Line has value " + statsMinSpacePassTrainL);
//               
//                for (java.util.Iterator it = TrainLoSTrainsPerHourMap.keySet().iterator(); it.hasNext();) {
//                    String currentLoSTrainsPerHourLeft = it.next().toString();
//                    double leftValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHourLeft)[0];
//                    double rightValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHourLeft)[1];
//                    if ((statsAvgTrainsPerHourL >= leftValue) && (statsAvgTrainsPerHourL <= rightValue)){
//                        elementLoSTrainsPerHourLeft = currentLoSTrainsPerHourLeft;
//                        break;
//                    }
//                    //else if ((statsAvgTrainsPerHourR >= leftValue) && (statsAvgTrainsPerHourR <= rightValue)) {
//                        //javax.swing.JOptionPane.showMessageDialog(null, "the value is" + statsAvgTrainsPerHourR);
//                    //}
//                }
//                for (java.util.Iterator it = TrainLoSTrainsPerHourMap.keySet().iterator(); it.hasNext();) {
//                    String currentLoSTrainsPerHourRight = it.next().toString();
//                    double leftValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHourRight)[0];
//                    double rightValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHourRight)[1];
//                    if ((statsAvgTrainsPerHourR >= leftValue) && (statsAvgTrainsPerHourR <= rightValue)){
//                        elementLoSTrainsPerHourRight = currentLoSTrainsPerHourRight;
//                        break;
//                    }
//                    //else if ((statsAvgTrainsPerHourR >= leftValue) && (statsAvgTrainsPerHourR <= rightValue)) {
//                        //javax.swing.JOptionPane.showMessageDialog(null, "the value is" + statsAvgTrainsPerHourR);
//                    //}
//                }
//                //javax.swing.JOptionPane.showMessageDialog(null, "the value is" + elementLoSTrainsPerHourLeft);
//                //javax.swing.JOptionPane.showMessageDialog(null, "the value is" + elementLoSTrainsPerHourRight);
//                if ((elementLoSTrainsPerHourLeft != null) && LoSTrainsPerHourOrderedList.contains(elementLoSTrainsPerHourLeft)) {
//                    // Check if LoS is the required one or better
//                    int elementLoSTrainsPerHourOrderLeft = LoSTrainsPerHourOrderedList.indexOf(elementLoSTrainsPerHourLeft);
//                    
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has TPH LoS " + elementLoSTrainsPerHour + " and the required LoS is " + requiredLoSTrainsPerHour);
//                    if (elementLoSTrainsPerHourOrderLeft <= requiredLoSTrainsPerHourOrder) {
//                        LoSTrainsPerHourStatusLeft = true;
//                    }
//                }
//                if ((elementLoSTrainsPerHourRight != null) && LoSTrainsPerHourOrderedList.contains(elementLoSTrainsPerHourRight)) {
//                    // Check if LoS is the required one or better
//                    int elementLoSTrainsPerHourOrderRight = LoSTrainsPerHourOrderedList.indexOf(elementLoSTrainsPerHourRight);
//                    
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has TPH LoS " + elementLoSTrainsPerHour + " and the required LoS is " + requiredLoSTrainsPerHour);
//                    if (elementLoSTrainsPerHourOrderRight <= requiredLoSTrainsPerHourOrder) {
//                        LoSTrainsPerHourStatusRight = true;
//                    }
//                }
//              
//                if ((!LoSTrainsPerHourStatusLeft) && (!LoSTrainsPerHourStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 0;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionTrainsPerHourLeft = new ActionTrainsPerHour(element, elementLoSTrainsPerHourLeft, elementLoSTrainsPerHourRight, requiredLoSTrainsPerHour, "StatsAvgTrainsPerHourL", "StatsAvgTrainsPerHourR", statsAvgTrainsPerHourL, statsAvgTrainsPerHourR, flag);
//                    actions.add(actionTrainsPerHourLeft);
//                    
//                    Annotation annotationTrainsPerHourLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationTrainsPerHourLeft);
//                }
//                else if (!LoSTrainsPerHourStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 1;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionTrainsPerHourLeft = new ActionTrainsPerHour(element, elementLoSTrainsPerHourLeft, null, requiredLoSTrainsPerHour, "StatsAvgTrainsPerHourL", null, statsAvgTrainsPerHourL, 0, flag);
//                    actions.add(actionTrainsPerHourLeft);
//                    
//                    Annotation annotationTrainsPerHourLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationTrainsPerHourLeft);
//                }
//                else if (!LoSTrainsPerHourStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 2;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionTrainsPerHourLeft = new ActionTrainsPerHour(element, null, elementLoSTrainsPerHourRight, requiredLoSTrainsPerHour, null, "StatsAvgTrainsPerHourR", 0, statsAvgTrainsPerHourR, flag);
//                    actions.add(actionTrainsPerHourLeft);
//                    
//                    Annotation annotationTrainsPerHourLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationTrainsPerHourLeft);
//                }
//            }
//            
//        }
        return null;
    }
    
    /**
     * 
     */
    public void dispose() {
    }
    
    double getTagDoubleValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        double ret = -1.0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Double.parseDouble(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    double getTagIntValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        int ret = 0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Integer.parseInt(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    String getTagStringValue(Element elm, String name, String type) { //gets the string tag value of an element <elm> of name <name> of stereotype <type>
        String ret = null;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = value.get(0).toString();
                break;
            }
        }
        return ret;
    }
}
