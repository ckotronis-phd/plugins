/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newvalidationplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationAction;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.utils.MDLog;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Classifier;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;

import java.awt.event.ActionEvent;
import java.util.Collection;
import javax.swing.*;

public class ActionDelay extends NMAction implements AnnotationAction {
    
    private Element elm;
    private String leftElmLoS;
    private String rightElmLoS;
    private String reqLoS;
    private String leftAttrName;
    private String rightAttrName;
    private double leftVal;
    private double rightVal;
    private int flg;
    
    String[] options = { "OK", "Suggested Solution/s"};
    /*
    * Example of the action that can be performed on multiple targets.
    */
    public ActionDelay(Element element, String leftElementLoS, String rightElementLoS, String requiredLoS, String leftAttributeName, String rightAttributeName, double leftValue, double rightValue, int flag)
    {
        super("REQUIREMENT_IS_NOT_VERIFIED", "The requirement is not verified! Click for details.", null);
        //mClassifier = classifier;
        elm = element;
        leftElmLoS = leftElementLoS;
        rightElmLoS = rightElementLoS;
        reqLoS = requiredLoS;
        leftAttrName = leftAttributeName;
        rightAttrName = rightAttributeName;
        leftVal = leftValue;
        rightVal = rightValue;
        flg = flag;
    }
    
    /**
     * Executes the action.
     *
     * @param e event caused execution.
     */
    public void actionPerformed(ActionEvent e)
    {
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        try
        {
            if (flg == 0) {
                //javax.swing.JOptionPane.showMessageDialog(null, "I am Lefty and Righty");
                int result = javax.swing.JOptionPane.showOptionDialog(null, 
                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {border-right: solid 1px #000; border-left: solid 1px #000;text-align: left;padding: 2px;}</style><center><b>Problem with Both Train Directions!</b></center><br><table><tr><th>Element</th><td>" 
                    + elm.getHumanName() 
                    + "</td></tr><tr><th>Current LoS (Left)</th><td>"
                    + leftElmLoS
                    + "</td></tr><tr><th>Current LoS (Right)</th><td>"
                    + rightElmLoS    
                    + "</td></tr><tr><th>Required LoS</th><td>"
                    + reqLoS 
                    + "</td></tr><tr><th>Non-verified Attribute (Left)</th><td>"
                    + leftAttrName
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + leftVal
                    + "</td></tr><tr><th>Non-verified Attribute (Right)</th><td>"
                    + rightAttrName    
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + rightVal + "</tr></html>", "'Delay' requirement - Details",
                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
                if (result == JOptionPane.NO_OPTION){
                    javax.swing.JOptionPane.showMessageDialog(null, "Passengers should be absorbed... ");
                    Application.getInstance().getGUILog().log("Passengers should be absorbed from passing Trains. Increasing the flow of the Trains, there will be more room inside passing Trains and no Passenger will remain at Stops. crease trainsMean or change trainsDistr, traisMean and trainsStandDev");
                }
            }
            else if (flg == 1) {
                //javax.swing.JOptionPane.showMessageDialog(null, "i am Lefty");
                int result = javax.swing.JOptionPane.showOptionDialog(null, 
                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {border-right: solid 1px #000; border-left: solid 1px #000;text-align: left;padding: 2px;}</style><center><b>Problem with Left Train Direction!</b></center><br><table><tr><th>Element</th><td>" 
                    + elm.getHumanName() 
                    + "</td></tr><tr><th>Current LoS (Left)</th><td>"
                    + leftElmLoS 
                    + "</td></tr><tr><th>Required LoS</th><td>"
                    + reqLoS 
                    + "</td></tr><tr><th>Non-verified Attribute (Left)</th><td>"
                    + leftAttrName
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + leftVal + "</tr></html>", "'Delay' requirement - Details",
                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
                if (result == JOptionPane.NO_OPTION){
                    javax.swing.JOptionPane.showMessageDialog(null, "Passengers should be absorbed... ");
                    Application.getInstance().getGUILog().log("Passengers should be absorbed from passing Trains. Increasing the flow of the Trains, there will be more room inside passing Trains and no Passenger will remain at Stops. crease trainsMean or change trainsDistr, traisMean and trainsStandDev");
                }
            }
            else if (flg == 2) {
                //javax.swing.JOptionPane.showMessageDialog(null, "i am Righty");
                int result = javax.swing.JOptionPane.showOptionDialog(null, 
                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {border-right: solid 1px #000; border-left: solid 1px #000;text-align: left;padding: 2px;}</style><center><b>Problem with Right Train Direction!</b></center><br><table><tr><th>Element</th><td>" 
                    + elm.getHumanName() 
                    + "</td></tr><tr><th>Current LoS (Right)</th><td>"
                    + rightElmLoS 
                    + "</td></tr><tr><th>Required LoS</th><td>"
                    + reqLoS 
                    + "</td></tr><tr><th>Non-verified Attribute (Right)</th><td>"
                    + rightAttrName
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + rightVal + "</tr></html>", "'Delay' requirement - Details",
                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
                if (result == JOptionPane.NO_OPTION){
                    javax.swing.JOptionPane.showMessageDialog(null, "Passengers should be absorbed... ");
                    Application.getInstance().getGUILog().log("Passengers should be absorbed from passing Trains. Increasing the flow of the Trains, there will be more room inside passing Trains and no Passenger will remain at Stops. crease trainsMean or change trainsDistr, traisMean and trainsStandDev");
                }
            }
            /*int result = javax.swing.JOptionPane.showOptionDialog(null, 
                    "<html><style>table {font-family: arial;border-collapse: collapse;}td, th {border-right: solid 1px #000; border-left: solid 1px #000;text-align: left;padding: 2px;}</style><table><tr><th>Element</th><td>" 
                    + elm.getHumanName() 
                    + "</td></tr><tr><th>Current LoS</th><td>"
                    + elmLoS 
                    + "</td></tr><tr><th>Required LoS</th><td>"
                    + reqLoS 
                    + "</td></tr><tr><th>Non-verified Attribute</th><td>"
                    + leftAttrName 
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + leftVal
                    + "</td></tr><tr><th>Non-verified Attribute</th><td>"
                    + rightAttrName
                    + "</td></tr><tr><th>Attribute's Value</th><td>"
                    + rightVal + "</tr></html>", "'Delay' requirement is not verified! - Details",
                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
            if (result == JOptionPane.NO_OPTION){
                //javax.swing.JOptionPane.showMessageDialog(null, "You can increase the TrainsMean in Line... That way the....");
                Application.getInstance().getGUILog().log("Passengers should be absorbed from passing Trains. Increasing the flow of the Trains, there will be more room inside passing Trains and no Passenger will remain at Stops. crease trainsMean or change trainsDistr, traisMean and trainsStandDev");
            }*/
            //javax.swing.JOptionPane.showMessageDialog(null, 
                    //"Current Element is: '" + elm.getHumanName() + "'" + "\nElement's Delay LoS is: '" + elmLoS + "'" + "\nRequired Delay LoS is: '" + reqLoS + "'" + "\nThe problem is that the value of attribute '" + attrName+"' is: '" + val + "'");

        }
        catch (Exception exc)
        {
            MDLog.getGUILog().error("", exc);
            sm.cancelSession();
        }
        finally
        {
            sm.closeSession();
        }
    }
    
    /**
     * Executes the action on specified targets.
     *
     * @param annotations action targets.
     */
    public void execute(Collection<Annotation> annotations)
    {
        if (annotations == null || annotations.isEmpty())
        {
            return;
        }
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        try
        {
            //for (Annotation annotation : annotations) {
                //BaseElement baseElement = annotation.getTarget();
                //BaseElement baseElement = annotation.getTarget();
                //if (baseElement instanceof Classifier) {
                    //Classifier classifier = (Classifier) baseElement;
                    //fixJavaConstantNames(classifier);
                //}               
            //}
        }
        catch (Exception e)
        {
            MDLog.getGUILog().error("", e);
            sm.cancelSession();
        }
        finally
        {
            sm.closeSession();
        }
    }
    
    /**
     * Returns java constant name.
     *
     * @param name a name of the attribute.
     * @return java constant name.
     */
    /*private static String toJavaConstantName(String name)
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < name.length(); i++)
        {
            char charAt = name.charAt(i);
            boolean wasLower = false;
            if (Character.isLetter(charAt) && Character.isLowerCase(charAt))
            {
                builder.append(Character.toUpperCase(charAt));
                wasLower = true;
            }
            else
            {
                builder.append(charAt);
            }
            if (wasLower && i + 1 < name.length() && Character.isUpperCase(name.charAt(i + 1)))
            {
                builder.append('_');
            }
        }
        return builder.toString();
    }*/
    
    public boolean canExecute(Collection<Annotation> annotations)
    {
        return true;
    }
    
    /*
     * Pws kaleitai auti i klasi sto kanoniko validation rule
    @Override
    protected Annotation createAnnotation(Element element, Constraint constraint)
    {
        FixJavaConstantNamesAction action = new FixJavaConstantNamesAction((Classifier) element);
        return new Annotation(element, constraint, Collections.singletonList(action));
    }
     */
}
