
package newvalidationplugin;

import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.ActionsID;
import com.nomagic.magicdraw.actions.ActionsProvider;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.magicdraw.validation.SmartListenerConfigurationProvider;
import com.nomagic.magicdraw.validation.ValidationRunData;
import com.nomagic.magicdraw.validation.ValidationRunHelper;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdinterfaces.Interface;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.ext.magicdraw.mdusecases.Actor;
import com.nomagic.uml2.impl.PropertyNames;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Class;
import java.util.*;

public class NewGeneralValidationRule implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    public static final Map<String, double[]> TrainGeneralLoSSpaceMap = new HashMap<String, double[]>();
    public static final Map<String, double[]> StopGeneralLoSSpaceMap = new HashMap<String, double[]>();
    public static final ArrayList<String> TrainGeneralLoSSpaceOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
    public static final ArrayList<String> StopGeneralLoSSpaceOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
    private List<NMAction> actions;

    public NewGeneralValidationRule() {
    }

    public void init(Project project, Constraint constraint) {
    }

    /**
     * Returns a map of classes and smart listener configurations.
     *
     * @return smart listener configurations.
     */
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        SmartListenerConfig config = new SmartListenerConfig();

        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        
        return configMap;
    }

    /**
     * Executes the rule.
     *
     * @param project    a project of the constraint.
     * @param constraint constraint which defines validation rules.
     * @param elements   collection of elements that have to be validated.
     * @return a set of <code>Annotation</code> objects which specifies invalid objects.
     */
    public Set<Annotation> run(Project project, Constraint constraint, Collection<? extends Element> elements) {

        Set<Annotation> result = new HashSet<Annotation>();
        Boolean TrainGeneralLoSSpaceStatusLeft = false;
        Boolean TrainGeneralLoSSpaceStatusRight = false;
        Boolean StopGeneralLoSSpaceStatusLeft = false;
        Boolean StopGeneralLoSSpaceStatusRight = false;
        String trainFilename = "TrainLoSSpaceLimits.txt";
        String stopFilename = "StopLoSSpaceLimits.txt";
        String trainWorkingDirectory = System.getProperty("user.home");
        String stopWorkingDirectory = System.getProperty("user.home");
        String trainAbsoluteFilePath = "";
        String stopAbsoluteFilePath = "";
        trainAbsoluteFilePath = trainWorkingDirectory + File.separator + trainFilename;
        stopAbsoluteFilePath = stopWorkingDirectory + File.separator + stopFilename;
        BufferedReader trainBr = null;
        BufferedReader stopBr = null;
        FileReader trainFr = null;
        FileReader stopFr = null;
        double[] trainLimits = new double[100];
        double[] stopLimits = new double[100];

        try {
            FileReader trainFileReader = new FileReader(trainAbsoluteFilePath);
            BufferedReader trainBufferedReader = new BufferedReader(trainFileReader);
            Scanner trainS = new Scanner(trainFileReader);
            trainS.useLocale(Locale.US);
            int i = 0;
            while(trainS.hasNextDouble()){
                trainLimits[i++] = trainS.nextDouble();                             
            }
            trainBufferedReader.close();
        } catch (IOException eve) {
                eve.printStackTrace();
        } finally {
            try {
                if (trainBr != null)
                    trainBr.close();
                if (trainFr != null)
                    trainFr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try {
            FileReader stopFileReader = new FileReader(stopAbsoluteFilePath);
            BufferedReader stopBufferedReader = new BufferedReader(stopFileReader);
            Scanner stopS = new Scanner(stopFileReader);
            stopS.useLocale(Locale.US);
            int i = 0;
            while(stopS.hasNextDouble()){
                stopLimits[i++] = stopS.nextDouble();                             
            }
            stopBufferedReader.close();
        } catch (IOException eve) {
                eve.printStackTrace();
        } finally {
            try {
                if (stopBr != null)
                    stopBr.close();
                if (trainFr != null)
                    stopFr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        TrainGeneralLoSSpaceMap.put("A", new double[] {trainLimits[0], trainLimits[1]});
        TrainGeneralLoSSpaceMap.put("B", new double[] {trainLimits[2], trainLimits[3]});
        TrainGeneralLoSSpaceMap.put("C", new double[] {trainLimits[4], trainLimits[5]});
        TrainGeneralLoSSpaceMap.put("D", new double[] {trainLimits[6], trainLimits[7]});
        TrainGeneralLoSSpaceMap.put("E", new double[] {trainLimits[8], trainLimits[9]});
        TrainGeneralLoSSpaceMap.put("F", new double[] {trainLimits[10], trainLimits[11]});
        
        StopGeneralLoSSpaceMap.put("A", new double[] {stopLimits[0], stopLimits[1]});
        StopGeneralLoSSpaceMap.put("B", new double[] {stopLimits[2], stopLimits[3]});
        StopGeneralLoSSpaceMap.put("C", new double[] {stopLimits[4], stopLimits[5]});
        StopGeneralLoSSpaceMap.put("D", new double[] {stopLimits[6], stopLimits[7]});
        StopGeneralLoSSpaceMap.put("E", new double[] {stopLimits[8], stopLimits[9]});
        StopGeneralLoSSpaceMap.put("F", new double[] {stopLimits[10], stopLimits[11]});
        
        
        
        for (ActionsCategory ac : ActionsProvider.getInstance().getMainToolbarActions().getCategories()) {
            if (ac.getName().startsWith("Validation")) {
                for (NMAction ac2 : ac.getActions()) {
                    String[] ac2Name = ac2.getName().split(" ", 2);
                    if (ac2Name[0].equals("Run")) {
                        ;;
                    }
                    else {   
                        Element pack = ModelHelper.findElementWithPath("rtsCostReqValSuite");
                        ValidationRunData runData = new ValidationRunData((com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package) pack, true, Collections.emptyList(), Annotation.getSeverityLevel(project, Annotation.WARNING));
                        ValidationRunHelper.run(runData, ActionsID.VALIDATION);
                    }
                }
            }
        }
        
        
        for (Element element : elements) {
            Stereotype sterLine = StereotypesHelper.getAppliedStereotypeByString(element, "Line");
            Stereotype sterStop = StereotypesHelper.getAppliedStereotypeByString(element, "Stop");
            
            if (sterLine != null) {
                TrainGeneralLoSSpaceStatusLeft = false;
                TrainGeneralLoSSpaceStatusRight = false;
                int verFlag = 0;
                String requiredTrainGeneralLoSSpace = null;
                int requiredTrainGeneralLoSSpaceOrder = -1;
                
                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
                
                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
                    Element relEl = (Element) it.next();
                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
                    
                    if (sterVerify != null) {
                        Abstraction abs = (Abstraction) relEl;
                        Element targetEl = abs.getTarget().iterator().next();
                        Stereotype sterTrainGeneralLoSSpace = StereotypesHelper.getAppliedStereotypeByString(targetEl, "LoSComfort");
                        
                        if (sterTrainGeneralLoSSpace != null) {
                            verFlag = (int) getTagIntValue(targetEl, "intVerFlag", targetEl.getHumanType());
                            //javax.swing.JOptionPane.showMessageDialog(null, "intVerFlag is" + verFlag);
                            
                            if (verFlag == 1) {
                                requiredTrainGeneralLoSSpace = getTagStringValue(targetEl, "stringAvg", targetEl.getHumanType());
                                //javax.swing.JOptionPane.showMessageDialog(null, "requiredTrainGeneralLoSSpace is" + requiredTrainGeneralLoSSpace);
                                //requiredTrainGeneralLoSSpaceRight = getTagStringValue(targetEl, "stringAvgRight", targetEl.getHumanType());
                            } 
                            else if (verFlag == 2){
                                requiredTrainGeneralLoSSpace = getTagStringValue(targetEl, "stringWorst", targetEl.getHumanType());
                                //javax.swing.JOptionPane.showMessageDialog(null, "requiredTrainGeneralLoSSpace is" + requiredTrainGeneralLoSSpace);
                                //requiredTrainGeneralLoSSpaceRight = getTagStringValue(targetEl, "stringWorstRight", targetEl.getHumanType());
                            }
                            requiredTrainGeneralLoSSpaceOrder = TrainGeneralLoSSpaceOrderedList.indexOf(requiredTrainGeneralLoSSpace);
                            //requiredTrainGeneralLoSSpaceOrder = TrainGeneralLoSSpaceOrderedList.indexOf(requiredTrainGeneralLoSSpace);
                        }
                    }
                }
                
                String elementTrainGeneralLoSSpaceLeft = null;
                String elementTrainGeneralLoSSpaceRight = null;
                double statsMinSpacePassTrainLByTen = getTagDoubleValue(element, "StatsMinSpacePassTrainL-ByTen", element.getHumanType());
                double statsMinSpacePassTrainL = statsMinSpacePassTrainLByTen/10.0;
                double statsMinSpacePassTrainRByTen = getTagDoubleValue(element, "StatsMinSpacePassTrainR-ByTen", element.getHumanType());
                double statsMinSpacePassTrainR = statsMinSpacePassTrainRByTen/10.0;

                /*for (java.util.Iterator it = TrainGeneralLoSSpaceMap.keySet().iterator(); it.hasNext();) {
                    String currentTrainGeneralLoSSpaceLeft = it.next().toString();
                    double leftValue = TrainGeneralLoSSpaceMap.get(currentTrainGeneralLoSSpaceLeft)[0];
                    double rightValue = TrainGeneralLoSSpaceMap.get(currentTrainGeneralLoSSpaceLeft)[1];
                    
                    if ((statsMinSpacePassTrainL >= leftValue) && (statsMinSpacePassTrainL <= rightValue)) {
                      elementTrainGeneralLoSSpaceLeft = currentTrainGeneralLoSSpaceLeft;
                      break;
                    }
                }
                
                for (java.util.Iterator it = TrainGeneralLoSSpaceMap.keySet().iterator(); it.hasNext();) {
                    String currentTrainLoSSpaceRight = it.next().toString();
                    double leftValue = TrainGeneralLoSSpaceMap.get(currentTrainLoSSpaceRight)[0];
                    double rightValue = TrainGeneralLoSSpaceMap.get(currentTrainLoSSpaceRight)[1];
                    
                    if ((statsMinSpacePassTrainR >= leftValue) && (statsMinSpacePassTrainR <= rightValue)) {
                      elementTrainGeneralLoSSpaceRight = currentTrainLoSSpaceRight;
                      break;
                    }
                }*/
                
                elementTrainGeneralLoSSpaceLeft = getTagStringValue(element, "stringTrainLoSSpaceLeft", element.getHumanType());
                //javax.swing.JOptionPane.showMessageDialog(null, "elementTrainGeneralLoSSpaceLeft is " + elementTrainGeneralLoSSpaceLeft);
                elementTrainGeneralLoSSpaceRight = getTagStringValue(element, "stringTrainLoSSpaceRight", element.getHumanType());
                //javax.swing.JOptionPane.showMessageDialog(null, "elementTrainGeneralLoSSpaceRight is " + elementTrainGeneralLoSSpaceRight);
                
                if ((elementTrainGeneralLoSSpaceLeft != null) && TrainGeneralLoSSpaceOrderedList.contains(elementTrainGeneralLoSSpaceLeft)) {
                    int elementTrainGeneralLoSSpaceOrderLeft = TrainGeneralLoSSpaceOrderedList.indexOf(elementTrainGeneralLoSSpaceLeft);
                    
                    if (elementTrainGeneralLoSSpaceOrderLeft <= requiredTrainGeneralLoSSpaceOrder) {
                        //javax.swing.JOptionPane.showMessageDialog(null, "PERASEEEEEEEEEEEE1111111");
                        TrainGeneralLoSSpaceStatusLeft = true;
                    }
                }
                if ((elementTrainGeneralLoSSpaceRight != null) && TrainGeneralLoSSpaceOrderedList.contains(elementTrainGeneralLoSSpaceRight)) {
                    // Check if LoS is the required one or better
                    int elementTrainLoSSpaceOrderRight = TrainGeneralLoSSpaceOrderedList.indexOf(elementTrainGeneralLoSSpaceRight);
                    // Verification!
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Space LoS " + elementTrainLoSSpace + " and the required LoS is " + requiredTrainLoSSpace);
                    if (elementTrainLoSSpaceOrderRight <= requiredTrainGeneralLoSSpaceOrder) {
                        //javax.swing.JOptionPane.showMessageDialog(null, "PERASEEEEEEEEEEEE2222222");
                        TrainGeneralLoSSpaceStatusRight = true;
                    }
                }
              
                if ((!TrainGeneralLoSSpaceStatusLeft) && (!TrainGeneralLoSSpaceStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 0;
                    //javax.swing.JOptionPane.showMessageDialog(null, "DEN PERASEEEEEEEEEEEE1111111");
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, elementTrainGeneralLoSSpaceLeft, elementTrainGeneralLoSSpaceRight, requiredTrainGeneralLoSSpace, "StatsMinSpacePassTrainL-ByTen", "StatsMinSpacePassTrainR-ByTen", statsMinSpacePassTrainL, statsMinSpacePassTrainR, flag);
                    actions.add(actionGeneralSpaceLeft);
                    
                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationGeneralSpaceLeft);
                }
                else if (!TrainGeneralLoSSpaceStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 1;
                    //javax.swing.JOptionPane.showMessageDialog(null, "DEN PERASEEEEEEEEEEEE3333333");
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, elementTrainGeneralLoSSpaceLeft, null, requiredTrainGeneralLoSSpace, "StatsMinSpacePassTrainL-ByTen", null, statsMinSpacePassTrainL, 0.0, flag);
                    actions.add(actionGeneralSpaceLeft);
                    
                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationGeneralSpaceLeft);
                }
                else if (!TrainGeneralLoSSpaceStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 2;
                    //javax.swing.JOptionPane.showMessageDialog(null, "DEN PERASEEEEEEEEEEEE222222");
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, null, elementTrainGeneralLoSSpaceRight, requiredTrainGeneralLoSSpace, null, "StatsMinSpacePassTrainL-ByTen", 0.0, statsMinSpacePassTrainR, flag);
                    actions.add(actionGeneralSpaceLeft);
                    
                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationGeneralSpaceLeft);
                }
            }
    
//            if (sterStop != null) {
//                //javax.swing.JOptionPane.showMessageDialog(null, "mpika");
//                StopGeneralLoSSpaceStatusLeft = false;
//                StopGeneralLoSSpaceStatusRight = false;
//                int stopFlag = 0;
//                String requiredStopGeneralLoSSpace = null;
//                int requiredStopGeneralLoSSpaceOrder = -1;
//                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
//                
//                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
//                    Element relEl = (Element) it.next();
//                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
//                    
//                    if (sterVerify != null) {
//                        Abstraction abs = (Abstraction) relEl;
//                        Element targetEl = abs.getTarget().iterator().next();
//                        Stereotype sterStopGeneralLoSSpace = StereotypesHelper.getAppliedStereotypeByString(targetEl, "LoSComfort");
//                        
//                        if (sterStopGeneralLoSSpace != null) {
//                            if (stopFlag == 1) {
//                                requiredStopGeneralLoSSpace = getTagStringValue(targetEl, "AvgLoS-Comfort", targetEl.getHumanType());
//                            }
//                            else {
//                                requiredStopGeneralLoSSpace = getTagStringValue(targetEl, "MinLoS-Comfort", targetEl.getHumanType());
//                            }
//                            requiredStopGeneralLoSSpaceOrder = StopGeneralLoSSpaceOrderedList.indexOf(requiredStopGeneralLoSSpace);
//                        }
//                    }
//                }
//                
//                String elementStopGeneralLoSSpaceLeft = null;
//                String elementStopGeneralLoSSpaceRight = null;
//                double statsMinSpacePassStopLByTen = getTagDoubleValue(element, "StatsMinSpacePassStopL-ByTen", element.getHumanType());
//                double statsMinSpacePassStopL = statsMinSpacePassStopLByTen/10.0;
//                double statsMinSpacePassStopRByTen = getTagDoubleValue(element, "StatsMinSpacePassStopR-ByTen", element.getHumanType());
//                double statsMinSpacePassStopR = statsMinSpacePassStopRByTen/10.0;
//                
//                for (java.util.Iterator it = StopGeneralLoSSpaceMap.keySet().iterator(); it.hasNext();) {
//                    String currentStopGeneralLoSSpaceLeft = it.next().toString();
//                    double leftValue = StopGeneralLoSSpaceMap.get(currentStopGeneralLoSSpaceLeft)[0];
//                    double rightValue = StopGeneralLoSSpaceMap.get(currentStopGeneralLoSSpaceLeft)[1];
//                    
//                    if ((statsMinSpacePassStopL >= leftValue) && (statsMinSpacePassStopL <= rightValue)) {
//                      elementStopGeneralLoSSpaceLeft = currentStopGeneralLoSSpaceLeft;
//                      break;
//                    }
//                }
//                for (java.util.Iterator it = StopGeneralLoSSpaceMap.keySet().iterator(); it.hasNext();) {
//                    String currentStopGeneralLoSSpaceRight = it.next().toString();
//                    double leftValue = StopGeneralLoSSpaceMap.get(currentStopGeneralLoSSpaceRight)[0];
//                    double rightValue = StopGeneralLoSSpaceMap.get(currentStopGeneralLoSSpaceRight)[1];
//                    if ((statsMinSpacePassStopR >= leftValue) && (statsMinSpacePassStopR <= rightValue)) {
//                      elementStopGeneralLoSSpaceRight = currentStopGeneralLoSSpaceRight;
//                      break;
//                    }
//                }
//                   
//                if ((elementStopGeneralLoSSpaceLeft != null) && StopGeneralLoSSpaceOrderedList.contains(elementStopGeneralLoSSpaceLeft)) {
//                    // Check if LoS is the required one or better
//                    int elementGeneralLoSSpaceOrderLeft = StopGeneralLoSSpaceOrderedList.indexOf(elementStopGeneralLoSSpaceLeft);
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has LoS " + elementStopLoSSpace + " and the required LoS is " + requiredStopLoSSpace);
//                    if (elementGeneralLoSSpaceOrderLeft <= requiredStopGeneralLoSSpaceOrder) {
//                        StopGeneralLoSSpaceStatusLeft = true;
//                    }
//                }
//                if ((elementStopGeneralLoSSpaceRight != null) && StopGeneralLoSSpaceOrderedList.contains(elementStopGeneralLoSSpaceRight)) {
//                    // Check if LoS is the required one or better
//                    int elementLoSSpaceOrderRight = StopGeneralLoSSpaceOrderedList.indexOf(elementStopGeneralLoSSpaceRight);
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has LoS " + elementStopLoSSpace + " and the required LoS is " + requiredStopLoSSpace);
//                    if (elementLoSSpaceOrderRight <= requiredStopGeneralLoSSpaceOrder) {
//                        StopGeneralLoSSpaceStatusRight = true;
//                    }
//                }
//                
//                if ((!StopGeneralLoSSpaceStatusLeft) && (!StopGeneralLoSSpaceStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 0;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, elementStopGeneralLoSSpaceLeft, elementStopGeneralLoSSpaceRight, requiredStopGeneralLoSSpace, "StatsMinSpacePassStopL-ByTen", "StatsMinSpacePassStopR-ByTen", statsMinSpacePassStopL, statsMinSpacePassStopR, flag);
//                    actions.add(actionGeneralSpaceLeft);
//                    
//                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationGeneralSpaceLeft);
//                }
//                else if (!StopGeneralLoSSpaceStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 1;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, elementStopGeneralLoSSpaceLeft, null, requiredStopGeneralLoSSpace, "StatsMinSpacePassStopL-ByTen", null, statsMinSpacePassStopL, null, flag);
//                    actions.add(actionGeneralSpaceLeft);
//                    
//                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationGeneralSpaceLeft);
//                }
//                else if (!StopGeneralLoSSpaceStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 2;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionGeneralSpaceLeft = new ActionGeneralSpace(element, null, elementStopGeneralLoSSpaceRight, requiredStopGeneralLoSSpace, null, "StatsMinSpacePassStopR-ByTen", null, statsMinSpacePassStopR, flag);
//                    actions.add(actionGeneralSpaceLeft);
//                    
//                    Annotation annotationGeneralSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationGeneralSpaceLeft);
//                }
//            }
        }

        return result;
    }
    
    /**
     * 
     */
    public void dispose() {
    }
    
    double getTagDoubleValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        double ret = -1.0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Double.parseDouble(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    double getTagIntValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        int ret = 0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Integer.parseInt(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    String getTagStringValue(Element elm, String name, String type) { //gets the string tag value of an element <elm> of name <name> of stereotype <type>
        String ret = null;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = value.get(0).toString();
                break;
            }
        }
        return ret;
    }
}
