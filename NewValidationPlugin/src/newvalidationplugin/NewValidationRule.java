/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newvalidationplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.validation.ElementValidationRuleImpl;
import com.nomagic.magicdraw.validation.SmartListenerConfigurationProvider;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.smartlistener.SmartListenerConfig;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdinterfaces.Interface;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.ext.magicdraw.mdusecases.Actor;
import com.nomagic.uml2.impl.PropertyNames;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Class;
import java.util.*;

/**
 *
 * @author Chris
 */

public class NewValidationRule implements ElementValidationRuleImpl, SmartListenerConfigurationProvider {

    // Initialize LoS mappings
    //for Trains
    //public static final Map<String, double[]> TrainLoSSpaceMap = Collections.unmodifiableMap(
    //new HashMap<String, double[]>() {{
        //put("A", new double[] {1.5, 3.0});
        //put("B", new double[] {0.1, 1.49});
    //}});
    
    public static final Map<String, double[]> TrainLoSSpaceMap = new HashMap<String, double[]>();
    
    //for Trains
    /*public static final Map<String, double[]> TrainLoSDelayMap = Collections.unmodifiableMap(
    new HashMap<String, double[]>() {{
        put("A", new double[] {1.0, 3.0});
        put("B", new double[] {3.0, 6.0});
    }});
    
    //for Trains
    public static final Map<String, int[]> TrainLoSTrainsPerHourMap = Collections.unmodifiableMap(
    new HashMap<String, int[]>() {{
        put("A", new int[] {7, 8});
       put("B", new int[] {3, 6});
    }});*/
    
    //for Stops
    //public static final Map<String, double[]> StopLoSSpaceMap = Collections.unmodifiableMap(
    //new HashMap<String, double[]>() {{
        //put("A", new double[] {1.5, 3.0});
        //put("B", new double[] {0.1, 1.49});
    //}});
    
    public static final Map<String, double[]> StopLoSSpaceMap = new HashMap<String, double[]>();
    
    public static final ArrayList<String> TrainLoSSpaceOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
    public static final ArrayList<String> StopLoSSpaceOrderedList = new ArrayList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
    //public static final ArrayList<String> LoSDelayOrderedList = new ArrayList<String>(Arrays.asList("A", "B"));
    //public static final ArrayList<String> LoSTrainsPerHourOrderedList = new ArrayList<String>(Arrays.asList("A", "B"));
    
    /**
     * Solving actions.
     */
    private List<NMAction> actions;
    private List<NMAction> actions2;

    /**
     * Default constructor. Default constructor is required.
     */
    public NewValidationRule() { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //mActions = new ArrayList<NMAction>();
        //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
        //NMAction action = new SpaceAction("REQUIREMENT_IS_NOT_VERIFIED", "Requirement_is_not_verified", null);
        //mActions.add(action);
    }

    public void init(Project project, Constraint constraint) {
    }

    /**
     * Returns a map of classes and smart listener configurations.
     *
     * @return smart listener configurations.
     */
    public Map<Class<? extends Element>, Collection<SmartListenerConfig>> getListenerConfigurations() {
        
        // initialize hasmap of smartlistener configurations
        Map<Class<? extends Element>, Collection<SmartListenerConfig>> configMap = new HashMap<Class<? extends Element>, Collection<SmartListenerConfig>>();
        Collection<SmartListenerConfig> configsForElement = new ArrayList<SmartListenerConfig>();
        
        // listen to the value of the named field
        SmartListenerConfig config = new SmartListenerConfig();
        // TODO: check if we should listen to VALUE or TAG_VALUE
        config.listenToNested(PropertyNames.PACKAGED_ELEMENT).listenTo(PropertyNames.OWNED_ATTRIBUTE).listenTo(PropertyNames.VALUE);
        configsForElement.add(config);
        
        //SmartListenerConfig nested = config.listenToNested("ownedOperation");
        //config.listenTo(PropertyNames.OWNER);
        //nested.listenTo("name");
        //nested.listenTo("ownedParameter");

        
        //Collection<SmartListenerConfig> configs = Collections.singletonList(config);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class, configs);
        //configMap.put(com.nomagic.uml2.ext.magicdraw.mdusecases.Actor.class, configs);
        configMap.put(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class.class , configsForElement);
        //configMap.put(Interface.class, configs);
        
        return configMap;
    }

    /**
     * Executes the rule.
     *
     * @param project    a project of the constraint.
     * @param constraint constraint which defines validation rules.
     * @param elements   collection of elements that have to be validated.
     * @return a set of <code>Annotation</code> objects which specifies invalid objects.
     */
    public Set<Annotation> run(Project project, Constraint constraint, Collection<? extends Element> elements) {

        // javax.swing.JOptionPane.showMessageDialog(null, "checking");
        //javax.swing.JOptionPane.showMessageDialog(null, "checking element annotations");
        Set<Annotation> result = new HashSet<Annotation>();
        
        Boolean TrainLoSSpaceStatusLeft = false;
        Boolean TrainLoSSpaceStatusRight = false;
        Boolean StopLoSSpaceStatusLeft = false;
        Boolean StopLoSSpaceStatusRight = false;
        //Boolean LoSDelayStatus = false;
        //Boolean LoSTrainsPerHourStatus = false;
        
        String trainFilename = "TrainLoSSpaceLimits.txt";
        String stopFilename = "StopLoSSpaceLimits.txt";
        //new code
        //String loSComfortFilename = "LoSComfort.txt";
        //String leftTrainLoSComfortFilename = "leftTrainLoSComfort.txt";
        //String rightTrainLoSComfortFilename = "rightTrainLoSComfort.txt";
        //String leftStopLoSComfortFilename = "leftStopLoSComfort.txt";
        //String rightStopLoSComfortFilename = "rightStopLoSComfort.txt";
        
        String trainWorkingDirectory = System.getProperty("user.home");
        String stopWorkingDirectory = System.getProperty("user.home");
        //new code
        //String loSComfortWorkingDirectory = System.getProperty("user.home");
        //String leftTrainLoSComfortWorkingDirectory = System.getProperty("user.home");
        //String rightTrainLoSComfortWorkingDirectory = System.getProperty("user.home");
        //String leftStopLoSComfortWorkingDirectory = System.getProperty("user.home");
        //String rightStopLoSComfortWorkingDirectory = System.getProperty("user.home");
        
        String trainAbsoluteFilePath = "";
        String stopAbsoluteFilePath = "";
        //new code
        //String loSComfortAbsoluteFilePath = "";
        //String leftTrainLoSComfortAbsoluteFilePath = "";
        //String rightTrainLoSComfortAbsoluteFilePath = "";
        //String leftStopLoSComfortAbsoluteFilePath = "";
        //String rightStopLoSComfortAbsoluteFilePath = "";
        
        trainAbsoluteFilePath = trainWorkingDirectory + File.separator + trainFilename;
        stopAbsoluteFilePath = stopWorkingDirectory + File.separator + stopFilename;
        //new code
        //loSComfortAbsoluteFilePath = loSComfortWorkingDirectory + File.separator + loSComfortFilename;
        //leftTrainLoSComfortAbsoluteFilePath = leftTrainLoSComfortWorkingDirectory + File.separator + leftTrainLoSComfortFilename;
        //rightTrainLoSComfortAbsoluteFilePath = rightTrainLoSComfortWorkingDirectory + File.separator + rightTrainLoSComfortFilename;
        //leftStopLoSComfortAbsoluteFilePath = leftStopLoSComfortWorkingDirectory + File.separator + leftStopLoSComfortFilename;
        //rightStopLoSComfortAbsoluteFilePath = rightStopLoSComfortWorkingDirectory + File.separator + rightStopLoSComfortFilename;
        
        BufferedReader trainBr = null;
        BufferedReader stopBr = null;
        //new code
        //BufferedReader loSComfortBr = null;
        //BufferedReader leftTrainLoSComfortBr = null;
        //BufferedReader rightTrainLoSComfortBr = null;
        //BufferedReader leftStopLoSComfortBr = null;
        //BufferedReader rightStopLoSComfortBr = null;
        FileReader trainFr = null;
        FileReader stopFr = null;
        //new code
        //FileReader loSComfortFr = null;
        //FileReader leftTrainLoSComfortFr = null;
        //FileReader rightTrainLoSComfortFr = null;
        //FileReader leftStopLoSComfortFr = null;
        //FileReader rightStopLoSComfortFr = null;
        
        //new code
        //File loSComfortTemp = new File(loSComfortAbsoluteFilePath);
        //File leftTrainLoSComfortTemp = new File(leftTrainLoSComfortAbsoluteFilePath);
        //File rightTrainLoSComfortTemp = new File(rightTrainLoSComfortAbsoluteFilePath);
        //File leftStopLoSComfortTemp = new File(leftStopLoSComfortAbsoluteFilePath);
        //File rightStopLoSComfortTemp = new File(rightStopLoSComfortAbsoluteFilePath);
        //loSComfortTemp.delete();

        double[] trainLimits = new double[100];
        double[] stopLimits = new double[100];

        try {
            FileReader trainFileReader = new FileReader(trainAbsoluteFilePath);
            BufferedReader trainBufferedReader = new BufferedReader(trainFileReader);
            Scanner trainS = new Scanner(trainFileReader);
            trainS.useLocale(Locale.US);
            //Loop through the file if there is a next int to process it will continue
            int i = 0;
            while(trainS.hasNextDouble()){
                //We store every int we read in the array
                trainLimits[i++] = trainS.nextDouble();                             
            }
            trainBufferedReader.close();
            //javax.swing.JOptionPane.showMessageDialog(null, content);
        } catch (IOException eve) {
                eve.printStackTrace();
        } finally {
            try {
                if (trainBr != null)
                        trainBr.close();
                if (trainFr != null)
                        trainFr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try {
            FileReader stopFileReader = new FileReader(stopAbsoluteFilePath);
            BufferedReader stopBufferedReader = new BufferedReader(stopFileReader);
            Scanner stopS = new Scanner(stopFileReader);
            stopS.useLocale(Locale.US);
            //Loop through the file if there is a next int to process it will continue
            int i = 0;
            while(stopS.hasNextDouble()){
                //We store every int we read in the array
                stopLimits[i++] = stopS.nextDouble();                             
            }
            stopBufferedReader.close();
            //javax.swing.JOptionPane.showMessageDialog(null, content);
        } catch (IOException eve) {
                eve.printStackTrace();
        } finally {
            try {
                if (stopBr != null)
                        stopBr.close();
                if (stopFr != null)
                        stopFr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
//        try {
//            FileReader stopFileReader = new FileReader(stopAbsoluteFilePath);
//            BufferedReader stopBufferedReader = new BufferedReader(stopFileReader);
//            Scanner stopS = new Scanner(stopFileReader);
//            stopS.useLocale(Locale.US);
//            //Loop through the file if there is a next int to process it will continue
//            int i = 0;
//            while(stopS.hasNextDouble()){
//                //We store every int we read in the array
//                stopLimits[i++] = stopS.nextDouble();                             
//            }
//            stopBufferedReader.close();
//            //javax.swing.JOptionPane.showMessageDialog(null, content);
//        } catch (IOException eve) {
//                eve.printStackTrace();
//        } finally {
//            try {
//                if (stopBr != null)
//                        stopBr.close();
//                if (trainFr != null)
//                        stopFr.close();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
        
        TrainLoSSpaceMap.put("A", new double[] {trainLimits[0], trainLimits[1]});
        TrainLoSSpaceMap.put("B", new double[] {trainLimits[2], trainLimits[3]});
        TrainLoSSpaceMap.put("C", new double[] {trainLimits[4], trainLimits[5]});
        TrainLoSSpaceMap.put("D", new double[] {trainLimits[6], trainLimits[7]});
        TrainLoSSpaceMap.put("E", new double[] {trainLimits[8], trainLimits[9]});
        TrainLoSSpaceMap.put("F", new double[] {trainLimits[10], trainLimits[11]});
        
        StopLoSSpaceMap.put("A", new double[] {stopLimits[0], stopLimits[1]});
        StopLoSSpaceMap.put("B", new double[] {stopLimits[2], stopLimits[3]});
        StopLoSSpaceMap.put("C", new double[] {stopLimits[4], stopLimits[5]});
        StopLoSSpaceMap.put("D", new double[] {stopLimits[6], stopLimits[7]});
        StopLoSSpaceMap.put("E", new double[] {stopLimits[8], stopLimits[9]});
        StopLoSSpaceMap.put("F", new double[] {stopLimits[10], stopLimits[11]});
        
        // for loop is for calculating and checking the LoS per affected element
        for (Element element : elements) {
            
            Stereotype sterLine = StereotypesHelper.getAppliedStereotypeByString(element, "Line");
            Stereotype sterStop = StereotypesHelper.getAppliedStereotypeByString(element, "Stop");
            //javax.swing.JOptionPane.showMessageDialog(null, "current element in second loop name=" + element.getHumanName() +" type=" + element.getHumanType());
            
            if (sterLine != null) {
                
                TrainLoSSpaceStatusLeft = false;
                TrainLoSSpaceStatusRight = false;
                
                //javax.swing.JOptionPane.showMessageDialog(null, "Constraint is: " + constraint.getName());
                
                // first, calculate the required LoS for this Line based on the relation
                String requiredTrainLoSSpace = null;
                int requiredTrainLoSSpaceOrder = -1;
                //String requiredLoSDelay = null;
                //int requiredLoSDelayOrder = -1;
                //String requiredLoSTrainsPerHour = null;
                //int requiredLoSTrainsPerHourOrder = -1;
               
                // retrieve all directed relationships with source the to-be-verified element
                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
                
                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
                    // get the next relationship element
                    Element relEl = (Element) it.next();
                    
                    // check if the relationship if of the verify type
                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
                    if (sterVerify != null) {
                        Abstraction abs = (Abstraction) relEl;
                        
                        // get the target element of the directed verify relationship
                        Element targetEl = abs.getTarget().iterator().next();
                        
                        // check if it is the correct stereotype
                        Stereotype sterTrainLoSSpace = StereotypesHelper.getAppliedStereotypeByString(targetEl, "TrainLoSSpace");
                        //Stereotype sterTrainLoSDelay = StereotypesHelper.getAppliedStereotypeByString(targetEl, "TrainLoSDelay");
                        //Stereotype sterTrainLoSTrainsPerHour = StereotypesHelper.getAppliedStereotypeByString(targetEl, "TrainLoSTrainsPerHour");
                        if (sterTrainLoSSpace != null) {
                            
                            requiredTrainLoSSpace = getTagStringValue(targetEl, "TrainLoS-Space", targetEl.getHumanType());
                            //javax.swing.JOptionPane.showMessageDialog(null, "EIEIIII element " + targetEl.getHumanName() +" LoS=" + requiredLoSSpace);
                            requiredTrainLoSSpaceOrder = TrainLoSSpaceOrderedList.indexOf(requiredTrainLoSSpace);
                        }
                        
                        /*if (sterTrainLoSDelay != null) {
                            requiredLoSDelay = getTagStringValue(targetEl, "TrainLoS-Delay", targetEl.getHumanType());
                            javax.swing.JOptionPane.showMessageDialog(null, "EIEIIII2 element " + targetEl.getHumanName() +" LoS=" + requiredLoSDelay);
                            requiredLoSDelayOrder = LoSDelayOrderedList.indexOf(requiredLoSDelay);
                            //javax.swing.JOptionPane.showMessageDialog(null, "Required LoS Delay is " + requiredLoSDelayOrder);
                        }
                        if (sterTrainLoSTrainsPerHour != null) {
                            requiredLoSTrainsPerHour = getTagStringValue(targetEl, "TrainLoS-TrainsPerHour", targetEl.getHumanType());
                            javax.swing.JOptionPane.showMessageDialog(null, "EIEIIII3 element " + targetEl.getHumanName() +" LoS=" + requiredLoSTrainsPerHour);
                            requiredLoSTrainsPerHourOrder = LoSTrainsPerHourOrderedList.indexOf(requiredLoSTrainsPerHour);
                        }*/
                    }
                }
                
                String elementTrainLoSSpaceLeft = null;
                String elementTrainLoSSpaceRight = null;
                
                //String elementLoSDelay = null;
                //String elementLoSTrainsPerHour = null;
                //javax.swing.JOptionPane.showMessageDialog(null, "Retrieving double tag value of " + element.getHumanName());
                
                double statsMinSpacePassTrainLByTen = getTagDoubleValue(element, "StatsMinSpacePassTrainL-ByTen", element.getHumanType());
                double statsMinSpacePassTrainL = statsMinSpacePassTrainLByTen/10.0;
                
                //new attribute ````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                double statsMinSpacePassTrainRByTen = getTagDoubleValue(element, "StatsMinSpacePassTrainR-ByTen", element.getHumanType());
                double statsMinSpacePassTrainR = statsMinSpacePassTrainRByTen/10.0;
                
                //double statsAvgPassengerDelayOnLineL = getTagDoubleValue(element, "StatsAvgPassengerDelayOnLineL", element.getHumanType());
                //double statsAvgTrainsPerHourL = getTagIntValue(element, "StatsAvgTrainsPerHourL", element.getHumanType());
     
                // Calculate the actual LoS of the element
                /*for (java.util.Iterator it = TrainLoSSpaceMap.keySet().iterator(); it.hasNext();) {
                    String currentTrainLoSSpaceLeft = it.next().toString();
                    double leftValue = TrainLoSSpaceMap.get(currentTrainLoSSpaceLeft)[0];
                    double rightValue = TrainLoSSpaceMap.get(currentTrainLoSSpaceLeft)[1];
                    if ((statsMinSpacePassTrainL >= leftValue) && (statsMinSpacePassTrainL <= rightValue)) {
                      elementTrainLoSSpaceLeft = currentTrainLoSSpaceLeft;
                      break;
                    }
                }
                
                for (java.util.Iterator it = TrainLoSSpaceMap.keySet().iterator(); it.hasNext();) {
                    String currentTrainLoSSpaceRight = it.next().toString();
                    double leftValue = TrainLoSSpaceMap.get(currentTrainLoSSpaceRight)[0];
                    double rightValue = TrainLoSSpaceMap.get(currentTrainLoSSpaceRight)[1];
                    if ((statsMinSpacePassTrainR >= leftValue) && (statsMinSpacePassTrainR <= rightValue)) {
                      elementTrainLoSSpaceRight = currentTrainLoSSpaceRight;
                      break;
                    }
                }*/
                
                /*for (java.util.Iterator it = TrainLoSDelayMap.keySet().iterator(); it.hasNext();) {
                    String currentLoSDelay = it.next().toString();
                    double leftValue = TrainLoSDelayMap.get(currentLoSDelay)[0];
                    double rightValue = TrainLoSDelayMap.get(currentLoSDelay)[1];
                    if ((statsAvgPassengerDelayOnLineL >= leftValue) && (statsAvgPassengerDelayOnLineL <= rightValue)) {
                      elementLoSDelay = currentLoSDelay;
                      //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay is " + elementLoSDelay);
                      break;
                    }
                }
                for (java.util.Iterator it = TrainLoSTrainsPerHourMap.keySet().iterator(); it.hasNext();) {
                    String currentLoSTrainsPerHour = it.next().toString();
                    double leftValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHour)[0];
                    double rightValue = TrainLoSTrainsPerHourMap.get(currentLoSTrainsPerHour)[1];
                    if ((statsAvgTrainsPerHourL >= leftValue) && (statsAvgTrainsPerHourL <= rightValue)) {
                      elementLoSTrainsPerHour = currentLoSTrainsPerHour;
                      break;
                    }
                }*/
                    
                elementTrainLoSSpaceLeft = getTagStringValue(element, "stringTrainLoSSpaceLeft", element.getHumanType());
                //javax.swing.JOptionPane.showMessageDialog(null, "elementTrainLoSSpaceLeft is " + elementTrainLoSSpaceLeft);
                elementTrainLoSSpaceRight = getTagStringValue(element, "stringTrainLoSSpaceRight", element.getHumanType());
                //javax.swing.JOptionPane.showMessageDialog(null, "elementTrainLoSSpaceRight is " + elementTrainLoSSpaceRight);
                
                if ((elementTrainLoSSpaceLeft != null) && TrainLoSSpaceOrderedList.contains(elementTrainLoSSpaceLeft)) {
                    // Check if LoS is the required one or better
                    
                    
                    //new code
                    /*try{
                        String content = element.getHumanName().replaceAll(" ", "") + "," + "left" + "," + elementTrainLoSSpaceLeft + "\n"; 
                        FileOutputStream fos = new FileOutputStream(loSComfortTemp, true);
                        byte[] contentInBytes = content.getBytes();

			fos.write(contentInBytes);
			fos.flush();
                        fos.close();
                    }catch(IOException ev){
                        ev.printStackTrace();
                    }*/
                    
                    
                    int elementTrainLoSSpaceOrderLeft = TrainLoSSpaceOrderedList.indexOf(elementTrainLoSSpaceLeft);
                    
                    // Verification!
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Space LoS " + elementTrainLoSSpace + " and the required LoS is " + requiredTrainLoSSpace);
                    if (elementTrainLoSSpaceOrderLeft <= requiredTrainLoSSpaceOrder) {
                        TrainLoSSpaceStatusLeft = true;
                    }
                }
                if ((elementTrainLoSSpaceRight != null) && TrainLoSSpaceOrderedList.contains(elementTrainLoSSpaceRight)) {
                    // Check if LoS is the required one or better
                    
                    //new code
                    /*try{
                        String content = element.getHumanName().replaceAll(" ", "") + "," + "right" + "," + elementTrainLoSSpaceRight + "\n"; 
                        FileOutputStream fos = new FileOutputStream(loSComfortTemp, true);
                        byte[] contentInBytes = content.getBytes();

			fos.write(contentInBytes);
			fos.flush();
                        fos.close();
                    }catch(IOException ev){
                        ev.printStackTrace();
                    }*/
                    
                    
                    int elementTrainLoSSpaceOrderRight = TrainLoSSpaceOrderedList.indexOf(elementTrainLoSSpaceRight);
                    
                    // Verification!
                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Space LoS " + elementTrainLoSSpace + " and the required LoS is " + requiredTrainLoSSpace);
                    if (elementTrainLoSSpaceOrderRight <= requiredTrainLoSSpaceOrder) {
                        TrainLoSSpaceStatusRight = true;
                    }
                }
                
                //javax.swing.JOptionPane.showMessageDialog(null, "TrainLoSSpaceStatusLeft is " + TrainLoSSpaceStatusLeft);
                //javax.swing.JOptionPane.showMessageDialog(null, "TrainLoSSpaceStatusRight is " + TrainLoSSpaceStatusRight);
                
                /*if ((elementLoSDelay != null) && LoSDelayOrderedList.contains(elementLoSDelay)) {
                    // Check if LoS is the required one or better
                    int elementLoSDelayOrder = LoSDelayOrderedList.indexOf(elementLoSDelay);
                    //javax.swing.JOptionPane.showMessageDialog(null, "Element LoS Delay Order is " + elementLoSDelay);
                    // Verification!
                    javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has Delay LoS " + elementLoSDelay + " and the required LoS is " + requiredLoSDelay);//requiredLoSDelay);
                    if (elementLoSDelayOrder <= requiredLoSDelayOrder) {
                        LoSDelayStatus = true;
                    }
                }
                if ((elementLoSTrainsPerHour != null) && LoSTrainsPerHourOrderedList.contains(elementLoSTrainsPerHour)) {
                    // Check if LoS is the required one or better
                    int elementLoSTrainsPerHourOrder = LoSTrainsPerHourOrderedList.indexOf(elementLoSTrainsPerHour);
                    
                    // Verification!
                    javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has TPH LoS " + elementLoSTrainsPerHour + " and the required LoS is " + requiredLoSTrainsPerHour);
                    if (elementLoSTrainsPerHourOrder <= requiredLoSTrainsPerHourOrder) {
                        LoSTrainsPerHourStatus = true;
                    }
                }*/
              
                if ((!TrainLoSSpaceStatusLeft) && (!TrainLoSSpaceStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 0;
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionSpaceLeft = new ActionSpace(element, elementTrainLoSSpaceLeft, elementTrainLoSSpaceRight, requiredTrainLoSSpace, "StatsMinSpacePassTrainL-ByTen", "StatsMinSpacePassTrainR-ByTen", statsMinSpacePassTrainL, statsMinSpacePassTrainR, flag);
                    actions.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationSpaceLeft);
                }
                else if (!TrainLoSSpaceStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 1;
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionSpaceLeft = new ActionSpace(element, elementTrainLoSSpaceLeft, null, requiredTrainLoSSpace, "StatsMinSpacePassTrainL-ByTen", null, statsMinSpacePassTrainL, 0.0, flag);
                    actions.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationSpaceLeft);
                }
                else if (!TrainLoSSpaceStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    int flag = 2;
                    actions = new ArrayList<NMAction>();
                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
                    NMAction actionSpaceLeft = new ActionSpace(element, null, elementTrainLoSSpaceRight, requiredTrainLoSSpace, null, "StatsMinSpacePassTrainL-ByTen", 0.0, statsMinSpacePassTrainR, flag);
                    actions.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
                    result.add(annotationSpaceLeft);
                }
            }
    
            
            if (sterStop != null) {
                StopLoSSpaceStatusLeft = false;
                StopLoSSpaceStatusRight = false;
                
                String requiredStopLoSSpace = null;
                int requiredStopLoSSpaceOrder = -1;
                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
                //javax.swing.JOptionPane.showMessageDialog(null, "mpika stop");
                
                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
                    Element relEl = (Element) it.next();
                    //javax.swing.JOptionPane.showMessageDialog(null, relEl.getHumanName());
                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
                    if (sterVerify != null) {
                        Abstraction abs = (Abstraction) relEl;
                        
                        Element targetEl = abs.getTarget().iterator().next();
                        //javax.swing.JOptionPane.showMessageDialog(null, targetEl.getHumanName());
                        Stereotype sterStopLoSSpace = StereotypesHelper.getAppliedStereotypeByString(targetEl, "StopLoSSpace");
                        if (sterStopLoSSpace != null) {
                            
                            requiredStopLoSSpace = getTagStringValue(targetEl, "StopLoS-Space", targetEl.getHumanType());
                            requiredStopLoSSpaceOrder = StopLoSSpaceOrderedList.indexOf(requiredStopLoSSpace);
                        }
                    }
                }
                
                String elementStopLoSSpaceLeft = null;
                String elementStopLoSSpaceRight = null;
                double statsMinSpacePassStopLByTen = getTagDoubleValue(element, "StatsMinSpacePassStopL-ByTen", element.getHumanType());
                double statsMinSpacePassStopL = statsMinSpacePassStopLByTen/10.0;
                
                double statsMinSpacePassStopRByTen = getTagDoubleValue(element, "StatsMinSpacePassStopR-ByTen", element.getHumanType());
                double statsMinSpacePassStopR = statsMinSpacePassStopRByTen/10.0; 
                elementStopLoSSpaceLeft = getTagStringValue(element, "stringStopLoSSpaceLeft", element.getHumanType());
                elementStopLoSSpaceRight = getTagStringValue(element, "stringStopLoSSpaceRight", element.getHumanType());
                
                if ((elementStopLoSSpaceLeft != null) && StopLoSSpaceOrderedList.contains(elementStopLoSSpaceLeft)) {
                    int elementStopLoSSpaceOrderLeft = StopLoSSpaceOrderedList.indexOf(elementStopLoSSpaceLeft);
                    if (elementStopLoSSpaceOrderLeft <= requiredStopLoSSpaceOrder) {
                        StopLoSSpaceStatusLeft = true;
                    }
                }
                if ((elementStopLoSSpaceRight != null) && StopLoSSpaceOrderedList.contains(elementStopLoSSpaceRight)) {
                    
                    int elementStopLoSSpaceOrderRight = StopLoSSpaceOrderedList.indexOf(elementStopLoSSpaceRight);
                    
                    if (elementStopLoSSpaceOrderRight <= requiredStopLoSSpaceOrder) {
                        StopLoSSpaceStatusRight = true;
                    }
                }
                
                //javax.swing.JOptionPane.showMessageDialog(null, StopLoSSpaceStatusLeft + " | " + StopLoSSpaceStatusRight);
              
                if ((!StopLoSSpaceStatusLeft) && (!StopLoSSpaceStatusRight)) { 
                    int flag = 0;
                    actions2 = new ArrayList<NMAction>();
                    NMAction actionSpaceLeft = new ActionSpace(element, elementStopLoSSpaceLeft, elementStopLoSSpaceRight, requiredStopLoSSpace, "StatsMinSpacePassStopL-ByTen", "StatsMinSpacePassStopR-ByTen", statsMinSpacePassStopL, statsMinSpacePassStopR, flag);
                    actions2.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions2);
                    result.add(annotationSpaceLeft);
                }
                else if (!StopLoSSpaceStatusLeft) { 
                    int flag = 1;
                    actions2 = new ArrayList<NMAction>();
                    NMAction actionSpaceLeft = new ActionSpace(element, elementStopLoSSpaceLeft, null, requiredStopLoSSpace, "StatsMinSpacePassStopL-ByTen", null, statsMinSpacePassStopL, 0.0, flag);
                    actions2.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions2);
                    result.add(annotationSpaceLeft);
                }
                else if (!StopLoSSpaceStatusRight) { 
                    int flag = 2;
                    actions2 = new ArrayList<NMAction>();
                    NMAction actionSpaceLeft = new ActionSpace(element, null, elementStopLoSSpaceRight, requiredStopLoSSpace, null, "StatsMinSpacePassStopR-ByTen", 0.0, statsMinSpacePassStopR, flag);
                    actions2.add(actionSpaceLeft);
                    
                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions2);
                    result.add(annotationSpaceLeft);
                }
            }
            
//            if (sterStop != null) {
//                
//                StopLoSSpaceStatusLeft = false;
//                StopLoSSpaceStatusRight = false;
//                // first, calculate the required LoS for this Line based on the relation
//                String requiredStopLoSSpace = null;
//                int requiredStopLoSSpaceOrder = -1;
//               
//                // retrieve all directed relationships with source the to-be-verified element
//                Collection<DirectedRelationship> eldir = element.get_directedRelationshipOfSource();
//                
//                for (java.util.Iterator it = eldir.iterator(); it.hasNext();) {
//                    // get the next relationship element
//                    Element relEl = (Element) it.next();
//                    
//                    // check if the relationship if of the verify type
//                    Stereotype sterVerify = StereotypesHelper.getAppliedStereotypeByString(relEl, "Verify");
//                    if (sterVerify != null) {
//                        Abstraction abs = (Abstraction) relEl;
//                        
//                        // get the target element of the directed verify relationship
//                        Element targetEl = abs.getTarget().iterator().next();
//                        
//                        // check if it is the correct stereotype
//                        Stereotype sterStopLoSSpace = StereotypesHelper.getAppliedStereotypeByString(targetEl, "StopLoSSpace");
//                        if (sterStopLoSSpace != null) {
//                            requiredStopLoSSpace = getTagStringValue(targetEl, "StopLoS-Space", targetEl.getHumanType());
//                            //javax.swing.JOptionPane.showMessageDialog(null, "element " + targetEl.getHumanName() +" LoS=" + requiredStopLoSSpace);
//                            requiredStopLoSSpaceOrder = StopLoSSpaceOrderedList.indexOf(requiredStopLoSSpace);
//                        }
//                    }
//                }
//                
//                String elementStopLoSSpaceLeft = null;
//                String elementStopLoSSpaceRight = null;
//                //javax.swing.JOptionPane.showMessageDialog(null, "Retrieving double tag value of " + element.getHumanName());
//                double statsMinSpacePassStopLByTen = getTagDoubleValue(element, "StatsMinSpacePassStopL-ByTen", element.getHumanType());
//                double statsMinSpacePassStopL = statsMinSpacePassStopLByTen/10.0;
//                double statsMinSpacePassStopRByTen = getTagDoubleValue(element, "StatsMinSpacePassStopR-ByTen", element.getHumanType());
//                double statsMinSpacePassStopR = statsMinSpacePassStopRByTen/10.0;
//                
//                //javax.swing.JOptionPane.showMessageDialog(null, "Line has value " + statsMinSpacePassTrainL);
//               
//                // Calculate the actual LoS of the element
//                /*for (java.util.Iterator it = StopLoSSpaceMap.keySet().iterator(); it.hasNext();) {
//                    String currentStopLoSSpaceLeft = it.next().toString();
//                    double leftValue = StopLoSSpaceMap.get(currentStopLoSSpaceLeft)[0];
//                    double rightValue = StopLoSSpaceMap.get(currentStopLoSSpaceLeft)[1];
//                    if ((statsMinSpacePassStopL >= leftValue) && (statsMinSpacePassStopL <= rightValue)) {
//                      elementStopLoSSpaceLeft = currentStopLoSSpaceLeft;
//                      break;
//                    }
//                }
//                for (java.util.Iterator it = StopLoSSpaceMap.keySet().iterator(); it.hasNext();) {
//                    String currentStopLoSSpaceRight = it.next().toString();
//                    double leftValue = StopLoSSpaceMap.get(currentStopLoSSpaceRight)[0];
//                    double rightValue = StopLoSSpaceMap.get(currentStopLoSSpaceRight)[1];
//                    if ((statsMinSpacePassStopR >= leftValue) && (statsMinSpacePassStopR <= rightValue)) {
//                      elementStopLoSSpaceRight = currentStopLoSSpaceRight;
//                      break;
//                    }
//                }*/
//                   
//                elementStopLoSSpaceLeft = getTagStringValue(element, "stringStopLoSSpaceLeft", element.getHumanType());
//                //javax.swing.JOptionPane.showMessageDialog(null, "elementStopLoSSpaceLeft is " + elementStopLoSSpaceLeft);
//                elementStopLoSSpaceRight = getTagStringValue(element, "stringStopLoSSpaceRight", element.getHumanType());
//                //javax.swing.JOptionPane.showMessageDialog(null, "elementStopLoSSpaceRight is " + elementStopLoSSpaceRight);
//                
//                if ((elementStopLoSSpaceLeft != null) && StopLoSSpaceOrderedList.contains(elementStopLoSSpaceLeft)) {
//                    // Check if LoS is the required one or better
//                    
//                    
//                    //new code
//                    /*try{
//                        String content = element.getHumanName().replaceAll(" ", "") + "," + "left" + "," + elementStopLoSSpaceLeft + "\n"; 
//                        FileOutputStream fos = new FileOutputStream(loSComfortTemp, true);
//                        byte[] contentInBytes = content.getBytes();
//
//			fos.write(contentInBytes);
//			fos.flush();
//                        fos.close();
//                    }catch(IOException ev){
//                        ev.printStackTrace();
//                    }*/
//                    
//                    
//                    int elementLoSSpaceOrderLeft = StopLoSSpaceOrderedList.indexOf(elementStopLoSSpaceLeft);
//                    
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has LoS " + elementStopLoSSpace + " and the required LoS is " + requiredStopLoSSpace);
//                    if (elementLoSSpaceOrderLeft <= requiredStopLoSSpaceOrder) {
//                        StopLoSSpaceStatusLeft = true;
//                    }
//                }
//                if ((elementStopLoSSpaceRight != null) && StopLoSSpaceOrderedList.contains(elementStopLoSSpaceRight)) {
//                    // Check if LoS is the required one or better
//                    
//                    
//                    //new code
//                    /*try{
//                        String content = element.getHumanName().replaceAll(" ", "") + "," + "right" + "," + elementStopLoSSpaceRight + "\n"; 
//                        FileOutputStream fos = new FileOutputStream(loSComfortTemp, true);
//                        byte[] contentInBytes = content.getBytes();
//
//			fos.write(contentInBytes);
//			fos.flush();
//                        fos.close();
//                    }catch(IOException ev){
//                        ev.printStackTrace();
//                    }*/
//                    
//                    
//                    int elementLoSSpaceOrderRight = StopLoSSpaceOrderedList.indexOf(elementStopLoSSpaceRight);
//                    
//                    // Verification!
//                    //javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName() + " has LoS " + elementStopLoSSpace + " and the required LoS is " + requiredStopLoSSpace);
//                    if (elementLoSSpaceOrderRight <= requiredStopLoSSpaceOrder) {
//                        StopLoSSpaceStatusRight = true;
//                    }
//                }
//                
//                javax.swing.JOptionPane.showMessageDialog(null, StopLoSSpaceStatusRight + " | " + StopLoSSpaceStatusLeft);
//                
//                if ((!StopLoSSpaceStatusLeft) && (!StopLoSSpaceStatusRight)) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 0;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionSpaceLeft = new ActionSpace(element, elementStopLoSSpaceLeft, elementStopLoSSpaceRight, requiredStopLoSSpace, "StatsMinSpacePassStopL-ByTen", "StatsMinSpacePassStopR-ByTen", statsMinSpacePassStopL, statsMinSpacePassStopR, flag);
//                    actions.add(actionSpaceLeft);
//                    
//                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationSpaceLeft);
//                }
//                else if (!StopLoSSpaceStatusLeft) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 1;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionSpaceLeft = new ActionSpace(element, elementStopLoSSpaceLeft, null, requiredStopLoSSpace, "StatsMinSpacePassStopL-ByTen", null, statsMinSpacePassStopL, null, flag);
//                    actions.add(actionSpaceLeft);
//                    
//                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationSpaceLeft);
//                }
//                else if (!StopLoSSpaceStatusRight) { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                    int flag = 2;
//                    actions = new ArrayList<NMAction>();
//                    //javax.swing.JOptionPane.showMessageDialog(null, "loading validation rule");
//                    NMAction actionSpaceLeft = new ActionSpace(element, null, elementStopLoSSpaceRight, requiredStopLoSSpace, null, "StatsMinSpacePassStopR-ByTen", null, statsMinSpacePassStopR, flag);
//                    actions.add(actionSpaceLeft);
//                    
//                    Annotation annotationSpaceLeft = new Annotation(element, constraint, actions);
//                    result.add(annotationSpaceLeft);
//                }
//            }
            
        }
        
        /*String trainLine = null;
        String stopLine = null;
        List<String> trainElNames=new ArrayList<String>();
        List<String> stopElNames=new ArrayList<String>();
        List<String> trainElLeftOrRights=new ArrayList<String>();
        List<String> stopElLeftOrRights=new ArrayList<String>();
        List<String> trainLoSs=new ArrayList<String>();
        List<String> stopLoSs=new ArrayList<String>();
        double trainLoSLeftSum = 0;
        double stopLoSLeftSum = 0;
        double trainLoSRightSum = 0;
        double stopLoSRightSum = 0;
        //double trainLoSLeftAvg = 0.0;
        //double stopLoSLeftAvg = 0.0;
        //double trainLoSRightAvg = 0.0;
        //double stopLoSRightAvg = 0.0;
        double loSLeftAvg = 0.0;
        double loSRightAvg = 0.0;*/

        
        /*try {                    
            FileReader fileReader = new FileReader(loSComfortTemp);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String lines = null;
            int count = 0;
            List<String> elNames = new ArrayList<String>();
            List<String> elLeftOrRights = new ArrayList<String>();
            List<String> loSs = new ArrayList<String>();
            double leftLoSTrainSum = 0.0;
            double leftLoSStopSum = 0.0;
            double rightLoSTrainSum = 0.0;
            double rightLoSStopSum = 0.0;
            double leftLoSAvg = 0.0;
            double rightLoSAvg = 0.0;
            int leftLoSTrain = 0;
            int rightLoSTrain = 0;
            int leftLoSStop = 0;
            int rightLoSStop = 0;
                
            while((lines = bufferedReader.readLine()) != null) {
                count++;
                String[] array= lines.split(",");
                elNames.add(array[0]);
                elLeftOrRights.add(array[1]);
                loSs.add(array[2]);
            }
            bufferedReader.close();
            
            if ((elements.size()*2) == count) {
                for (int i=0; i<elements.size(); i++) {
                    if (elNames.get(i).startsWith("Line")) {
                        javax.swing.JOptionPane.showMessageDialog(null, "name = " + elNames.get(i));
                        if (elLeftOrRights.get(i).equals("left")) {
                            javax.swing.JOptionPane.showMessageDialog(null, "left type = " + elLeftOrRights.get(i));
                            leftLoSTrain = TrainLoSSpaceOrderedList.indexOf(loSs.get(i));
                            leftLoSTrainSum = leftLoSTrainSum + leftLoSTrain;
                        } 
                        else {
                            rightLoSTrain = TrainLoSSpaceOrderedList.indexOf(loSs.get(i));
                            rightLoSTrainSum = rightLoSTrainSum + rightLoSTrain;
                        }
                    }
                    else if (elNames.get(i).startsWith("Stop")) {
                        javax.swing.JOptionPane.showMessageDialog(null, "name = " + elNames.get(i));
                        if (elLeftOrRights.get(i).equals("left")) {
                            javax.swing.JOptionPane.showMessageDialog(null, "left type = " + elLeftOrRights.get(i));
                            leftLoSStop = StopLoSSpaceOrderedList.indexOf(loSs.get(i));
                            leftLoSStopSum = leftLoSStopSum + leftLoSStop;
                        } 
                        else {
                            rightLoSStop = StopLoSSpaceOrderedList.indexOf(loSs.get(i));
                            rightLoSStopSum = rightLoSStopSum + rightLoSStop;
                        }
                    }
                }
                javax.swing.JOptionPane.showMessageDialog(null, "left train Sum is " + leftLoSTrainSum);
                javax.swing.JOptionPane.showMessageDialog(null, "left stop Sum is " + leftLoSStopSum);
                javax.swing.JOptionPane.showMessageDialog(null, "right train Sum is " + rightLoSTrainSum);
                javax.swing.JOptionPane.showMessageDialog(null, "right Stop Sum is " + rightLoSStopSum);
                leftLoSAvg = (leftLoSTrainSum + leftLoSStopSum) / elements.size();
                rightLoSAvg = (rightLoSTrainSum + rightLoSStopSum) / elements.size();
                javax.swing.JOptionPane.showMessageDialog(null, "left LoS  avg is " + leftLoSAvg + " - right los avg is " + rightLoSAvg);
            }
        } catch (IOException even) {
                even.printStackTrace();
        }*/
        
        
        //new code!!
        /*for (Element element : elements) {
            
            Stereotype sterTrainLoSComfort = StereotypesHelper.getAppliedStereotypeByString(element, "Line");
            Stereotype sterStopLoSComfort = StereotypesHelper.getAppliedStereotypeByString(element, "Stop");

            if (sterTrainLoSComfort != null) {
                
                // retrieve all directed relationships with source the to-be-verified element
                Collection<DirectedRelationship> trainElDir = element.get_directedRelationshipOfSource();
                
                for (java.util.Iterator trainIt = trainElDir.iterator(); trainIt.hasNext();) {
                    // get the next relationship element
                    String content[] = null;
                    
                    Element trainRelEl = (Element) trainIt.next();
                    // check if the relationship if of the verify type
                    Stereotype trainSterVerify = StereotypesHelper.getAppliedStereotypeByString(trainRelEl, "Verify");
                    if (trainSterVerify != null) {
                        Abstraction trainAbs = (Abstraction) trainRelEl;
                        // get the target element of the directed verify relationship
                        Element trainTargetEl = trainAbs.getTarget().iterator().next();
                        // check if it is the correct stereotype
                        Stereotype sterLoSComfort = StereotypesHelper.getAppliedStereotypeByString(trainTargetEl, "LevelofService");
                        if (sterLoSComfort != null) { 
                            try {            
                                FileReader fileReader = new FileReader(loSComfortTemp);
                                BufferedReader bufferedReader = new BufferedReader(fileReader);
                                Scanner s = new Scanner(fileReader);
                                //stopS.useLocale(Locale.US);
                                int i = 0;
                                while ((trainLine = bufferedReader.readLine())!= null) {
                                    String[] trainAr= trainLine.split(",");
                                    if (trainAr[0].startsWith("Line")) {
                                        continue;
                                    }
                                    trainElNames.add(trainAr[0]);
                                    trainElLeftOrRights.add(trainAr[1]);
                                    trainLoSs.add(trainAr[2]);
                                }
                                bufferedReader.close();
                                for(i = 0; i < trainElLeftOrRights.size(); i++) {
                                    if (trainElLeftOrRights.get(i).equals("left")) {
                                        int trainLoSLeft = TrainLoSSpaceOrderedList.indexOf(trainLoSs.get(i));
                                        trainLoSLeftSum = trainLoSLeftSum + trainLoSLeft;
                                        //javax.swing.JOptionPane.showMessageDialog(null, "los left sum " + loSLeftSum);
                                    }
                                    else {
                                        int trainLoSRight = TrainLoSSpaceOrderedList.indexOf(trainLoSs.get(i));
                                        trainLoSRightSum = trainLoSRightSum + trainLoSRight;
                                        //javax.swing.JOptionPane.showMessageDialog(null, "los right " + loSs.get(i));
                                    }
                                    //javax.swing.JOptionPane.showMessageDialog(null, "dir " + elLeftOrRights.get(i) + " - los " + loSs.get(i));
                                }
                                //loSLeftAvg = (double)(loSLeftSum / (elLeftOrRights.size()/2));
                                //javax.swing.JOptionPane.showMessageDialog(null, "los left sum " + loSLeftSum + " - size " + elLeftOrRights.size()/2 + " - avg " + loSLeftAvg);
                                //loSRightAvg = (double)(loSRightSum / (elLeftOrRights.size()/2));
                                //javax.swing.JOptionPane.showMessageDialog(null, "los right sum " + loSRightSum + " - size " + elLeftOrRights.size()/2 + " - avg " + loSRightAvg);
                            } catch (IOException eve) {
                                    eve.printStackTrace();
                            }
                        }
                    }
                }
            }
            
            if (sterStopLoSComfort != null) {
                
                // retrieve all directed relationships with source the to-be-verified element
                Collection<DirectedRelationship> stopElDir = element.get_directedRelationshipOfSource();
                
                for (java.util.Iterator stopIt = stopElDir.iterator(); stopIt.hasNext();) {
                    // get the next relationship element
                    String content[] = null;
                    
                    Element stopRelEl = (Element) stopIt.next();
                    // check if the relationship if of the verify type
                    Stereotype stopSterVerify = StereotypesHelper.getAppliedStereotypeByString(stopRelEl, "Verify");
                    if (stopSterVerify != null) {
                        Abstraction stopAbs = (Abstraction) stopRelEl;
                        // get the target element of the directed verify relationship
                        Element stopTargetEl = stopAbs.getTarget().iterator().next();
                        // check if it is the correct stereotype
                        Stereotype sterLoSComfort = StereotypesHelper.getAppliedStereotypeByString(stopTargetEl, "LevelofService");
                        if (sterLoSComfort != null) {
                            try {            
                                FileReader fileReader = new FileReader(loSComfortTemp);
                                BufferedReader bufferedReader = new BufferedReader(fileReader);
                                Scanner s = new Scanner(fileReader);
                                //stopS.useLocale(Locale.US);
                                int i = 0;
                                while ((stopLine = bufferedReader.readLine())!= null) {
                                    String[] stopAr= stopLine.split(",");
                                    if (stopAr[0].startsWith("Stop")) {
                                        continue;
                                    }
                                    stopElNames.add(stopAr[0]);
                                    stopElLeftOrRights.add(stopAr[1]);
                                    stopLoSs.add(stopAr[2]);
                                    //i++;
                                    //javax.swing.JOptionPane.showMessageDialog(null, "fw3efrw3e4" + i);
                                }
                                bufferedReader.close();
                                for(i = 0; i < stopElLeftOrRights.size(); i++) {
                                    if (stopElLeftOrRights.get(i).equals("left")) {
                                        int stopLoSLeft = StopLoSSpaceOrderedList.indexOf(stopLoSs.get(i));
                                        stopLoSLeftSum = stopLoSLeftSum + stopLoSLeft;
                                        //javax.swing.JOptionPane.showMessageDialog(null, "los left sum " + loSLeftSum);
                                    }
                                    else {
                                        int stopLoSRight = StopLoSSpaceOrderedList.indexOf(stopLoSs.get(i));
                                        stopLoSRightSum = stopLoSRightSum + stopLoSRight;
                                        //javax.swing.JOptionPane.showMessageDialog(null, "los right " + loSs.get(i));
                                    }
                                    //javax.swing.JOptionPane.showMessageDialog(null, "dir " + elLeftOrRights.get(i) + " - los " + loSs.get(i));
                                }
                                //loSLeftAvg = (double)(loSLeftSum / (elLeftOrRights.size()/2));
                                //javax.swing.JOptionPane.showMessageDialog(null, "los left sum " + loSLeftSum + " - size " + elLeftOrRights.size()/2 + " - avg " + loSLeftAvg);
                                //loSRightAvg = (double)(loSRightSum / (elLeftOrRights.size()/2));
                                //javax.swing.JOptionPane.showMessageDialog(null, "los right sum " + loSRightSum + " - size " + elLeftOrRights.size()/2 + " - avg " + loSRightAvg);
                            } catch (IOException eve) {
                                    eve.printStackTrace();
                            }
                        }
                    }
                }
            }
            loSLeftAvg = (double)((trainLoSLeftSum + stopLoSLeftSum) / elements.size());
            javax.swing.JOptionPane.showMessageDialog(null, "los left sum " + (trainLoSLeftSum + stopLoSLeftSum) + " - size " + elements.size() + " - avg " + loSLeftAvg);
            //javax.swing.JOptionPane.showMessageDialog(null, "train los left sum " + trainLoSLeftSum + " - size " + trainElLeftOrRights.size()/2 + " - avg " + trainLoSLeftAvg);
            //trainLoSRightAvg = (double)(trainLoSRightSum / (trainElLeftOrRights.size()/2));
            //javax.swing.JOptionPane.showMessageDialog(null, "train los right sum " + trainLoSRightSum + " - size " + trainElLeftOrRights.size()/2 + " - avg " + trainLoSRightAvg);
            
            loSRightAvg = (double)((stopLoSRightSum + trainLoSRightSum) / elements.size());
            javax.swing.JOptionPane.showMessageDialog(null, "los right sum " + (trainLoSRightSum + stopLoSRightSum) + " - size " + elements.size() + " - avg " + loSRightAvg);
            //javax.swing.JOptionPane.showMessageDialog(null, "stop los left sum " + stopLoSLeftSum + " - size " + stopElLeftOrRights.size()/2 + " - avg " + stopLoSLeftAvg);
            //stopLoSRightAvg = (double)(stopLoSRightSum / (stopElLeftOrRights.size()/2));
            //javax.swing.JOptionPane.showMessageDialog(null, "stop los right sum " + stopLoSRightSum + " - size " + stopElLeftOrRights.size()/2 + " - avg " + stopLoSRightAvg);
        }*/
        
        //new code
        /*try {                    
            FileReader fileReader = new FileReader(loSComfortTemp);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            Scanner s = new Scanner(fileReader);
            int count = 0;
            String line = null;
            while((line = bufferedReader.readLine()) != null) {
                count++;
            }
            bufferedReader.close();
            if ((elements.size()*2) == count) {
                //loSComfortTemp.delete();
            }
        } catch (IOException eve) {
                eve.printStackTrace();
        } finally {
            try {
                if (stopBr != null)
                        stopBr.close();
                if (trainFr != null)
                        stopFr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }*/
        
        return result;
    }

    
    /*
    protected Annotation createAnnotation(Element element, Constraint constraint)
    {
        SpaceAction action = new SpaceAction(element);
        return new Annotation(element, constraint, Collections.singletonList(action));
    }
    */
    
    
    /**
     * 
     */
    public void dispose() {
    }
    
    double getTagDoubleValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        double ret = -1.0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Double.parseDouble(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    double getTagIntValue(Element elm, String name, String type) { //gets the double tag value of an element <elm> of name <name> of stereotype <type>
        int ret = 0;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = Integer.parseInt(value.get(0).toString());
                break;
            }
        }
        return ret;
    }
    
    String getTagStringValue(Element elm, String name, String type) { //gets the string tag value of an element <elm> of name <name> of stereotype <type>
        String ret = null;
        Stereotype ster = StereotypesHelper.getAppliedStereotypeByString(elm, type);
        List<Property> attrs = ster.getOwnedAttribute();
        for (int j = 0; j < attrs.size(); ++j) {
            Property tagDef = attrs.get(j);
            if (tagDef.getHumanName().endsWith(name)) {
                List value = StereotypesHelper.getStereotypePropertyValue(elm, ster, tagDef.getName());
                ret = value.get(0).toString();
                break;
            }
        }
        return ret;
    }
}