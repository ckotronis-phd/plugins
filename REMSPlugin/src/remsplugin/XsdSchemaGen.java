/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remsplugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File; 
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wiztools.xsdgen.ParseException;
import org.wiztools.xsdgen.XsdGen;

public class XsdSchemaGen {
    
    final String xsdFile = "ConfigurationSchema.xsd";
    final String filesDir = "ConfigurationDir";
    final String workingDirectory = System.getProperty("user.home");
    String xsd;
    
    
    public XsdSchemaGen() {

    }
    
    public void formXsd() {
        
        try {
            
            String[] cmd = {
                "/bin/bash",
                "-c",
                "java -jar /home/christos_kotronis/Downloads/xsd-gen-fat-0.2.4.jar /home/christos_kotronis/ConfigurationDir/Configuration.xml",
            };
            
            Process p = Runtime.getRuntime().exec(cmd);
            //"java -jar /home/christos_kotronis/Desktop/jp.jar /home/christos_kotronis/Modeling_Tools/Cameo/plugins/familyrelationships.pl");
            // you can pass the system command or a script to exec command. here i used uname -a system command
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            
            //javax.swing.JOptionPane.showMessageDialog(null, workingDirectory + File.separator + xmlFile);
            
            File f = new File(workingDirectory + File.separator + filesDir + File.separator + xsdFile);
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            String s = null;

            while ((s = stdInput.readLine()) != null) {
                System.out.println("Std OUT: " + s);
                 //javax.swing.JOptionPane.showMessageDialog(null,"Std OUT: " + s);
                 bw.write(s);
            }
            
            bw.close();

            while ((s = stdError.readLine()) != null) {
                System.out.println("Std ERROR : " + s);
                javax.swing.JOptionPane.showMessageDialog(null,"Std ERROR : " + s );
            }

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
