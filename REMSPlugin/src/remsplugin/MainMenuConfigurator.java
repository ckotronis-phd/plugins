/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remsplugin;

import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.MDActionsCategory;

/**
 *
 * @author christos_kotronis
 */
public class MainMenuConfigurator implements AMConfigurator {
    
    String MENU = "Designer Actions Menu";
    /**
     * Action will be added to manager.
     */
    private NMAction action;

    /**
     * Creates configurator.
     * @param action action to be added to main menu.
     */
    public MainMenuConfigurator(NMAction action) {
        this.action = action;
    }

    /**
     * @param mngr 
     * @see com.nomagic.actions.AMConfigurator#configure(ActionsManager)
     *  Methods adds action to given manager Examples category.
     */
    public void configure(ActionsManager mngr) {
        // searching for Examples action category
        ActionsCategory category = (ActionsCategory) mngr.getActionFor(MENU);
        
        if (category == null) {
            // creating new category
            category = new MDActionsCategory(MENU, MENU);
            category.setNested(true);
            mngr.addCategory(category);
        }
        category.addAction(action);
    }

    /**
     * 
     * @return
     */
    public int getPriority() {
        return AMConfigurator.MEDIUM_PRIORITY;
    }
    
}
