/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remsplugin;

import com.nomagic.magicdraw.actions.MDAction;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rg
 */

    public class RunCmd {
        
        List<String> str = new ArrayList<String>();
        
        
    public RunCmd() {

    }

    public List<String> start() {
        try {
            
            String[] cmd = {
                "/bin/bash",
                "-c",
                "java -jar /home/christos_kotronis/eclipse-workspace/jprol.zip_expanded/jprol/prol.jar /home/christos_kotronis/Modeling_Tools/Cameo/plugins/familyrelationships.pl Frequently RealTime NonImportant | grep '^[XYZ]' | tail -3 | grep -Eoh '\\((.*?)\\)' | tr -d '()'",
            };
            
            Process p = Runtime.getRuntime().exec(cmd);
                    //"java -jar /home/christos_kotronis/Desktop/jp.jar /home/christos_kotronis/Modeling_Tools/Cameo/plugins/familyrelationships.pl");
            // you can pass the system command or a script to exec command. here i used uname -a system command
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command
            String s = null;

            while ((s = stdInput.readLine()) != null) {
                System.out.println("Std OUT: " + s);
                 //javax.swing.JOptionPane.showMessageDialog(null,"Std OUT: " + s);
                 str.add(s);
            }

            while ((s = stdError.readLine()) != null) {
                System.out.println("Std ERROR : " + s);
                javax.swing.JOptionPane.showMessageDialog(null,"Std ERROR : " + s );
            }


        } catch (IOException e) {

            e.printStackTrace();
        }

        return str;
    }
}