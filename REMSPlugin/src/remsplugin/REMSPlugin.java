package remsplugin;

import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.ActionsID;
import com.nomagic.magicdraw.actions.ActionsProvider;
import com.nomagic.magicdraw.actions.DiagramContextAMConfigurator;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.actions.MDActionsCategory;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationManager;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.PresentationElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.properties.ConstraintProperty;
import com.nomagic.magicdraw.simulation.SimulationManager;
import com.nomagic.magicdraw.simulation.execution.session.SimulationSession;
import com.nomagic.magicdraw.sysml.util.SysMLConstants;
import com.nomagic.magicdraw.sysml.util.SysMLProfile;
import com.nomagic.magicdraw.ui.actions.DefaultDiagramAction;
import com.nomagic.magicdraw.uml.DiagramTypeConstants;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.paths.PathElement;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.validation.ValidationRunData;
import com.nomagic.magicdraw.validation.ValidationRunHelper;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Association;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;
import com.nomagic.uml2.ext.magicdraw.compositestructures.mdports.Port;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.ElementsFactory;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.*;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Classifier;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Constraint;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.EnumerationLiteral;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Generalization;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralInteger;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralString;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Namespace;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Relationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.TypedElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification;
import com.nomagic.uml2.ext.magicdraw.compositestructures.mdinternalstructures.Connector;
import com.nomagic.uml2.ext.magicdraw.compositestructures.mdinternalstructures.ConnectorEnd;
import java.awt.Color;
import java.awt.Component;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import java.awt.Desktop;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class REMSPlugin extends com.nomagic.magicdraw.plugins.Plugin {               

    private DefaultDiagramAction setProps;
    private DefaultDiagramAction createReq;
    private DefaultDiagramAction configure;
    private DefaultDiagramAction generate;
    private DefaultDiagramAction verify;
    private DefaultDiagramAction createAuxReqBlck;
    private DefaultDiagramAction createParam;
   
    int size = 0;
    JPanel jpan5;
    String tmpEnumName;
    List<String> selectedLevelFromList = new ArrayList<String>();
    ArrayList<String> userEnumNames = new ArrayList<String>();
    Collection<Stereotype> sterHier = null;
    Collection<Stereotype> sterHier2 = null;
    boolean pressedOkInGenerateMethod = false;
    
    public void init() {
        ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance(); 
        manager.addMainMenuConfigurator(new MainMenuConfigurator(getSeparatedActions()));
        
        
        setProps = new DefaultDiagramAction("Display component properties", "Display component properties", null, null) {
            public void actionPerformed(ActionEvent e) { 
                Project project = Application.getInstance().getProject();                 
                Diagram diagram = project.getActiveDiagram().getDiagram(); 
                Property property;
                boolean propertyAlreadyHasValue = false;
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected(); 
                    
                    if (presentationElement.getElement() != null) {     
                        Element selectedElement = presentationElement.getElement(); 
                        Collection<Element> selectedElementOwnedElements = selectedElement.getOwnedElement();
                        List<Stereotype> selectedElementStereotypes = StereotypesHelper.getStereotypes(selectedElement);  
                        
                        for (Stereotype selectedElementStereotype : selectedElementStereotypes) { 
                            List<Property> stereotypeOwnedAttributes = selectedElementStereotype.getOwnedAttribute(); 
                            
                            for (Element selectedElementOwnedElement : selectedElementOwnedElements) {
                                
                                if (selectedElementOwnedElement.getHumanName().equals("Property base_Class")) {
                                    propertyAlreadyHasValue = true;
                                    break;
                                }
                            }
                            if (propertyAlreadyHasValue) {
                                final JFrame frame = new JFrame();
                                JPanel mainPanel = new JPanel();
                                mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
                                mainPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                JPanel panel1 = new JPanel();
                                JPanel panel2 = new JPanel();
                                
                                panel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                panel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                JLabel panel1Label = new JLabel("<html>Component properties are already being displayed!<br/>You can create new desired properties.</html>");
                                panel1Label.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                panel1Label.setIconTextGap(10);
                                panel1.add(panel1Label);

                                JButton panel2Button1 = new JButton("OK");
                                panel2Button1.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        frame.setVisible(false);
                                    }
                                });
                                panel2.add(panel2Button1);

                                mainPanel.add(panel1);
                                mainPanel.add(panel2);

                                javax.swing.JOptionPane.showOptionDialog(frame, mainPanel, "Component properties display - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                                
                                return;
                            }
                            else {
                                for (Property stereotypeOwnedAttribute : stereotypeOwnedAttributes) { 
                                    SessionManager.getInstance().createSession("DisplayProperties");
                                
                                    property = project.getElementsFactory().createPropertyInstance(); //ftiaxnw nea properties me vasi ta parapanw
                                    property.setName(stereotypeOwnedAttribute.getName());
                                    property.setType(stereotypeOwnedAttribute.getType());
                                
                                    try {
                                        ModelElementsManager.getInstance().addElement(property, selectedElement);
                                    } catch (ReadOnlyElementException ex) {
                                        Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    SessionManager.getInstance().closeSession();   
                                }
                                final JFrame frame = new JFrame();
                                JPanel mainPanel = new JPanel();
                                mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
                                mainPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                JPanel panel1 = new JPanel();
                                JPanel panel2 = new JPanel();

                                panel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                panel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                JLabel panel1Label = new JLabel("<html>All component properties are now being displayed!</html>");
                                panel1Label.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
                                panel1Label.setIconTextGap(10);
                                panel1.add(panel1Label);

                                JButton panel2Button1 = new JButton("OK");
                                panel2Button1.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        frame.setVisible(false);
                                    }
                                });
                                panel2.add(panel2Button1);

                                mainPanel.add(panel1);
                                mainPanel.add(panel2);

                                javax.swing.JOptionPane.showOptionDialog(frame, mainPanel, "Component properties display", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                            }
                        }
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProfile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                Collection<Stereotype> selectedElementStereotypesHierarchy;

                if (remsProfile != null) {
                    
                    if (getFirstSelected() != null) {                    
                        PresentationElement presentationElement = getFirstSelected(); 
                        
                        if (presentationElement.getElement() != null) {     
                            Element selectedElement = presentationElement.getElement();
                            List<Stereotype> selectedElementStereotypes = StereotypesHelper.getStereotypes(selectedElement);
                            selectedElementStereotypesHierarchy = StereotypesHelper.getStereotypesHierarchy(selectedElement);
                            
                            for (Stereotype selectedElementStereotype : selectedElementStereotypes) {
                                
                                if (selectedElementStereotype.getName().startsWith("Verification") || selectedElementStereotype.getName().equals("ConstraintBlock")) {  
                                    setEnabled(false);
                                }
                                else {
                                    for (Stereotype selectedElementStereotypeHierarchy : selectedElementStereotypesHierarchy) {
                                        String[] selectedElementStereotypeHierarchyName = selectedElementStereotypeHierarchy.getHumanName().split(" ", 2);
                                        
                                        if (selectedElementStereotypeHierarchyName[1].equals("AbstractRequirement") || selectedElementStereotypeHierarchyName[1].equals("ValueType") || selectedElementStereotypeHierarchyName[1].equals("DirectedRelationshipPropertyPath")) {
                                            setEnabled(false);
                                            break;
                                        }
                                        else {
                                            setEnabled(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        
        
        configure = new DefaultDiagramAction("Configure component", "Configure component", null, null) {
            public void actionPerformed(ActionEvent e) { 
                Project project = Application.getInstance().getProject(); 
                Diagram diagram = project.getActiveDiagram().getDiagram();  
                
//                JSONObject parsed = null;
//                JSONParser parser = null;
//                try {
//                    parsed = (JSONObject) new JSONParser().parse(new FileReader("C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/configs/BlockConfigurations.json"));
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
                SessionManager.getInstance().createSession("Configure");
//                final JSONObject blockConfigurations = parsed; 
                File inputConfigurationsFile = new File("C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/configs/BlockConfigurations.xml");
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = null;
                Document document = null;
                
                try {
                    documentBuilder = documentBuilderFactory.newDocumentBuilder();
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    document = documentBuilder.parse(inputConfigurationsFile);
                } catch (SAXException ex) {
                    Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                }
                document.getDocumentElement().normalize();
                NodeList blockList = document.getElementsByTagName("Block");
                
                HashMap<String, HashMap<String, HashMap>> blockConfigurations = new HashMap<String, HashMap<String, HashMap>>();
                HashMap<String, HashMap> blockHashMap = new HashMap<String, HashMap>();
                HashMap<String, String> configurationHashMap = new HashMap<String, String>();
                HashMap<String, HashMap<String, String>> blockToLevels = new HashMap();
                
                for (int blockIndex = 0; blockIndex < blockList.getLength(); blockIndex++) {
                    Node blockNode = blockList.item(blockIndex);
                    String[] blockNodeName = blockNode.getAttributes().getNamedItem("name").toString().replace("\"", "").split("=");
                    String blockName = blockNodeName[1];
                    blockConfigurations.put(blockName, new HashMap<String, HashMap>());
                    blockHashMap = blockConfigurations.get(blockName);
                    NodeList configurationList = blockNode.getChildNodes();
                    
                    for (int configurationIndex = 0; configurationIndex < configurationList.getLength(); configurationIndex++) {
                        Node configurationNode = configurationList.item(configurationIndex);
                        
                        if (configurationNode.hasAttributes()) {
                            String[] configurationNodeName = configurationNode.getAttributes().getNamedItem("name").toString().replace("\"", "").split("=");
                            String configurationName = configurationNodeName[1];
                            blockHashMap.put(configurationName, new HashMap<String, String>());
                            configurationHashMap = (HashMap) blockHashMap.get(configurationName);
                            NodeList propertyList = configurationNode.getChildNodes();
                            
                            for (int propertyIndex = 0; propertyIndex < propertyList.getLength(); propertyIndex++) {
                                Node propertyNode = propertyList.item(propertyIndex);
                                
                                if (propertyNode.hasAttributes()) {
                                    String[] propNodeName = propertyNode.getAttributes().getNamedItem("name").toString().replace("\"", "").split("=");
                                    String propName = propNodeName[1];
                                    configurationHashMap.put(propName, propertyNode.getTextContent());
                                }
                            }
                        }
                    }    
                }
                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  

                    if (presentationElement.getElement() != null) {     
                        final Element selectedElement = presentationElement.getElement();
                        List<Stereotype> selectedElementStereotypes = StereotypesHelper.getStereotypes(selectedElement);
                        
                        for (Stereotype selectedElementStereotype : selectedElementStereotypes) {
                            if (blockConfigurations.containsKey(selectedElementStereotype.getName())) {
                                String ster = selectedElementStereotype.getName();
                                Map thisBlockConfs = (Map) blockConfigurations.get(ster);

                                final JFrame frame = new JFrame();
                                //Create main panels
                                JPanel mainPanel = new JPanel();
                                //Set main panels' layout
                                mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
                                mainPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                //Create subpanels
                                JPanel panel1 = new JPanel();
                                JPanel panel2 = new JPanel();
                                JPanel panel3 = new JPanel();
                                //Subpanel 1 (details) and 2 (actions) layouts
                                panel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                panel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Configurations"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                panel3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                
                                JLabel txt = new JLabel("<html>You can select a component configuration<br/>from the following list.</html>");
                                txt.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                txt.setIconTextGap(10);
                                panel1.add(txt);
                                
                                ButtonGroup group = new ButtonGroup();
                                Box box = Box.createVerticalBox();
                                for (java.util.Iterator iter = thisBlockConfs.keySet().iterator(); iter.hasNext();) {
                                    String blockConfsString = (String) iter.next();
                                    final JRadioButton confs = new JRadioButton(blockConfsString);
                                    confs.setActionCommand(blockConfsString);
                                    group.add(confs);
                                    box.add(confs);
                                    box.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                }
                                
                                final JRadioButton conf5 = new JRadioButton("Suggested Mode");
                                group.add(conf5);
                                box.add(conf5);
                                panel2.add(box);
                                 
                                JButton jpFinish = new JButton("OK");
                                jpFinish.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        String choice = group.getSelection().getActionCommand();
                                        Map thisBlockConf = null;
                                        for (java.util.Iterator it = thisBlockConfs.keySet().iterator(); it.hasNext();) {
                                            String s = (String) it.next();
                                            if (s.equals(choice)) {
                                                thisBlockConf = (Map) thisBlockConfs.get(choice);
                                            }
                                        }                             
                                        if (thisBlockConf != null) {
                                            Collection<Element> selectedElementOwnedProperties = selectedElement.getOwnedElement();
                                            String[] selectedElementOwnedPropertyName = null;
                                            Property property = null;
                                            
                                            for (Element selectedElementOwnedProperty : selectedElementOwnedProperties) {
                                                selectedElementOwnedPropertyName = selectedElementOwnedProperty.getHumanName().split(" ");
                                                if (selectedElementOwnedProperty.getHumanType().equals("Value Property")) {
                                                    if (thisBlockConf.containsKey(selectedElementOwnedPropertyName[2])) {
                                                        property = (Property) selectedElementOwnedProperty;
                                                        ValueSpecification vs = ModelHelper.createValueSpecification(project, property.getType(), thisBlockConf.get(selectedElementOwnedPropertyName[2]), null);
                                                        property.setDefaultValue(vs);    
                                                    }
                                                }
                                            }
                                        }
                                        else if (conf5.isSelected()){
                                            TypedElement currentConstraintElement;
                                            Element relatedRequirement;
                                            Element blockRelatedRequirement;
                                            Element verData;
                                            Element verFormula;
                                            List<String> propertiesAndOwners = new ArrayList<String>();
                                            List<Element> verificationFormulas = new ArrayList<Element>();
                                            List<TypedElement> constraintProperties = new ArrayList<TypedElement>();
                                            List<Element> requirements = new ArrayList<Element>();
                                            Collection<DiagramPresentationElement> projectDiagrams = project.getDiagrams();
                                            HashMap<String, HashSet<String>> blockToReqs = new HashMap<String, HashSet<String>>();
                                            Stereotype satisfyStereotype = StereotypesHelper.getStereotype(project, "Satisfy");
                                            Stereotype verifyStereotype = StereotypesHelper.getStereotype(project, "Verify");
                                            Stereotype refineStereotype = StereotypesHelper.getStereotype(project, "Refine");
                                            
                                            Collection<Element> selectedElementOwnedProperties = selectedElement.getOwnedElement();
                                            List presElms = project.getActiveDiagram().getPresentationElements();
                                            String[] selectedElementOwnedPropertyName;
                                            Element presentationPropertyElement;
                                            String[] partPropertyElementName;
                                            String[] presentationPropertyElementName;
                                            List<Element> blocks = new ArrayList<Element>();
                                            List<Element> parentChildBlocks = new ArrayList<Element>();
                                            List<String> configureBlocks = new ArrayList<String>();
                                            blocks.add(selectedElement);
                                            configureBlocks.add(selectedElement.getHumanType());
                                            HashMap<String, ArrayList<String>> blocksToPropNames;
                                            HashMap<String, HashMap<String, String>> blocksToPropNamesVals = new HashMap();
                                            //vriskw paidia tou epilegmenou block, an exei
                                            for (Element selectedElementOwnedProperty : selectedElementOwnedProperties) {
                                                selectedElementOwnedPropertyName = selectedElementOwnedProperty.getHumanName().split(" ");

                                                if (selectedElementOwnedProperty.getHumanType().equals("Part Property")) {
                                                    partPropertyElementName = selectedElementOwnedProperty.getHumanName().split(" ");
                                                    for (int i = presElms.size() - 1; i >= 0; --i) {
                                                        PresentationElement presentationElement = (PresentationElement) presElms.get(i);
                                                        presentationPropertyElement = presentationElement.getElement();
                                                        presentationPropertyElementName = presentationPropertyElement.getHumanName().split(" ", 2);

                                                        if (blockConfigurations.containsKey(presentationPropertyElement.getHumanType())) {
                                                            if (presentationPropertyElementName[1].equals(partPropertyElementName[2])) {
                                                                blocks.add(presentationPropertyElement);
                                                                configureBlocks.add(presentationPropertyElement.getHumanType());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            for (Element elementBlock : blocks) {
                                                //pairnw ta elements tou kathe block element
                                                Collection<Element> elementOwnedElements = elementBlock.getOwnedElement();
                                                //pairnw tis sindeseis tou kathe block
                                                Collection<DirectedRelationship> blockRelationships = elementBlock.get_directedRelationshipOfSource(); 

                                                for (java.util.Iterator relationshipsIter = blockRelationships.iterator(); relationshipsIter.hasNext();) {
                                                    Element blockRelationship = (Element) relationshipsIter.next();   
                                                    //an iparxei satisfy sindesi tou block me requirement
                                                    if (blockRelationship.getHumanName().equals(satisfyStereotype.getName())) {
                                                        Abstraction blockRelationshipAbstraction = (Abstraction) blockRelationship;
                                                        //pairnw to sindedemeno requirement
                                                        blockRelatedRequirement = blockRelationshipAbstraction.getTarget().iterator().next();
                                                        //vazw me mikro to prwto gramma tou block, p.x. dataAggregator
                                                        String blockName = elementBlock.getHumanType().substring(0, 1).toLowerCase().concat(elementBlock.getHumanType().substring(1));
                                                        if (!blockToReqs.containsKey(blockName)) { 
                                                            blockToReqs.put(blockName, new HashSet<String>());
                                                        }
                                                        if (!blockToLevels.containsKey(blockName)) { 
                                                            blockToLevels.put(blockName, new HashMap());
                                                        }
                                                        String blockReqName = blockRelatedRequirement.getHumanName().split(" ")[1];
                                                        blockToReqs.get(blockName).add(blockReqName.substring(0, 1).toLowerCase().concat(blockReqName.substring(1)));
                                                        requirements.add(blockRelatedRequirement);
                                                                   
                                                        Collection<Stereotype> sterHierarchy = StereotypesHelper.getStereotypesHierarchy(blockRelatedRequirement);
                                                        String constraintName = blockRelatedRequirement.getHumanName().split(" ")[1];
                                                        String levelName = constraintName.substring(0, 1).toUpperCase().concat(constraintName.substring(1)).concat("Level");

                                                        for (Stereotype ster : sterHierarchy) {
                                                            if (StereotypesHelper.getStereotypePropertyValue(blockRelatedRequirement, ster, "levels") != null) {
                                                                blockToLevels.get(blockName).put(levelName, StereotypesHelper.getStereotypePropertyValue(blockRelatedRequirement, ster, "levels").get(0).toString());
                                                            }
                                                        }
                                                        Collection<DirectedRelationship> requirementRelationships = blockRelatedRequirement.get_directedRelationshipOfTarget();
                                                        for (java.util.Iterator reqRelationshipsIter = requirementRelationships.iterator(); reqRelationshipsIter.hasNext();) {
                                                            Element reqRelationship = (Element) reqRelationshipsIter.next();   
                                                            if (reqRelationship.getHumanName().equals(verifyStereotype.getName())) {
                                                                Abstraction reqRelationshipAbstraction = (Abstraction) reqRelationship;
                                                                verData = reqRelationshipAbstraction.getSource().iterator().next();
                                                                Collection<Element> verDataOwnedElements = verData.getOwnedElement();
                                                                //pairnw to cosntraint mesa sto data
                                                                for (Element verDataOwnedElement : verDataOwnedElements) {
                                                                    if (verDataOwnedElement.getHumanType().equals("Constraint Property")) {
                                                                        currentConstraintElement = (TypedElement) verDataOwnedElement;
                                                                        constraintProperties.add(currentConstraintElement);
                                                                    }
                                                                }
                                                            }
                                                            if (reqRelationship.getHumanName().equals(refineStereotype.getName())) {
                                                                Abstraction reqRelationshipAbstraction = (Abstraction) reqRelationship;
                                                                verFormula = reqRelationshipAbstraction.getSource().iterator().next();
                                                                verificationFormulas.add(verFormula);
                                                            }
                                                        }
                                                    }
                                                }
                                                for (Element elementOwnedElement : elementOwnedElements) {

                                                    for (java.util.Iterator relationshipsItr = blockRelationships.iterator(); relationshipsItr.hasNext();) {
                                                        Element blckRelationship = (Element) relationshipsItr.next();   
                                                        //an ta elements tou block einai value properties
                                                        if (blckRelationship.getHumanName().equals(satisfyStereotype.getName()) && (elementOwnedElement.getHumanType().equals("Value Property"))) {
                                                            String propertiesAndOwnersString = elementOwnedElement.getOwner().getHumanType() + "." + elementOwnedElement.getHumanName().split(" ")[2];
                                                            propertiesAndOwnersString = propertiesAndOwnersString.substring(0, 1).toLowerCase().concat(propertiesAndOwnersString.substring(1));
                                                            propertiesAndOwners.add(propertiesAndOwnersString);
                                                            break;
                                                        }
                                                    }
                                                }   
                                            }
                                            Set<String> propertiesAndOwnersSet = new HashSet<String>(propertiesAndOwners);
                                            propertiesAndOwners.clear();
                                            propertiesAndOwners.addAll(propertiesAndOwnersSet);         

                                            Set<Element> verificationFormulasSet = new HashSet<Element>(verificationFormulas);
                                            verificationFormulas.clear();
                                            verificationFormulas.addAll(verificationFormulasSet);

                                            Set<TypedElement> constraintPropertiesSet = new HashSet<TypedElement>(constraintProperties);
                                            constraintProperties.clear();
                                            constraintProperties.addAll(constraintPropertiesSet);

                                            Set<Element> requirementsSet = new HashSet<Element>(requirements);
                                            requirements.clear();
                                            requirements.addAll(requirementsSet);

                                            blocksToPropNames = createProlog(project, blockToLevels, blockToReqs, propertiesAndOwners, verificationFormulas, constraintProperties, requirements);
                                            //gia to question, oti onoma exei i lista, vazw Var0, 1, 2, klp
                                            for (int j=0; j< configureBlocks.size(); j++) {
                                                Collections.replaceAll(configureBlocks, configureBlocks.get(j), "Var" + j);
                                            }
                                            
                                            String configurePredicateString = "configure(";
                                            configurePredicateString += Arrays.toString(configureBlocks.toArray());
                                            configurePredicateString += ")";
                                            
                                            String questionFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_question.pl";
                                            FileWriter questionFileWriter = null;
                                            File questionFile = new File(questionFileName);
                                            try {
                                                questionFile.createNewFile();
                                            } catch (IOException ex) {
                                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            try {
                                                questionFileWriter = new FileWriter(questionFile);
                                            } catch (IOException ex) {
                                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            try {
                                                questionFileWriter.write(configurePredicateString);
                                            } catch (IOException ex) {
                                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            try {
                                                questionFileWriter.flush();
                                                questionFileWriter.close();
                                            } catch (IOException ex) {
                                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            
                                            ArrayList<String> cmds = new ArrayList();
                                            cmds.add("java");
                                            cmds.add("-jar");
                                            cmds.add("C:\\Users\\chris\\Desktop\\REMSNewProject\\PrologApp2\\dist\\PrologApp2.jar");
                                            ProcessBuilder pb = new ProcessBuilder(cmds);

                                            try {
                                                Process p = pb.start();
                                                p.waitFor();
                                            } catch (IOException ioex) {
                                                ioex.printStackTrace();
                                            } catch (InterruptedException iex) {
                                                iex.printStackTrace();
                                            }
                                            String prologResultsFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_results.pl";
                                            HashMap<String, ArrayList<String>> blocksToPropVals = new HashMap();
                                            ArrayList<String> prologResults = new ArrayList<>();
                                            File prologResultsFile = new File(prologResultsFileName);
                                            Scanner prologResultsFileReader = null;
                                            try {
                                                prologResultsFileReader = new Scanner(prologResultsFile);
                                            } catch (FileNotFoundException ex) {
                                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            while (prologResultsFileReader.hasNext()) {
                                                String prologResultLine = prologResultsFileReader.nextLine();
                                                prologResultLine = prologResultLine.replace("(", ",").replace(")", "");
                                                String[] prologResultsLineElements = prologResultLine.split(",");
                                                String blockName = prologResultsLineElements[0];
                                                blocksToPropVals.put(blockName, new ArrayList());
                                                for (int prologResultsLineElementsIdx=1; prologResultsLineElementsIdx < prologResultsLineElements.length; prologResultsLineElementsIdx++) {
                                                    blocksToPropVals.get(blockName).add(prologResultsLineElements[prologResultsLineElementsIdx].replace(" ", "").replace("'", ""));
                                                }
                                            }
                                            prologResultsFileReader.close();
                                            
                                            for (String blockName: blocksToPropNames.keySet()) {
                                                blocksToPropNamesVals.put(blockName, new HashMap());
                                                int propIdx = 0;
                                                for (propIdx = 0; propIdx < blocksToPropNames.get(blockName).size(); propIdx++) {
                                                    String propName = blocksToPropNames.get(blockName).get(propIdx);
                                                    String propVal = blocksToPropVals.get(blockName).get(propIdx);
                                                    blocksToPropNamesVals.get(blockName).put(propName, propVal);
                                                }
                                            }
                                            parentChildBlocks.add(selectedElement);
                                            parentChildBlocks.addAll(blocks);
                                            
                                            for (Element blck : parentChildBlocks) {
                                                Collection<Element> prologConfSelectedElementOwnedProps = blck.getOwnedElement();
                                                String[] prologConfSelectedElementOwnedPropName = null;
                                                Property property = null;

                                                List<Stereotype> sters = StereotypesHelper.getStereotypes(blck);
                                                String sterName = sters.get(0).getName();
                                                String sterBlockName = sterName.substring(0, 1).toLowerCase().concat(sterName.substring(1));
                                                if (blocksToPropNamesVals.containsKey(sterBlockName)) {
                                                    for (Element prologConfSelectedElementOwnedProp : prologConfSelectedElementOwnedProps) {
                                                        prologConfSelectedElementOwnedPropName = prologConfSelectedElementOwnedProp.getHumanName().split(" ");
                                                        if (prologConfSelectedElementOwnedProp.getHumanType().equals("Value Property")) {
                                                            property = (Property) prologConfSelectedElementOwnedProp; 
                                                            String propName = property.getHumanName().split(" ")[2];
                                                            if (blocksToPropNamesVals.get(sterBlockName).containsKey(propName)) {
                                                                ValueSpecification vs = ModelHelper.createValueSpecification(project, property.getType(), blocksToPropNamesVals.get(sterBlockName).get(propName), null);
                                                                property.setDefaultValue(vs);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            final JFrame jFrame = new JFrame();
                                            JPanel jMainPanel = new JPanel();
                                            jMainPanel.setLayout(new BoxLayout(jMainPanel, BoxLayout.PAGE_AXIS));
                                            jMainPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));  
                                            JPanel jPanel1 = new JPanel();
                                            JPanel jPanel2 = new JPanel();
                                            JPanel jPanel3 = new JPanel();

                                            jPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                            jPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Tasks"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                            jPanel3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                            JLabel jPanel1Label1 = new JLabel("<html>The following -decision support-<br/>"
                                                                        + "tasks have been COMPLETED!</html>");
                                            jPanel1Label1.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
                                            jPanel1Label1.setIconTextGap(10);
                                            jPanel1.add(jPanel1Label1);

                                            Box box2 = Box.createVerticalBox();
                                            JLabel jPanel1Lbl1 = new JLabel("<html>\"Update Prolog predicates\"</html>");
                                            JLabel jPanel1Lbl2 = new JLabel("<html>\"Update Prolog rules\"</html>");
                                            JLabel jPanel1Lbl3 = new JLabel("<html>\"Generate Prolog query\"</html>");
                                            JLabel jPanel1Lbl4 = new JLabel("<html>\"Execute Prolog\"</html>");
                                            JLabel jPanel1Lbl5 = new JLabel("<html>\"Generate results\"</html>");
                                            JLabel jPanel1Lbl6 = new JLabel("<html>\"Integrate results to model\"</html>");
                                                
                                            box2.add(jPanel1Lbl1);
                                            box2.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                            box2.add(jPanel1Lbl2);
                                            box2.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                            box2.add(jPanel1Lbl3);
                                            box2.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                            box2.add(jPanel1Lbl4);
                                            box2.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                            box2.add(jPanel1Lbl5);
                                            box2.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
                                            box2.add(jPanel1Lbl6);
                                            
                                            jPanel2.add(box2);
                                            
                                            JButton jPanel1Button1 = new JButton("OK");
                                            jPanel1Button1.addActionListener(new ActionListener() {
                                                public void actionPerformed(ActionEvent e) {       
                                                    jFrame.setVisible(false);
                                                }
                                            });
                                            
                                            JButton jPanel1Button2 = new JButton("Cancel");
                                            jPanel1Button2.addActionListener(new ActionListener() {
                                                public void actionPerformed(ActionEvent e) {       
                                                    jFrame.setVisible(false);
                                                }
                                            });

                                            jPanel3.add(jPanel1Button1);
                                            jPanel3.add(jPanel1Button2);
                                            
                                            jMainPanel.add(jPanel1);
                                            jMainPanel.add(jPanel2);
                                            jMainPanel.add(jPanel3);
                                            javax.swing.JOptionPane.showOptionDialog(jFrame, jMainPanel, "Decision Support System", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                                        }  
                                        frame.setVisible(false);
                                    }
                                });          
                                //Add buttons in subpanel 2
                                panel3.add(jpFinish);
                                //Add subpanels in main panel 1
                                mainPanel.add(panel1);
                                mainPanel.add(panel2);
                                mainPanel.add(panel3);

                                javax.swing.JOptionPane.showOptionDialog(frame, mainPanel, "Component configuration", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                            }
                        }
                        SessionManager.getInstance().closeSession();
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProfile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                Collection<Stereotype> selectedElementStereotypesHierarchy;

                if (remsProfile != null) {
                    
                    if (getFirstSelected() != null) {                    
                        PresentationElement presentationElement = getFirstSelected(); 
                        
                        if (presentationElement.getElement() != null) {     
                            Element selectedElement = presentationElement.getElement();
                            List<Stereotype> selectedElementStereotypes = StereotypesHelper.getStereotypes(selectedElement);
                            selectedElementStereotypesHierarchy = StereotypesHelper.getStereotypesHierarchy(selectedElement);
                            
                            for (Stereotype selectedElementStereotype : selectedElementStereotypes) {
                                
                                if (selectedElementStereotype.getName().startsWith("Verification") || selectedElementStereotype.getName().equals("ConstraintBlock")) {  
                                    setEnabled(false);
                                }
                                else {
                                    for (Stereotype selectedElementStereotypeHierarchy : selectedElementStereotypesHierarchy) {
                                        String[] selectedElementStereotypeHierarchyName = selectedElementStereotypeHierarchy.getHumanName().split(" ", 2);
                                        
                                        if (selectedElementStereotypeHierarchyName[1].equals("AbstractRequirement") || selectedElementStereotypeHierarchyName[1].equals("ValueType") || selectedElementStereotypeHierarchyName[1].equals("DirectedRelationshipPropertyPath")) {
                                            setEnabled(false);
                                            break;
                                        }
                                        else {
                                            setEnabled(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        
        
        verify = new DefaultDiagramAction("Verify", "Verify", null, null) {
            public void actionPerformed(ActionEvent e) { 
                Project proj = Application.getInstance().getProject();                
                Profile prof = (Profile) StereotypesHelper.getProfile(proj, "REMSProfile");  
                //Profile costProf = (Profile) StereotypesHelper.getProfile(proj, "GeneralCostProfile");  
                Diagram diag = proj.getActiveDiagram().getDiagram();    
                DiagramPresentationElement diagPresElem = proj.getDiagram(diag); 
                
                SessionManager.getInstance().createSession("Ver");

                Stereotype ver = StereotypesHelper.getStereotype(proj, "Verify");
                Stereotype sat = StereotypesHelper.getStereotype(proj, "Satisfy");
                Element relEl = null;
                Element relElCap = null;
                Element relElDev = null;
                Element relElAgg = null;
                Element relEl2 = null;
                Element relEl2Cap = null;
                Element relEl2Dev = null;
                Element relEl2Agg = null;
                String[] relEl2Name = null;
                String[] relEl2NameCap = null;
                String[] relEl2NameDev = null;
                String[] relEl2NameAgg = null;
                String[] elName;
                String[] elNameCap;
                String[] elNameDev;
                String[] elNameAgg;
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presElem = getFirstSelected();  

                    if (presElem.getElement() != null) {     
                        Element elem = presElem.getElement();
                        List<Stereotype> sters = StereotypesHelper.getStereotypes(elem);
                        
                        for (Stereotype ster : sters) {
                            
                            //javax.swing.JOptionPane.showMessageDialog(null, ster.getHumanName());
                            
                            if (ster.getName().equals("Layer")) {
                                Collection<DirectedRelationship> dirRel = elem.get_directedRelationshipOfSource();
                                
                                for (java.util.Iterator dirRelIt = dirRel.iterator(); dirRelIt.hasNext();) {
                                    Element satRel = (Element) dirRelIt.next();                        
                                    if (satRel.getHumanName().equals(sat.getName())) {
                                        Abstraction satAbs = (Abstraction) satRel;
                                        relEl = satAbs.getTarget().iterator().next();
                                        //javax.swing.JOptionPane.showMessageDialog(null, relEl.getHumanName());
                                        
                                        AnnotationManager instance = AnnotationManager.getInstance();
                                        List<Annotation> listEls = instance.getAnnotations(relEl);
                                        List<Annotation> listSats = instance.getAnnotations(satAbs);
                                        
                                        for (Annotation annotationEls : listEls)
                                        {
                                            instance.remove(annotationEls);
                                        }
                                        
                                        for (Annotation annotationSats : listSats)
                                        {
                                            instance.remove(annotationSats);
                                        }
                                        instance.update();
                                    }
                                }
                                
                                Collection<Relationship> dirRelAss = elem.get_relationshipOfRelatedElement();
                                
                                for (java.util.Iterator dirRelIt2 = dirRelAss.iterator(); dirRelIt2.hasNext();) {
                                    Element relEval = (Element) dirRelIt2.next();
                                    //javax.swing.JOptionPane.showMessageDialog(null, "onoma ass: " + relEval.getHumanName() + " | " + relEval.getHumanType());
                                    if (relEval.getHumanType().endsWith("Association")) {
                                        //javax.swing.JOptionPane.showMessageDialog(null, "mpika");
                                        Association ass = (Association) relEval;
                                        relEl2 = ModelHelper.getFirstMemberEnd(ass);
                                        //javax.swing.JOptionPane.showMessageDialog(null, "onoma element ass: " + relEl2.getHumanName() + " |" + relEl2.getHumanType());
                                        if (relEl2.getHumanType().equals("Reference Property")) {
                                            relEl2Name = relEl2.getHumanName().split(" ");
                                            Collection<DiagramPresentationElement> diags = proj.getDiagrams();
                                            Stereotype ster2 = StereotypesHelper.getStereotype(proj, "VerificationCritData", prof);
                                            List presEls = proj.getActiveDiagram().getPresentationElements();

                                            for (int i = presEls.size() - 1; i >= 0; --i) {
                                                PresentationElement presEl = (PresentationElement) presEls.get(i);
                                                Element el = presEl.getElement();
                                                elName = el.getHumanName().split(" ");
                                                //javax.swing.JOptionPane.showMessageDialog(null, "onoma prese eleme: " + el.getHumanName());
                                                if (elName[0].equals("VerificationCritData")) {
                                                    //javax.swing.JOptionPane.showMessageDialog(null, "onoma: " + relEl2.getHumanName());
                                                    if (elName[1].equals(relEl2Name[2])) {
                                                    //javax.swing.JOptionPane.showMessageDialog(null, "vrika idio onoma");
                                                        Collection<Element> els = el.getOwnedElement();

                                                        for (Element element : els) {
                                                            String arr[] = element.getHumanName().split(" ", 2);
                                                            String diagName = arr[0];
                                                            
                                                            if (diagName.equals("Diagram")) {
                                                                //javax.swing.JOptionPane.showMessageDialog(null, "mpika diagram");
                                                                //javax.swing.JOptionPane.showMessageDialog(null, el.getHumanName() + " | " + relEl2.getHumanName());
                                                                
                                                                //javax.swing.JOptionPane.showMessageDialog(null, "vrika parametric diagram");
                                                                MyExecutionListener mel = new MyExecutionListener(proj, prof, el);
                                                                SimulationManager.registerSimulationExecutionListener(mel);
                                                                SimulationSession simSession = SimulationManager.execute(el, true, true);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (ster.getName().equals("CapExCost")) {
                                Collection<DirectedRelationship> dirRelCap = elem.get_directedRelationshipOfSource();
                                
                                for (java.util.Iterator dirRelItCap = dirRelCap.iterator(); dirRelItCap.hasNext();) {
                                    Element satRelCap = (Element) dirRelItCap.next();                        
                                    if (satRelCap.getHumanName().equals(sat.getName())) {
                                        Abstraction satAbsCap = (Abstraction) satRelCap;
                                        relElCap = satAbsCap.getTarget().iterator().next();
                                        //javax.swing.JOptionPane.showMessageDialog(null, relElCap.getHumanName());
                                        
                                        AnnotationManager instanceCap = AnnotationManager.getInstance();
                                        List<Annotation> listElsCap = instanceCap.getAnnotations(relElCap);
                                        List<Annotation> listSatsCap = instanceCap.getAnnotations(satAbsCap);
                                        
                                        for (Annotation annotationElsCap : listElsCap)
                                        {
                                            instanceCap.remove(annotationElsCap);
                                        }
                                        
                                        for (Annotation annotationSatsCap : listSatsCap)
                                        {
                                            instanceCap.remove(annotationSatsCap);
                                        }
                                        instanceCap.update();
                                    }
                                }
                                
                                Collection<Relationship> dirRelAssCap = elem.get_relationshipOfRelatedElement();
                                
                                for (java.util.Iterator dirRelIt2Cap = dirRelAssCap.iterator(); dirRelIt2Cap.hasNext();) {
                                    Element relEvalCap = (Element) dirRelIt2Cap.next();
                                    
                                    if (relEvalCap.getHumanType().endsWith("Association")) {
                                        Association assCap = (Association) relEvalCap;
                                        relEl2Cap = ModelHelper.getFirstMemberEnd(assCap);
                                        
                                        if (relEl2Cap.getHumanType().equals("Reference Property")) {
                                            relEl2NameCap = relEl2Cap.getHumanName().split(" ");
                                            Collection<DiagramPresentationElement> diagsCap = proj.getDiagrams();
                                            Stereotype ster2Cap = StereotypesHelper.getStereotype(proj, "VerificationCritData", prof);
                                            List presElsCap = proj.getActiveDiagram().getPresentationElements();

                                            for (int i = presElsCap.size() - 1; i >= 0; --i) {
                                                PresentationElement presElCap = (PresentationElement) presElsCap.get(i);
                                                Element elCap = presElCap.getElement();
                                                elNameCap = elCap.getHumanName().split(" ");
                                                
                                                if (elNameCap[0].equals("VerificationCritData")) {
                                                    
                                                    if (elNameCap[1].equals(relEl2NameCap[2])) {
                                                    //javax.swing.JOptionPane.showMessageDialog(null, "vrika idio onoma");
                                                        Collection<Element> elsCap = elCap.getOwnedElement();

                                                        for (Element elementCap : elsCap) {
                                                            String arrCap[] = elementCap.getHumanName().split(" ", 2);
                                                            String diagNameCap = arrCap[0];
                                                            
                                                            if (diagNameCap.equals("Diagram")) {
                                                                //javax.swing.JOptionPane.showMessageDialog(null, "vrika parametric diagram");
                                                                MyExecutionListener melCap = new MyExecutionListener(proj, prof, elCap);
                                                                SimulationManager.registerSimulationExecutionListener(melCap);
                                                                SimulationSession simSessionCap = SimulationManager.execute(elCap, true, true);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (ster.getName().equals("Device")) {
                                Collection<DirectedRelationship> dirRelDev = elem.get_directedRelationshipOfSource();
                                
                                for (java.util.Iterator dirRelItDev = dirRelDev.iterator(); dirRelItDev.hasNext();) {
                                    Element satRelDev = (Element) dirRelItDev.next();                        
                                    if (satRelDev.getHumanName().equals(sat.getName())) {
                                        Abstraction satAbsDev = (Abstraction) satRelDev;
                                        relElDev = satAbsDev.getTarget().iterator().next();
                                        //javax.swing.JOptionPane.showMessageDialog(null, relElDev.getHumanName());
                                        
                                        AnnotationManager instanceDev = AnnotationManager.getInstance();
                                        List<Annotation> listElsDev = instanceDev.getAnnotations(relElDev);
                                        List<Annotation> listSatsDev = instanceDev.getAnnotations(satAbsDev);
                                        
                                        for (Annotation annotationElsDev : listElsDev)
                                        {
                                            instanceDev.remove(annotationElsDev);
                                        }
                                        
                                        for (Annotation annotationSatsDev : listSatsDev)
                                        {
                                            instanceDev.remove(annotationSatsDev);
                                        }
                                        instanceDev.update();
                                    }
                                }
                                
                                Collection<Relationship> dirRelAssDev = elem.get_relationshipOfRelatedElement();
                                
                                for (java.util.Iterator dirRelIt2Dev = dirRelAssDev.iterator(); dirRelIt2Dev.hasNext();) {
                                    Element relEvalDev = (Element) dirRelIt2Dev.next();
                                    
                                    if (relEvalDev.getHumanType().endsWith("Association")) {
                                        Association assDev = (Association) relEvalDev;
                                        relEl2Dev = ModelHelper.getFirstMemberEnd(assDev);
                                        
                                        if (relEl2Dev.getHumanType().equals("Reference Property")) {
                                            relEl2NameDev = relEl2Dev.getHumanName().split(" ");
                                            Collection<DiagramPresentationElement> diagsDev = proj.getDiagrams();
                                            Stereotype ster2Dev = StereotypesHelper.getStereotype(proj, "VerificationCritData", prof);
                                            List presElsDev = proj.getActiveDiagram().getPresentationElements();

                                            for (int i = presElsDev.size() - 1; i >= 0; --i) {
                                                PresentationElement presElDev = (PresentationElement) presElsDev.get(i);
                                                Element elDev = presElDev.getElement();
                                                elNameDev = elDev.getHumanName().split(" ");
                                                
                                                if (elNameDev[0].equals("VerificationCritData")) {
                                                    
                                                    if (elNameDev[1].equals(relEl2NameDev[2])) {
                                                    //javax.swing.JOptionPane.showMessageDialog(null, "vrika idio onoma");
                                                        Collection<Element> elsDev = elDev.getOwnedElement();

                                                        for (Element elementDev : elsDev) {
                                                            String arrDev[] = elementDev.getHumanName().split(" ", 2);
                                                            String diagNameDev = arrDev[0];
                                                            
                                                            if (diagNameDev.equals("Diagram")) {
                                                                //javax.swing.JOptionPane.showMessageDialog(null, "vrika parametric diagram");
                                                                MyExecutionListener melDev = new MyExecutionListener(proj, prof, elDev);
                                                                SimulationManager.registerSimulationExecutionListener(melDev);
                                                                SimulationSession simSessionDev = SimulationManager.execute(elDev, true, true);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (ster.getName().equals("DataAggregator")) {
                                Collection<DirectedRelationship> dirRelAgg = elem.get_directedRelationshipOfSource();
                                
                                for (java.util.Iterator dirRelItAgg = dirRelAgg.iterator(); dirRelItAgg.hasNext();) {
                                    Element satRelAgg = (Element) dirRelItAgg.next();                        
                                    if (satRelAgg.getHumanName().equals(sat.getName())) {
                                        Abstraction satAbsAgg = (Abstraction) satRelAgg;
                                        relElAgg = satAbsAgg.getTarget().iterator().next();
                                        //javax.swing.JOptionPane.showMessageDialog(null, relElAgg.getHumanName());
                                        
                                        AnnotationManager instanceAgg = AnnotationManager.getInstance();
                                        List<Annotation> listElsAgg = instanceAgg.getAnnotations(relElAgg);
                                        List<Annotation> listSatsAgg = instanceAgg.getAnnotations(satAbsAgg);
                                        
                                        for (Annotation annotationElsAgg : listElsAgg)
                                        {
                                            instanceAgg.remove(annotationElsAgg);
                                        }
                                        
                                        for (Annotation annotationSatsAgg : listSatsAgg)
                                        {
                                            instanceAgg.remove(annotationSatsAgg);
                                        }
                                        instanceAgg.update();
                                    }
                                }
                                
                                Collection<Relationship> dirRelAssAgg = elem.get_relationshipOfRelatedElement();
                                
                                for (java.util.Iterator dirRelIt2Agg = dirRelAssAgg.iterator(); dirRelIt2Agg.hasNext();) {
                                    Element relEvalAgg = (Element) dirRelIt2Agg.next();
                                    
                                    if (relEvalAgg.getHumanType().endsWith("Association")) {
                                        Association assAgg = (Association) relEvalAgg;
                                        relEl2Agg = ModelHelper.getFirstMemberEnd(assAgg);
                                        
                                        if (relEl2Agg.getHumanType().equals("Reference Property")) {
                                            relEl2NameAgg = relEl2Agg.getHumanName().split(" ");
                                            Collection<DiagramPresentationElement> diagsAgg = proj.getDiagrams();
                                            Stereotype ster2Agg = StereotypesHelper.getStereotype(proj, "VerificationCritData", prof);
                                            List presElsAgg = proj.getActiveDiagram().getPresentationElements();

                                            for (int i = presElsAgg.size() - 1; i >= 0; --i) {
                                                PresentationElement presElAgg = (PresentationElement) presElsAgg.get(i);
                                                Element elAgg = presElAgg.getElement();
                                                elNameAgg = elAgg.getHumanName().split(" ");
                                                
                                                if (elNameAgg[0].equals("VerificationCritData")) {
                                                    
                                                    if (elNameAgg[1].equals(relEl2NameAgg[2])) {
                                                    //javax.swing.JOptionPane.showMessageDialog(null, "vrika idio onoma");
                                                        Collection<Element> elsAgg = elAgg.getOwnedElement();

                                                        for (Element elementAgg : elsAgg) {
                                                            String arrAgg[] = elementAgg.getHumanName().split(" ", 2);
                                                            String diagNameAgg = arrAgg[0];
                                                            
                                                            if (diagNameAgg.equals("Diagram")) {
                                                                //javax.swing.JOptionPane.showMessageDialog(null, "vrika parametric diagram");
                                                                MyExecutionListener melAgg = new MyExecutionListener(proj, prof, elAgg);
                                                                SimulationManager.registerSimulationExecutionListener(melAgg);
                                                                SimulationSession simSessionAgg = SimulationManager.execute(elAgg, true, true);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        SessionManager.getInstance().closeSession();
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project proj = Application.getInstance().getProject();                
                Profile remsProf = (Profile) StereotypesHelper.getProfile(proj, "REMSProfile"); 
                Diagram diag = proj.getActiveDiagram().getDiagram();   
                DiagramPresentationElement diagPresElem = proj.getDiagram(diag); 
                Stereotype sat = StereotypesHelper.getStereotype(proj, "Satisfy");

                if (remsProf != null) {
                    
                    if (getFirstSelected() != null) {                    
                        PresentationElement presElem = getFirstSelected();  

                        if (presElem.getElement() != null) {     
                            Element elem = presElem.getElement();
                            List<Stereotype> sters = StereotypesHelper.getStereotypes(elem);
                            
                            for (Stereotype ster : sters) {
                            
                                if (ster.getName().equals("Layer")) {
                                    setEnabled(true);
                                } 
                                else if (ster.getName().equals("CapExCost")) {
                                    Collection<DirectedRelationship> dirReltmp = elem.get_directedRelationshipOfSource();
                                    for (java.util.Iterator dirRelIt = dirReltmp.iterator(); dirRelIt.hasNext();) {
                                        Element satRel = (Element) dirRelIt.next();                        
                                        if (satRel.getHumanName().equals(sat.getName())) {
                                            setEnabled(true);
                                        }
                                        else {
                                            setEnabled(false);
                                        }
                                    }
                                }
                                else if (ster.getName().equals("Device")) {
                                    setEnabled(true);
                                } 
                                else if (ster.getName().equals("DataAggregator")) {
                                    setEnabled(true);
                                } 
                                else {
                                    setEnabled(false);
                                }
                            }
                        }
                    }
                }
            }
        };
        
        createReq = new DefaultDiagramAction("Synchronize human criticality", "Synchronize human criticality", null, null) {
            public void actionPerformed(ActionEvent e) { 
                final Project project = Application.getInstance().getProject();                
                Profile remsProfile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile");
                final Diagram diagram = project.getActiveDiagram().getDiagram();   
                List<Stereotype> selectedElementStereotypes = new ArrayList<Stereotype>();
                Property prop;
                Collection<Element> colEl;
                Element value;
                Element task = null;
                String[] selectedElementStereotypeName = null;
                String[] selectedElementName = null;
                String[] elmNm = null;
                String[] derSterName = null;
                List<String> listComp = new ArrayList<String>();
                List<Element> listElComp = new ArrayList<Element>();
                String[] strs;
                String[] els = null;
                String enumName = null;
                Stereotype sat = StereotypesHelper.getStereotype(project, "Satisfy");
                Stereotype derv = StereotypesHelper.getStereotype(project, "DeriveReqt");
                Stereotype all = StereotypesHelper.getStereotype(project, "Allocate");
                Stereotype gen = StereotypesHelper.getStereotype(project, "generate");
                boolean satFlag = false;
                boolean dervFlag = false;
                boolean allFlag = false;
                boolean genFlag = false;
                boolean valueTypeFlag = false;
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {     
                        Element selectedElement = presentationElement.getElement();
                        selectedElementName = selectedElement.getHumanName().split(" ");
                        selectedElementStereotypes = StereotypesHelper.getStereotypes(selectedElement);
                        
                        for (Stereotype selectedElementStereotype : selectedElementStereotypes) {
                            selectedElementStereotypeName = selectedElementStereotype.getHumanName().split(" ");
                        }
                        Collection<DirectedRelationship> selectedElementDirectedRelationships = selectedElement.get_directedRelationshipOfTarget();
                        
                        if (selectedElementDirectedRelationships.isEmpty()) {
                            messageFrame2("<html>There are no connections to this criticality!<br/>In order to synchronize it, the \"Concern\" that generates the criticality should connect to it via a \"Derive\" relationship.<br/>In addition, a \"Task\" should connect to it via a \"Satisfy\" relationship.<br/>(Check the \"Satisfies\" field in the \"Task\" specification.)</html>");
                            return;
                        }
                        else {
                            for (java.util.Iterator directedRelationshipsGenenerateIterator = selectedElementDirectedRelationships.iterator(); directedRelationshipsGenenerateIterator.hasNext();) {
                                Element taskGen = (Element) directedRelationshipsGenenerateIterator.next();
                                Stereotype sterGen = StereotypesHelper.getAppliedStereotypeByString(taskGen, "generate");

                                if (sterGen != null) {
                                    Abstraction taskGenAbs = (Abstraction) taskGen;
                                    task = taskGenAbs.getSource().iterator().next();
                                    genFlag = true;
                                    break;
                                }
                            }
                            for (java.util.Iterator dirRelSatIt = selectedElementDirectedRelationships.iterator(); dirRelSatIt.hasNext();) {
                                Element taskSat = (Element) dirRelSatIt.next();
                                
                                if (taskSat.getHumanName().equals(sat.getName())) {
                                    Abstraction taskSatAbs = (Abstraction) taskSat;
                                    task = taskSatAbs.getSource().iterator().next();
                                    satFlag = true;
                                    break;
                                }
                            }
                            if (!genFlag) {
                                final JFrame fr1 = new JFrame();
                                JPanel jpn1 = new JPanel();
                                jpn1.setLayout(new BoxLayout(jpn1, BoxLayout.PAGE_AXIS));
                                jpn1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                JPanel jpn11 = new JPanel();
                                JPanel jpn61 = new JPanel();
                                
                                jpn11.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                jpn61.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                JLabel inf1 = new JLabel("<html>There are no \"generate\" connections to this criticality!<br/>In order to synchronize it, the \"Concern\" that generates the criticality should connect to it via a \"generate\" relationship.</html>");
                                inf1.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                inf1.setIconTextGap(10);
                                jpn11.add(inf1);

                                JButton jpFin1 = new JButton("OK");
                                jpFin1.addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        fr1.setVisible(false);
                                    }
                                });
                                jpn61.add(jpFin1);

                                jpn1.add(jpn11);
                                jpn1.add(jpn61);

                                javax.swing.JOptionPane.showOptionDialog(fr1, jpn1, "Criticality connections - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                                
                                return;
                            }
                            if (satFlag) {
                                Collection<DirectedRelationship> drAll = task.get_directedRelationshipOfSource();
                                
                                for (java.util.Iterator drAllIt = drAll.iterator(); drAllIt.hasNext();) {
                                    Element taskAll = (Element) drAllIt.next();

                                    if (taskAll.getHumanName().equals(all.getName())) {
                                        allFlag = true;
                                    }
                                }
                            }
                            else {
                                final JFrame fr2 = new JFrame();
                                JPanel jpn2 = new JPanel();
                                jpn2.setLayout(new BoxLayout(jpn2, BoxLayout.PAGE_AXIS));
                                jpn2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                JPanel jpn12 = new JPanel();
                                JPanel jpn62 = new JPanel();
                                
                                jpn12.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                jpn62.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                JLabel inf2 = new JLabel("<html>There are no \"Satisfy\" connections to this criticality!<br/>In order to synchronize it, a \"Task\" should connect to it via a \"Satisfy\" relationship.<br/>(Check the \"Satisfies\" field in the \"Task\" specification.)</html>");
                                inf2.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                inf2.setIconTextGap(10);
                                jpn12.add(inf2);

                                JButton jpFin2 = new JButton("OK");
                                jpFin2.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        fr2.setVisible(false);
                                    }
                                });
                                jpn62.add(jpFin2);

                                jpn2.add(jpn12);
                                jpn2.add(jpn62);

                                javax.swing.JOptionPane.showOptionDialog(fr2, jpn2, "Criticality connections - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                                
                                return;
                            }
                            if (satFlag) {
                                
                                if (!allFlag) {
                                    final JFrame fr3 = new JFrame();
                                    JPanel jpn3 = new JPanel();
                                    jpn3.setLayout(new BoxLayout(jpn3, BoxLayout.PAGE_AXIS));
                                    jpn3.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                    JPanel jpn13 = new JPanel();
                                    JPanel jpn63 = new JPanel();
                                    
                                    jpn13.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                    jpn63.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                    JLabel inf3 = new JLabel("<html>The \"Task\" that satisfies this criticality is not allocated to a system component!<br/>In order to synchronize it, the related \"Task\" should be connected to a desired system component via an \"Allocate\" relationship.<br/>(Check the \"Allocated to\" field in the \"Task\" specification.)</html>");
                                    inf3.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                    inf3.setIconTextGap(10);
                                    jpn13.add(inf3);

                                    JButton jpFin3 = new JButton("OK");
                                    jpFin3.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            fr3.setVisible(false);
                                        }
                                    });
                                    jpn63.add(jpFin3);

                                    jpn3.add(jpn13);
                                    jpn3.add(jpn63);

                                    javax.swing.JOptionPane.showOptionDialog(fr3, jpn3, "Criticality connections - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                                    
                                    return;
                                }
                            }
                        }
                        sterHier = StereotypesHelper.getStereotypesHierarchy(selectedElement);
                        
                        for (Stereotype ster : sterHier) {
                            String[] name = ster.getHumanName().split(" ", 2);
                            
                            if (name[1].equals(selectedElementStereotypeName[1])) {
                                colEl = ster.getOwnedElement();

                                for (Element el : colEl) {
                                    
                                    if (el.getHumanName().equals("Property Derived")) {
                                        String[] propName = el.getHumanName().split(" ", 2);
                                        prop = StereotypesHelper.getPropertyByName(ster, propName[1]);
                                        ValueSpecification vs = prop.getDefaultValue();
                                        value = (Element) ModelHelper.getValueSpecificationValue(vs);
                                        derSterName = value.getHumanName().split(" ");
                                    }
                                    if (satFlag) {
                                        
                                        if (allFlag) {
                                            
                                            List<String> tagValTypes = StereotypesHelper.getStereotypePropertyValue(selectedElement, ster, "valueType");
                                            
                                            if (tagValTypes.isEmpty()) {
                                                final JFrame fr4 = new JFrame();
                                                JPanel jpn4 = new JPanel();
                                                jpn4.setLayout(new BoxLayout(jpn4, BoxLayout.PAGE_AXIS));
                                                jpn4.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                                JPanel jpn14 = new JPanel();
                                                JPanel jpn64 = new JPanel();
                                                //Subpanel 1 (details) and 2 (actions) layouts
                                                jpn14.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                                jpn64.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                                JLabel inf4 = new JLabel("<html>The \"valueType\" property of the criticality has no value!<br/>Insert the name of a complement \"ValueType\" - \"Enumeration\".<br/>If it does not exist, you have to create one.</html>");
                                                inf4.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                                inf4.setIconTextGap(10);
                                                jpn14.add(inf4);

                                                JButton jpFin4 = new JButton("OK");
                                                jpFin4.addActionListener(new ActionListener() {
                                                    public void actionPerformed(ActionEvent e) {
                                                        fr4.setVisible(false);
                                                    }
                                                });
                                                jpn64.add(jpFin4);

                                                jpn4.add(jpn14);
                                                jpn4.add(jpn64);

                                                javax.swing.JOptionPane.showOptionDialog(fr4, jpn4, "Criticality enumeration - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);

                                                return;
                                            }
                                            else {
                                                valueTypeFlag = true;
                                            }      
                                            if (valueTypeFlag) {
                                                
                                                List<String> tagValTypes2 = StereotypesHelper.getStereotypePropertyValue(selectedElement, ster, "levels");
                                                selectedLevelFromList = tagValTypes2;

                                                if (tagValTypes2.isEmpty()) {
                                                    final JFrame fr5 = new JFrame();
                                                    JPanel jpn5 = new JPanel();
                                                    jpn5.setLayout(new BoxLayout(jpn5, BoxLayout.PAGE_AXIS));
                                                    jpn5.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                                                    JPanel jpn15 = new JPanel();
                                                    JPanel jpn65 = new JPanel();
                                                    
                                                    jpn15.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                                                    jpn65.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                                                    JLabel inf5 = new JLabel("<html>The \"levels\" property of the criticality has no value!<br/>Insert a value from the complement \"ValueType\" (Enumeration).</html>");
                                                    inf5.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
                                                    inf5.setIconTextGap(10);
                                                    jpn15.add(inf5);

                                                    JButton jpFin5 = new JButton("OK");
                                                    jpFin5.addActionListener(new ActionListener() {
                                                        public void actionPerformed(ActionEvent e) {
                                                            fr5.setVisible(false);
                                                        }
                                                    });
                                                    jpn65.add(jpFin5);

                                                    jpn5.add(jpn15);
                                                    jpn5.add(jpn65);

                                                    javax.swing.JOptionPane.showOptionDialog(fr5, jpn5, "Criticality levels - Warning", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);

                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                                List<String> tagVals = StereotypesHelper.getStereotypePropertyValue(selectedElement, ster, "valueType");

                                for (String tagVal : tagVals) {
                                    enumName = tagVal;
                                }
                            }
                        }
                        SessionManager.getInstance().createSession("CreateReq");

                        Collection<DiagramPresentationElement> diagramPresentationElements = project.getDiagrams();

                        for (DiagramPresentationElement diagramPresentationElement : diagramPresentationElements) {

                            if (diagramPresentationElement.getElement().getOwner().getHumanType().equals("BPMN Process")) {
                                Collection<Element> dpeEls = diagramPresentationElement.getElement().getOwner().getOwnedElement();

                                for (Element dpeEl : dpeEls) {
                                    
                                    if (dpeEl.getHumanType().equals("Task") || dpeEl.getHumanType().equals("Manual Task") || dpeEl.getHumanType().equals("User Task") || dpeEl.getHumanType().equals("Service Task")) {
                                        Collection<DirectedRelationship> dirRel = dpeEl.get_directedRelationshipOfSource();
                                        Element relAllEl;
                                        Element relSatEl;

                                        for (java.util.Iterator dirRelIt = dirRel.iterator(); dirRelIt.hasNext();) {
                                            Element rel = (Element) dirRelIt.next();   

                                            if (rel.getHumanName().equals(sat.getName())) {
                                                Abstraction satAbs = (Abstraction) rel;
                                                relSatEl = satAbs.getTarget().iterator().next();
                                                elmNm = relSatEl.getHumanName().split(" ");

                                                if (elmNm[1].equals(selectedElementName[1])) {

                                                    for (java.util.Iterator dirRelIt2 = dirRel.iterator(); dirRelIt2.hasNext();) {
                                                        Element rel2 = (Element) dirRelIt2.next();   

                                                        if (rel2.getHumanName().equals(all.getName())) {
                                                            Abstraction allAbs = (Abstraction) rel2;
                                                            relAllEl = allAbs.getTarget().iterator().next();
                                                            listComp.add(relAllEl.getHumanName());
                                                            listElComp.add(relAllEl);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Element componentThatSatisfies;
                        
                        for (int k = 0; k<listComp.size(); k++) {
                            List presElems = project.getActiveDiagram().getPresentationElements();
                            
                            for (int i = presElems.size() - 1; i >= 0; --i) {
                                PresentationElement presElemEnum = (PresentationElement) presElems.get(i);
                                Element elemEnum = presElemEnum.getElement();
                                els = elemEnum.getHumanName().split(" ");

                                if (elemEnum.getHumanType().equals("Value Type")) {
                                    if (els.length >= 3) {
                                        if (els[2].equals(enumName)) {
                                            tmpEnumName = enumName;
                                            Collection<Element> colEls = elemEnum.getOwnedElement();

                                            for (Element colEl2 : colEls) {

                                                if (colEl2.getHumanType().equals("Enumeration Literal")) { 
                                                    strs = colEl2.getHumanName().split(" ");
                                                    userEnumNames.add(strs[2]);
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        continue;
                                    }
                                }
                            }
                            final com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class auxReq = project.getElementsFactory().createClassInstance();
                            final Stereotype sterAuxReq = StereotypesHelper.getStereotype(project, derSterName[1], remsProfile);
                            StereotypesHelper.addStereotype(auxReq, sterAuxReq);

                            try {
                                ModelElementsManager.getInstance().addElement(auxReq, project.getModel());
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            PresentationElementsManager presElemManAuxReq = PresentationElementsManager.getInstance();
                            DiagramPresentationElement diagPresElemAuxReq = project.getDiagram(diagram);
                            ShapeElement shapeAuxReq = null;

                            try {
                                shapeAuxReq = presElemManAuxReq.createShapeElement(auxReq, diagPresElemAuxReq);
                            } catch (ReadOnlyElementException ex) {

                            }
                            Rectangle boundsAuxReq = shapeAuxReq.getBounds();
                            final Element auxReq2 = (Element) auxReq;

                            ElementsFactory elemFac = Application.getInstance().getProject().getElementsFactory();
                            Abstraction absSat = elemFac.createAbstractionInstance();
                            StereotypesHelper.addStereotype(absSat, StereotypesHelper.getStereotype(project, "Satisfy"));
                            absSat.setOwner(Application.getInstance().getProject().getModel());
                            ModelHelper.setSupplierElement(absSat, listElComp.get(k));
                            ModelHelper.setClientElement(absSat, auxReq2);
                            PresentationElement supplierSat = project.getSymbolElementMap().getPresentationElement(listElComp.get(k));
                            PresentationElement clientSat = project.getSymbolElementMap().getPresentationElement(auxReq2);

                            componentThatSatisfies = listElComp.get(k);
                            
                            Abstraction absDer = elemFac.createAbstractionInstance();
                            StereotypesHelper.addStereotype(absDer, StereotypesHelper.getStereotype(project, "DeriveReqt"));
                            absDer.setOwner(Application.getInstance().getProject().getModel());
                            ModelHelper.setSupplierElement(absDer, auxReq2);
                            ModelHelper.setClientElement(absDer, selectedElement);
                            PresentationElement supplierDer = project.getSymbolElementMap().getPresentationElement(auxReq2);
                            PresentationElement clientDer = project.getSymbolElementMap().getPresentationElement(selectedElement);

                            try {
                                PathElement pathElem = (PathElement) PresentationElementsManager.getInstance().createPathElement(absDer, clientDer, supplierDer);
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                ModelElementsManager.getInstance().addElement(absDer, project.getModel());
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                            size = userEnumNames.size();
                            
                            JFrame mainFrame = new JFrame();
                            JTabbedPane mainTabbedPane = new JTabbedPane();
                            
                            JPanel panel1 = new JPanel();
                            panel1.setLayout(new BoxLayout(panel1, BoxLayout.PAGE_AXIS));
                            panel1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));

                            JPanel panel2 = new JPanel();
                            panel2.setLayout(new BoxLayout(panel2, BoxLayout.PAGE_AXIS));
                            panel2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));

                            mainTabbedPane.add("1 - System criticality", panel1);
                            mainTabbedPane.add("2 - Enumeration", panel2);
                            mainTabbedPane.setEnabledAt(1, false);
                            
                            JPanel criticalityPanel1 = new JPanel();
                            criticalityPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));

                            JLabel criticalityPanel1DetailsLabel = new JLabel("<html>The selected <b>" +selectedElementName[1] + "</b> human criticality derives<br/> a <b>" + derSterName[1] + "</b> system criticality that is satisfied by <b>" + componentThatSatisfies.getHumanName() + "</b>.<br/><br/>You can insert a name and write a decription for the derived system criticality.<br/>Its stereotype will be inserted automatically from the system.<br/>In case you leave blank text fields, a -deficient- system criticality<br/>will still be generated.</html>");
                            criticalityPanel1.add(criticalityPanel1DetailsLabel);

                            JPanel criticalityPanel2 = new JPanel();
                            criticalityPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("System criticality name"), BorderFactory.createEmptyBorder(1,1,1,1)));
                            
                            JTextField criticalityName = new JTextField("", 20);
                            criticalityPanel2.add(criticalityName);
                            
                            JPanel criticalityPanel3 = new JPanel();
                            criticalityPanel3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("System criticality description"), BorderFactory.createEmptyBorder(1,1,1,1)));

                            JTextArea criticalityDescription = new JTextArea(3, 40);
                            criticalityDescription.setLineWrap(true);
                            criticalityDescription.setWrapStyleWord(true);

                            JScrollPane criticalityPanel3DescriptionScrollPane = new JScrollPane(criticalityDescription);
                            criticalityPanel3DescriptionScrollPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
                            criticalityPanel3.add(criticalityPanel3DescriptionScrollPane);
                            
                            JPanel criticalityPanel4 = new JPanel();
                            criticalityPanel4.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Menu action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));
                            
                            JButton criticalityPanel4NextButton = new JButton("Next (Create new enumeration)");
                            criticalityPanel4NextButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    mainTabbedPane.setEnabledAt(1, true);
                                    mainTabbedPane.setSelectedIndex(1);
                                }
                            });
                            criticalityPanel4.add(criticalityPanel4NextButton);

                            JButton criticalityPanel4KeepButton = new JButton("Finish (Keep human enumeration)");
                            criticalityPanel4KeepButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    StereotypesHelper.setStereotypePropertyValue(auxReq, sterAuxReq, "valueType", tmpEnumName);
                                    StereotypesHelper.setStereotypePropertyValue(auxReq, sterAuxReq, "levels", selectedLevelFromList.get(0));
                                    mainFrame.setVisible(false);
                                }
                            });
                            criticalityPanel4.add(criticalityPanel4KeepButton);

                            JButton criticalityPanel4CancelButton = new JButton("Cancel");
                            criticalityPanel4CancelButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent ev) {
                                    mainFrame.setVisible(false);
                                }
                            });
                            criticalityPanel4.add(criticalityPanel4CancelButton);

                            panel1.add(criticalityPanel1);
                            panel1.add(criticalityPanel2);
                            panel1.add(criticalityPanel3);
                            panel1.add(criticalityPanel4);
                            
                            JPanel enumerationPanel1 = new JPanel();
                            enumerationPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));

                            JLabel enumerationPanel1DetailsLabel = new JLabel("<html>Human criticality levels can be translated into system criticality levels.</br>A generated enumeration of these desired levels will accompany the derived system criticality.</br></br>First, insert an enumeration name.</br>Then, insert a system criticality level for each human one in the list below.</br>Finally, select (just click) a desired level.</html>");
                            enumerationPanel1.add(enumerationPanel1DetailsLabel);

                            JPanel enumerationPanel2 = new JPanel();
                            enumerationPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Enumeration name"), BorderFactory.createEmptyBorder(1,1,1,1)));
                            
                            JTextField enumerationName = new JTextField("", 20);
                            enumerationPanel2.add(enumerationName);
                            
                            JPanel enumerationPanel3 = new JPanel();
                            String[] columnNames = {"Human criticality levels", "System criticality levels"};
 
                            String[][] data = new String[size][2];
                            
                            for (int i=0; i<size; i++) {
                                data[i][0] = userEnumNames.get(i);
                            }
                            
                            JTable table = new JTable(data, columnNames) {
                                public boolean isCellEditable(int row, int column) {
                                    return column == 1 ? true : false;
                                }
                            };
                            table.setPreferredScrollableViewportSize(new Dimension(400, 70));
                            table.setFillsViewportHeight(true);
                            
//                            JPanel tmpPanel = new JPanel();
//                            tmpPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Selected Value"), BorderFactory.createEmptyBorder(1,1,1,1)));
//                            JLabel label = new JLabel("...");
//                            table.getModel().addTableModelListener(new TableModelListener() {
//                                public void tableChanged(TableModelEvent tme) {
//                                    int row = tme.getFirstRow();
//                                    int column = tme.getColumn();
//                                    TableModel model = (TableModel) tme.getSource();
//                                    Object data = model.getValueAt(row, column);
//                                    label.setText((String) data);
//                                }
//                            });
                            
                            
                            JScrollPane scrollPane = new JScrollPane(table);
                            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
                            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                            scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Levels list"), BorderFactory.createEmptyBorder(20,20,20,20)));
                            
                            enumerationPanel3.add(scrollPane);
//                            tmpPanel.add(label);
                            
                            JPanel enumerationPanel4 = new JPanel();
                            enumerationPanel4.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Menu action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

                            JButton enumerationPanel4BackButton = new JButton("Back");
                            enumerationPanel4BackButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent even) {
                                    mainTabbedPane.setSelectedIndex(0);
                                }
                            });
                            enumerationPanel4.add(enumerationPanel4BackButton);

                            size = userEnumNames.size();
                           
                            JButton enumerationPanel4FinishButton = new JButton("Finish");
                            enumerationPanel4FinishButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent event) {
                                    
                                    auxReq.setName(criticalityName.getText());
                                    StereotypesHelper.setStereotypePropertyValue(auxReq2, sterAuxReq, SysMLConstants.REQUIREMENT_TEXT_TAG, criticalityDescription.getText());

                                    ArrayList newEnumerationLevelsList = new ArrayList();
                                    EnumerationLiteral[] enumerationLiteralsArray = new EnumerationLiteral[size];
                                    
                                    for(int i = 0; i<table.getModel().getRowCount(); i++)
                                    {
                                        newEnumerationLevelsList.add(table.getModel().getValueAt(i,1)); //get the all row values at column index 0 
                                        enumerationLiteralsArray[i] = project.getElementsFactory().createEnumerationLiteralInstance();
                                    }
                                    com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Enumeration criticalityEnumerationElement = project.getElementsFactory().createEnumerationInstance();
                                    Stereotype criticalityEnumerationElementStereotype = StereotypesHelper.getStereotype(project, SysMLConstants.VALUE_TYPE_STEREOTYPE, SysMLConstants.SYSML_PROFILE);
                                    StereotypesHelper.addStereotype(criticalityEnumerationElement, criticalityEnumerationElementStereotype);
                                    criticalityEnumerationElement.setName(enumerationName.getText());

                                    try {
                                        ModelElementsManager.getInstance().addElement(criticalityEnumerationElement, project.getModel());
                                    } catch (ReadOnlyElementException ex) {
                                    }
                                    PresentationElementsManager presentationElementManagerEnumeration = PresentationElementsManager.getInstance();
                                    DiagramPresentationElement diagramPresentationElementEnumeration = project.getDiagram(diagram);
                                    ShapeElement shapeElementEnumeration = null;

                                    try {
                                        shapeElementEnumeration = presentationElementManagerEnumeration.createShapeElement(criticalityEnumerationElement, diagramPresentationElementEnumeration);
                                    } catch (ReadOnlyElementException ex) {
                                        Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    Rectangle rectEnum = shapeElementEnumeration.getBounds();
                                    Element tempEnumerationElement = (Element) criticalityEnumerationElement;
                                    PresentationElement presentationElementEnumeration = project.getSymbolElementMap().getPresentationElement(tempEnumerationElement);

                                    for (int w = 0; w < size; w++) {
                                        enumerationLiteralsArray[w].setName((String) table.getModel().getValueAt(w,1));
                                        criticalityEnumerationElement.getOwnedLiteral().add(enumerationLiteralsArray[w]);

                                        try {
                                            ModelElementsManager.getInstance().addElement(enumerationLiteralsArray[w], criticalityEnumerationElement);
                                        } catch (ReadOnlyElementException ex3) {
                                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex3);
                                        }
                                        StereotypesHelper.setStereotypePropertyValue(criticalityEnumerationElement, criticalityEnumerationElementStereotype, enumerationLiteralsArray[w].getHumanName(), (String) table.getModel().getValueAt(w,1));
                                    }
//                                   
                                    int row = table.getSelectedRow();
                                    int column = table.getSelectedColumn();
                                    String o;
                                    if (table.getValueAt(row, column) != null) {
                                        o = (String) table.getValueAt(row, column);
                                        
                                        for (java.util.Iterator it = sterHier.iterator(); it.hasNext();) {
                                            Stereotype ster = (Stereotype) it.next();
                                            String[] sterHierName = ster.getHumanName().split(" ", 2);

                                            if (sterHierName[1].equals("AbstractRequirement")) {
                                                StereotypesHelper.setStereotypePropertyValue(auxReq, sterAuxReq, "valueType", criticalityEnumerationElement.getName());
                                                StereotypesHelper.setStereotypePropertyValue(auxReq, sterAuxReq, "levels", o);
                                            }
                                        }
                                    }
                                    else {
                                        javax.swing.JOptionPane.showMessageDialog(null, "You must select a desired level from the levels list (just click on desired level).");
                                        return;
                                    }
                                    mainFrame.setVisible(false);
                                }
                            });
                            enumerationPanel4.add(enumerationPanel4FinishButton);

                            JButton enumerationPanel4CancelButton = new JButton("Cancel");
                            enumerationPanel4CancelButton.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent event2) {
                                    mainFrame.setVisible(false);
                                }
                            });
                            enumerationPanel4.add(enumerationPanel4CancelButton);

                            panel2.add(enumerationPanel1);
                            panel2.add(enumerationPanel2);
                            panel2.add(enumerationPanel3);
//                            panel2.add(tmpPanel);
                            panel2.add(enumerationPanel4);

                            javax.swing.JOptionPane.showOptionDialog(mainFrame, mainTabbedPane, "Synchronize human criticality", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                            mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        
//                            JPanel oldLevelsPanel = new JPanel();
//                            String[] columnNames = {"Human criticality levels",
//                                "System criticality levels"};
// 
//                            String[][] data = new String[size][2];
//                            
//                            for (int i=0; i<size; i++) {
//                                data[i][0] = userEnumNames.get(i);
//                            }
//                            
//                            JTable table = new JTable(data, columnNames) {
//                                public boolean isCellEditable(int row, int column) {
//                                    return column == 1 ? true : false;
//                                }
//                            };
//                            table.setPreferredScrollableViewportSize(new Dimension(400, 70));
//                            table.setFillsViewportHeight(true);
//                            JTableHeader header = table.getTableHeader();
//                            header.setBackground(Color.gray);
//                            
//                            JScrollPane scrollPane = new JScrollPane(table);
//                            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
//                            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//                            scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Levels list"), BorderFactory.createEmptyBorder(20,20,20,20)));
//                            
//                            oldLevelsPanel.add(scrollPane);
//                            
//                            ArrayList newEnumerationLevelsList = new ArrayList();
//                            for(int i = 0; i<table.getModel().getRowCount(); i++)
//                            {
//                                newEnumerationLevelsList.add(table.getModel().getValueAt(i,1)); //get the all row values at column index 0
//                            }
//                            
//                            
////                            jpan.add(jpan1);
////                            jpan.add(jpan2);
////                            jpan.add(jpan3);
//////                            jpan.add(new JSeparator(SwingConstants.HORIZONTAL));
////                            jpan.add(jpan7);
////                            jpan.add(jpan4);
////                            jpan.add(jpan5);
////                            jpan.add(jpan6);
//
//                            jpan.add(oldLevelsPanel);
//                            
//                            
//                            javax.swing.JOptionPane.showOptionDialog(frame, jpan, "Synchronize human criticality and enumeration", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
//                            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                            
                            userEnumNames.removeAll(userEnumNames);
                        }
                        SessionManager.getInstance().closeSession();
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProfile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                
                if (remsProfile != null) {
                    if (getFirstSelected() != null) {                    
                        PresentationElement presentationElement = getFirstSelected();  

                        if (presentationElement.getElement() != null) {     
                            Element selectedElement = presentationElement.getElement();
                            Collection<Stereotype> selectedElementStereotypesHierarchy = StereotypesHelper.getStereotypesHierarchy(selectedElement);
                            
                            if (selectedElementStereotypesHierarchy != null) {
                                for (Stereotype selectedElementStereotypeHierarchy : selectedElementStereotypesHierarchy) {
                                    String[] selectedElementStereotypeHierarchyName = selectedElementStereotypeHierarchy.getHumanName().split(" ", 2);
                            
                                    if (selectedElementStereotypeHierarchyName[1].equals("HumanCriticality")) {
                                        setEnabled(true);
                                        break;
                                    }
                                    else {
                                        setEnabled(false);
                                    }
                                }
                            }                        
                        }
                    }
                }
            }
        };

        
        generate = new DefaultDiagramAction("Generate human criticality", "Generate human criticality", null, null) {
            @Override
            public void actionPerformed(ActionEvent e) { 
                SessionManager.getInstance().createSession("Generate");
                Project project = Application.getInstance().getProject();                
                Profile profile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile");
                Diagram diagram = project.getActiveDiagram().getDiagram(); 
                List<Stereotype> profileStereotypes = StereotypesHelper.getStereotypesByProfile(profile);
                List<Stereotype> criticalityStereotypes = new ArrayList<Stereotype>();
                List<String> enumerations = new ArrayList<String>();
                pressedOkInGenerateMethod = false;
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presentationElement = getFirstSelected();  
                    
                    if (presentationElement.getElement() != null) {     
                        Element selectedElement = presentationElement.getElement();
                        
                        JFrame mainFrame = new JFrame();
                        JTabbedPane mainTabbedPane = new JTabbedPane();
                        JPanel criticalityPanel = new JPanel();
                        criticalityPanel.setLayout(new BoxLayout(criticalityPanel, BoxLayout.PAGE_AXIS));
                        criticalityPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                        
                        JPanel enumerationPanel = new JPanel();
                        enumerationPanel.setLayout(new BoxLayout(enumerationPanel, BoxLayout.PAGE_AXIS));
                        enumerationPanel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                        
                        mainTabbedPane.add("1 - Human criticality", criticalityPanel);
                        mainTabbedPane.add("2 - Enumeration", enumerationPanel);
 
                        JPanel criticalityPanel1 = new JPanel();
                        criticalityPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JLabel criticalityPanel1DetailsLabel = new JLabel("<html>The selected element is a concern that can generate human criticalities.<br/><br/>You can choose a stereotype, insert a name and write a decription<br/>for the generated human criticality.<br/>In case you leave blank text fields, a -deficient- human criticality<br/>and a corresponding enumeration will still be generated.</html>");
                        criticalityPanel1.add(criticalityPanel1DetailsLabel);
                        
                        JPanel criticalityPanel2 = new JPanel();
                        criticalityPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Human criticality stereotype and name"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JPanel criticalityPanel2StereotypePanel = new JPanel();
                        criticalityPanel2StereotypePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Stereotype"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        JComboBox<String> criticalityPanel2StereotypesComboBox = new JComboBox<>();
                        
                        for (Stereotype profileStereotype : profileStereotypes) {
                            String[] profileStereotypeName = profileStereotype.getHumanName().split(" ", 2);
                            
                            if (profileStereotypeName[1].equals("HumanCriticality")) {
                                for(Relationship profileStereotypeRelationship: profileStereotype.get_relationshipOfRelatedElement() ) {
                                    if(profileStereotypeRelationship.getHumanType().equals("Generalization")) {
                                        Generalization profileStereotypeGeneralizationRelationship = (Generalization) profileStereotypeRelationship;
                                        String[] profileStereotypeGeneralizationRelationshipName = profileStereotypeGeneralizationRelationship.getSpecific().getHumanName().split(" ", 2);
                                        
                                        if (profileStereotypeGeneralizationRelationshipName[1].equals("HumanCriticality")) {
                                            ;;
                                        }
                                        else {
                                            criticalityStereotypes.add((Stereotype) profileStereotypeGeneralizationRelationship.getSpecific());
                                        }
                                    }     
                                }
                            }
                        }
                        for (Stereotype criticalityStereotype : criticalityStereotypes) {
                            String[] criticalityStereotypeName = criticalityStereotype.getHumanName().split(" ", 2);
                            criticalityPanel2StereotypesComboBox.addItem(criticalityStereotypeName[1]);
                        }
                        criticalityPanel2StereotypePanel.add(criticalityPanel2StereotypesComboBox);
                        
                        JPanel criticalityPanel2NamePanel = new JPanel();
                        criticalityPanel2NamePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Name"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JTextField criticalityName = new JTextField("", 20);
                        criticalityPanel2NamePanel.add(criticalityName);
                        
                        criticalityPanel2.add(criticalityPanel2StereotypePanel);
                        criticalityPanel2.add(criticalityPanel2NamePanel);
                        
                        JPanel criticalityPanel3 = new JPanel();
                        criticalityPanel3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Human criticality description"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JTextArea criticalityDescription = new JTextArea(3, 40);
                        criticalityDescription.setLineWrap(true);
                        criticalityDescription.setWrapStyleWord(true);
                        
                        JScrollPane criticalityPanel3DescriptionScrollPane = new JScrollPane(criticalityDescription);
                        criticalityPanel3DescriptionScrollPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
                        criticalityPanel3.add(criticalityPanel3DescriptionScrollPane);
                        
                        JPanel criticalityPanel4 = new JPanel();
                        criticalityPanel4.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Menu action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JButton criticalityPanel4NextButton = new JButton("Next");
                        criticalityPanel4NextButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                mainTabbedPane.setSelectedIndex(1);
                            }
                        });
                        criticalityPanel4.add(criticalityPanel4NextButton);
                        
                        JButton criticalityPanel4CancelButton = new JButton("Cancel");
                        criticalityPanel4CancelButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent ev) {
                                mainFrame.setVisible(false);
                            }
                        });
                        criticalityPanel4.add(criticalityPanel4CancelButton);
                        
                        criticalityPanel.add(criticalityPanel1);
                        criticalityPanel.add(criticalityPanel2);
                        criticalityPanel.add(criticalityPanel3);
                        criticalityPanel.add(criticalityPanel4);
                        
                        JPanel enumerationPanel1 = new JPanel();
                        enumerationPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JLabel enumerationPanel1DetailsLabel = new JLabel("<html>Each generated human criticality is accompanied by an enumeration of desired levels.<br/><br/>You can insert the enumeration name and levels.<br/>A list will be generated and populated with these levels.<br/>You can select a desired level from the list.</html>");
                        enumerationPanel1.add(enumerationPanel1DetailsLabel);
                        
                        JPanel enumerationPanel2 = new JPanel();
                        enumerationPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Enumeration name"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        JTextField enumerationName = new JTextField("", 20);
                        enumerationPanel2.add(enumerationName);
                        
                        JButton clearButton = new JButton("Clear text");
                        clearButton.setEnabled(false);
                        JTextField levels = new JTextField(14);
                        
                        levels.getDocument().addDocumentListener(new DocumentListener() {
                            public void changedUpdate(DocumentEvent e) {
                                changed();
                            }
                            public void removeUpdate(DocumentEvent e) {
                                changed();
                            }
                            public void insertUpdate(DocumentEvent e) {
                                changed();
                            }

                            public void changed() {
                               if (levels.getText().isEmpty()){
                                 clearButton.setEnabled(false);
                               }
                               else {
                                 clearButton.setEnabled(true);
                              }
                            }
                        });
                        
                        DefaultListModel listModel;
                        listModel = new DefaultListModel();
                        JButton removeButton = new JButton("Remove selected");
                        
                        JList list;
                        list = new JList(listModel);
                        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        list.setFixedCellWidth(300);
                        
                        if (list.getSelectedIndex() == -1) {
                            removeButton.setEnabled(false);
                        } else {
                            removeButton.setEnabled(true);
                        }
                        
                        list.addListSelectionListener(new ListSelectionListener() {
                            public void valueChanged(ListSelectionEvent e) {
                                if (e.getValueIsAdjusting() == false) {
 
                                    if (list.getSelectedIndex() == -1) {
                                        removeButton.setEnabled(false);
                                    } else {
                                        removeButton.setEnabled(true);
                                    }
                                }
                            }
                        });
                        
                        list.setSelectedIndex(0);
                        list.setVisibleRowCount(6);
                        JScrollPane listScrollPane = new JScrollPane(list);
                        listScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
                        listScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                        
                        removeButton.addActionListener(new ActionListener(){
                            public void actionPerformed(ActionEvent event) {
                                enumerations.remove(list.getSelectedValue().toString());
                                int index = list.getSelectedIndex();
                                listModel.remove(index);
                                int size = listModel.getSize();

                                if (size == 0) { 
                                    removeButton.setEnabled(false);
                                } else {
                                    if (index == listModel.getSize()) {
                                        index--;
                                    }
                                    list.setSelectedIndex(index);
                                }
                            }
                        });
                        
                        JButton insertButton = new JButton("Insert");
                        insertButton.addActionListener(new ActionListener(){
                            public void actionPerformed(ActionEvent even) {
                                if (levels.getText().equals("") || (listModel.contains(levels.getText()))) {
                                    levels.requestFocusInWindow();
                                    levels.selectAll();
                                    return;
                                }
                                int index = list.getSelectedIndex(); 
                                if (index == -1) { 
                                    index = 0;
                                } else {  
                                    index++;
                                }
                                listModel.addElement(levels.getText());
                                enumerations.add(levels.getText());
                                list.setSelectedIndex(index);
                                levels.requestFocusInWindow();
                                levels.setText("");
                            }
                        });
                        
                        clearButton.addActionListener(new ActionListener(){
                            public void actionPerformed(ActionEvent eve) {
                                levels.setText("");
                            }
                        });
                        
                        JPanel buttonPane = new JPanel();
                        buttonPane.add(levels);
                        buttonPane.add(Box.createHorizontalStrut(5));
                        buttonPane.add(insertButton);
                        buttonPane.add(clearButton);
                        buttonPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Insert levels"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JPanel listPane = new JPanel();
                        listPane.add(listScrollPane);
                        listPane.add(removeButton);
                        listPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Levels list"), BorderFactory.createEmptyBorder(20,20,20,20)));

                        JPanel enumerationPanel4 = new JPanel();
                        enumerationPanel4.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Menu action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));
                        
                        JButton enumerationPanel4BackButton = new JButton("Back");
                        enumerationPanel4BackButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent even) {
                                mainTabbedPane.setSelectedIndex(0);
                            }
                        });
                        enumerationPanel4.add(enumerationPanel4BackButton);
                        
                        JButton enumerationPanel4FinishButton = new JButton("Finish");
                        enumerationPanel4FinishButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent event) {
                                pressedOkInGenerateMethod = true;
                                mainFrame.setVisible(false);
                            }
                        });
                        enumerationPanel4.add(enumerationPanel4FinishButton);
                        
                        JButton enumerationPanel4CancelButton = new JButton("Cancel");
                        enumerationPanel4CancelButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent event2) {
                                mainFrame.setVisible(false);
                            }
                        });
                        enumerationPanel4.add(enumerationPanel4CancelButton);
                        
                        enumerationPanel.add(enumerationPanel1);
                        enumerationPanel.add(enumerationPanel2);
                        enumerationPanel.add(buttonPane);
                        enumerationPanel.add(listPane);
                        enumerationPanel.add(enumerationPanel4);
                        
                        
                        javax.swing.JOptionPane.showOptionDialog(mainFrame, mainTabbedPane, "Generate human criticality and enumeration", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        
                        if (pressedOkInGenerateMethod) {
                            com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class newCriticality = project.getElementsFactory().createClassInstance();
                            Stereotype newCriticalityStereotype = StereotypesHelper.getStereotype(project, String.valueOf(criticalityPanel2StereotypesComboBox.getSelectedItem()), profile);
                            StereotypesHelper.addStereotype(newCriticality, newCriticalityStereotype);
                            newCriticality.setName(criticalityName.getText());
                            StereotypesHelper.setStereotypePropertyValue(newCriticality, newCriticalityStereotype, SysMLConstants.REQUIREMENT_TEXT_TAG, criticalityDescription.getText());

                            try {
                                ModelElementsManager.getInstance().addElement(newCriticality, project.getModel());
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            PresentationElementsManager presentationElementManagerNewCriticality = PresentationElementsManager.getInstance();
                            DiagramPresentationElement diagramPresentationElementNewCriticality = project.getDiagram(diagram);
                            ShapeElement shapeNewCriticality = null;

                            try {
                                shapeNewCriticality = presentationElementManagerNewCriticality.createShapeElement(newCriticality, diagramPresentationElementNewCriticality);
                            } catch (ReadOnlyElementException ex) {

                            }
                            Rectangle boundsNewCriticality = shapeNewCriticality.getBounds();
                            Element tempCriticality = (Element) newCriticality;

                            ElementsFactory elementFactory = Application.getInstance().getProject().getElementsFactory();
                            Abstraction abstractionGenenerate = elementFactory.createAbstractionInstance();
                            StereotypesHelper.addStereotype(abstractionGenenerate, StereotypesHelper.getStereotype(project, "generate", profile));
                            abstractionGenenerate.setOwner(Application.getInstance().getProject().getModel());
                            ModelHelper.setSupplierElement(abstractionGenenerate, tempCriticality);
                            ModelHelper.setClientElement(abstractionGenenerate, selectedElement);
                            PresentationElement supplierElement = project.getSymbolElementMap().getPresentationElement(tempCriticality);
                            PresentationElement clientElement = project.getSymbolElementMap().getPresentationElement(selectedElement);

                            try {
                                PathElement pathElement = (PathElement) PresentationElementsManager.getInstance().createPathElement(abstractionGenenerate, clientElement, supplierElement);
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                ModelElementsManager.getInstance().addElement(abstractionGenenerate, project.getModel());
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Enumeration newEnumeration = project.getElementsFactory().createEnumerationInstance();
                            Stereotype newEnumerationStereotype = StereotypesHelper.getStereotype(project, SysMLConstants.VALUE_TYPE_STEREOTYPE, SysMLConstants.SYSML_PROFILE);
                            StereotypesHelper.addStereotype(newEnumeration, newEnumerationStereotype);
                            newEnumeration.setName(enumerationName.getText());
                            
                            EnumerationLiteral[] newEnumerationLiteral = new EnumerationLiteral[enumerations.size()];
                            
                            for (int k = 0; k<enumerations.size(); k++) {
                                newEnumerationLiteral[k] = project.getElementsFactory().createEnumerationLiteralInstance();
                            }
                            try {
                                ModelElementsManager.getInstance().addElement(newEnumeration, project.getModel());
                            } catch (ReadOnlyElementException ex) {
                            }
                            PresentationElementsManager presentationElementManagerNewEnumeration = PresentationElementsManager.getInstance();
                            DiagramPresentationElement diagramPresentationElementNewEnumeration = project.getDiagram(diagram);
                            ShapeElement shapeElement = null;

                            try {
                                shapeElement = presentationElementManagerNewEnumeration.createShapeElement(newEnumeration, diagramPresentationElementNewEnumeration);
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Rectangle boundsNewEnumeration = shapeElement.getBounds();
                            Element tempEnumeration = (Element) newEnumeration;
                            PresentationElement presElem = project.getSymbolElementMap().getPresentationElement(tempEnumeration);

                            for (int w = 0; w < enumerations.size(); w++) {
                                newEnumerationLiteral[w].setName(enumerations.get(w));
                                newEnumeration.getOwnedLiteral().add(newEnumerationLiteral[w]);

                                try {
                                    ModelElementsManager.getInstance().addElement(newEnumerationLiteral[w], newEnumeration);
                                } catch (ReadOnlyElementException ex3) {
                                    Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex3);
                                }
                                StereotypesHelper.setStereotypePropertyValue(newEnumeration, newEnumerationStereotype, newEnumerationLiteral[w].getHumanName(), enumerations.get(w));
                            }
                            StereotypesHelper.setStereotypePropertyValue(newCriticality, newCriticalityStereotype, "valueType", newEnumeration.getName());
                            if (list.getSelectedValue() == null) {
                                StereotypesHelper.setStereotypePropertyValue(newCriticality, newCriticalityStereotype, "levels", "");
                            }
                            else {
                                StereotypesHelper.setStereotypePropertyValue(newCriticality, newCriticalityStereotype, "levels", list.getSelectedValue().toString());
                            }
                            
                        }
                    }
                }
                SessionManager.getInstance().closeSession();
            }
            
            @Override
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProfile = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                
                if (remsProfile != null) {
                    if (getFirstSelected() != null) {                    
                        PresentationElement presentationElement = getFirstSelected();  

                        if (presentationElement.getElement() != null) {     
                            Element selectedElement = presentationElement.getElement();
                            Collection<Stereotype> selectedElementStereotypesHierarchy = StereotypesHelper.getStereotypesHierarchy(selectedElement);
                            
                            if (selectedElementStereotypesHierarchy != null) {
                                for (Stereotype selectedElementStereotypeHierarchy : selectedElementStereotypesHierarchy) {
                                    String[] selectedElementStereotypeHierarchyName = selectedElementStereotypeHierarchy.getHumanName().split(" ", 2);
                            
                                    if (selectedElementStereotypeHierarchyName[1].equals("Concern")) {
                                        setEnabled(true);
                                        break;
                                    }
                                    else {
                                        setEnabled(false);
                                    }
                                }
                            }                        
                        }
                    }
                }
            }
        };
          
        createAuxReqBlck = new DefaultDiagramAction("Validate", "Validate", null, null) {
            
            public void actionPerformed(ActionEvent e) { 
                Project proj = Application.getInstance().getProject();                
                Profile reqProf = (Profile) StereotypesHelper.getProfile(proj, "CostReqProfile");
//                Profile remsProf = (Profile) StereotypesHelper.getProfile(proj, "REMSProfile");
                Diagram diag = proj.getActiveDiagram().getDiagram();   
//                DiagramPresentationElement diagPresElem = proj.getDiagram(diag);
                Stereotype sat = StereotypesHelper.getStereotype(proj, "Satisfy");
                Stereotype ref = StereotypesHelper.getStereotype(proj, "Refine");
                Stereotype ver = StereotypesHelper.getStereotype(proj, "Verify");
                Element b = null;
                String[] bName = null;
                Element verBlock = null;
//                String[] verReqName = null;
                boolean refFlag = false;
                boolean verFlag = false;
                boolean satFlag = false;

                if (getFirstSelected() != null) {                    
                    PresentationElement presElem = getFirstSelected();  
                    
                    if (presElem.getElement() != null) {     
                        Element element = presElem.getElement();
                        String[] elemName = element.getHumanName().split(" ", 2);
//                        Collection<Stereotype> sters = StereotypesHelper.getStereotypesHierarchy(element);
                        
//                        for (Stereotype ster : sters) {
//                            String[] name = ster.getHumanName().split(" ", 2);
                            
//                            if (name[1].equals("AbstractRequirement")) {

                        Collection<DirectedRelationship> rels = element.get_directedRelationshipOfTarget();

                        for (java.util.Iterator relIter = rels.iterator(); relIter.hasNext();) {
                            Element relEl = (Element) relIter.next();

                            if (relEl.getHumanName().equals(ref.getName())) {
                                refFlag = true;
                                break;
                            }
                        }

                        if (!refFlag) {
                            messageFrame3("<html>Criticality is not connected with a VerificationCritFormula element!</html>");
                            return;
                        }
                        Collection<DirectedRelationship> rels2 = element.get_directedRelationshipOfTarget();

                        for (java.util.Iterator relIter2 = rels2.iterator(); relIter2.hasNext();) {
                            Element verEl = (Element) relIter2.next();

                            if (verEl.getHumanName().equals(ver.getName())) {
                                Abstraction absVer = (Abstraction) verEl;
                                verBlock = absVer.getSource().iterator().next();
                                String[] tmpElmName = verBlock.getHumanName().split(" ", 2);

                                if (tmpElmName[0].equals("VerificationCritData")) {
                                    messageFrame3("<html>Criticality is already connected with a VerificationCritData element!<br/>You should continue with the criticality's computation.</html>");
                                    return;
                                }
                            }
                        }
                        Collection<DirectedRelationship> rels3 = element.get_directedRelationshipOfTarget();

                        for (java.util.Iterator relIter3 = rels3.iterator(); relIter3.hasNext();) {
                            Element block = (Element) relIter3.next();

                            if (block.getHumanName().equals(sat.getName())) {
                                satFlag = true;
                                Abstraction absBlck = (Abstraction) block;
                                b = absBlck.getSource().iterator().next();
                                bName = b.getHumanName().split(" ", 2);
                            }
                        }
                        SessionManager.getInstance().createSession("Validate");
                        com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class auxBlck = proj.getElementsFactory().createClassInstance();
                        Stereotype sterAuxBlck = StereotypesHelper.getStereotype(proj, "VerificationCritData", reqProf);
                        StereotypesHelper.addStereotype(auxBlck, sterAuxBlck);
                        auxBlck.setName(elemName[1] + "_Verification");
                        
                        try {
                            ModelElementsManager.getInstance().addElement(auxBlck, proj.getModel());
                        } 
                        catch (ReadOnlyElementException ex) {
                        }
                        PresentationElementsManager presentationElementsManagerAux = PresentationElementsManager.getInstance();
                        DiagramPresentationElement diagramPresentationElementAux = proj.getDiagram(diag);
                        ShapeElement shapeAux = null;

                        try {
                            shapeAux = presentationElementsManagerAux.createShapeElement(auxBlck, diagramPresentationElementAux);
                        } 
                        catch (ReadOnlyElementException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Rectangle boundsAux = shapeAux.getBounds();
                        Element aux = (Element) auxBlck;
                        PresentationElement peAux = proj.getSymbolElementMap().getPresentationElement(aux);
                        ElementsFactory fStop = Application.getInstance().getProject().getElementsFactory();
                        Abstraction absVerStop = fStop.createAbstractionInstance();
                        StereotypesHelper.addStereotype(absVerStop, StereotypesHelper.getStereotype(proj, "Verify"));
                        absVerStop.setOwner(Application.getInstance().getProject().getModel());
                        ModelHelper.setSupplierElement(absVerStop, element);
                        ModelHelper.setClientElement(absVerStop, aux);
                        PresentationElement supplier = proj.getSymbolElementMap().getPresentationElement(element);
                        PresentationElement client = proj.getSymbolElementMap().getPresentationElement(aux);

                        try {
                            PathElement path = (PathElement) PresentationElementsManager.getInstance().createPathElement(absVerStop, client, supplier);
                        } 
                        catch (ReadOnlyElementException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        try {
                            ModelElementsManager.getInstance().addElement(absVerStop, proj.getModel());
                        } 
                        catch (ReadOnlyElementException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (satFlag) {
                            Association a = proj.getElementsFactory().createAssociationInstance();
                            StereotypesHelper.addStereotype(a, StereotypesHelper.getStereotype(proj, "Evaluate", reqProf));
                            a.setOwner(Application.getInstance().getProject().getModel());
                            ModelHelper.setSupplierElement(a, auxBlck);
                            ModelHelper.setClientElement(a, b);

                            ModelHelper.setNavigable(ModelHelper.getFirstMemberEnd(a), true);
                            ModelHelper.setNavigable(ModelHelper.getSecondMemberEnd(a), true);

                            Property firstEnd = (Property) ModelHelper.getFirstMemberEnd(a);
                            firstEnd.setName(auxBlck.getName());

                            Property secondEnd = (Property) ModelHelper.getSecondMemberEnd(a);
                            secondEnd.setName(bName[1]);

                            PresentationElement s = proj.getSymbolElementMap().getPresentationElement(auxBlck);
                            PresentationElement c = proj.getSymbolElementMap().getPresentationElement(b);

                            try {
                                PathElement path = (PathElement) PresentationElementsManager.getInstance().createPathElement(a, c, s);
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            try {
                                ModelElementsManager.getInstance().addElement(a, proj.getModel());
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        Property property = proj.getElementsFactory().createPropertyInstance();
                        property.setName("output");
                        auxBlck.getOwnedAttribute().add(property);   

                        try {
                            ModelElementsManager.getInstance().addElement(property, auxBlck);
                        } 
                        catch (ReadOnlyElementException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        property.setName("output");
                        Element typeString = ModelHelper.findElementWithPath("SysML::Libraries::PrimitiveValueTypes::String");
                        property.setType((Classifier) typeString);
                        SessionManager.getInstance().closeSession();
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProf = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                boolean isHumanReq = false;
                boolean isConcern = false;
                boolean isReq = false;

                if (remsProf != null) {
                    
                    if (getFirstSelected() != null) {                    
                        PresentationElement presElem = getFirstSelected();  

                        if (presElem.getElement() != null) {     
                            Element elem = presElem.getElement();
                            Collection<Stereotype> sterHierarchy = StereotypesHelper.getStereotypesHierarchy(elem);
                            
                            for (Stereotype sterHier : sterHierarchy) {
                                String[] sterHierName = sterHier.getHumanName().split(" ", 2);
                            
                                if (sterHierName[1].equals("AbstractRequirement")) {
                                    isReq = true;
                                    break;
                                }
                            }
                            if (isReq) {
                               
                                for (Stereotype sterHier : sterHierarchy) {
                                    String[] sterHierName = sterHier.getHumanName().split(" ", 2);

                                    if (sterHierName[1].equals("HumanCriticality")) {
                                        isHumanReq = true;
                                        break;
                                    }
                                    if (sterHierName[1].equals("Concern")) {
                                        isConcern = true;
                                        break;
                                    }
                                }
                                if (isHumanReq) {
                                    setEnabled(false);
                                }
                                else if (isConcern) {
                                    setEnabled(false);
                                }
                                else {
                                    setEnabled(true);
                                }
                            }
                            else {
                                setEnabled(false);
                            }
                        }
                    }
                }
            }
        };
        
        createParam = new DefaultDiagramAction("Compute", "Compute", null, null) {
            
            public void actionPerformed(ActionEvent e) { 
                //SessionManager.getInstance().createSession("Compute");
                Project proj = Application.getInstance().getProject();                
//                Profile rtsProf = (Profile) StereotypesHelper.getProfile(proj, "REMSProfile");
//                Profile reqProf = (Profile) StereotypesHelper.getProfile(proj, "CostReqProfile");
//                Diagram diag = proj.getActiveDiagram().getDiagram();   
//                DiagramPresentationElement diagPresElem = proj.getDiagram(diag); 
                Element verCritBlock = null;
                Element verCritFormula = null;
                Diagram diagram = null;
                boolean verFlag = false;
                boolean refFlag = false;
                boolean outputValueFlag = false;
                boolean hasDiag = false;
                ConstraintProperty prop;
                String[] ownedElemName = null;
                String[] ownedElemTypeName = null;
                Element tmpElem = null;
                
                Stereotype ver = StereotypesHelper.getStereotype(proj, "Verify");
                Stereotype ref = StereotypesHelper.getStereotype(proj, "Refine");
                
                if (getFirstSelected() != null) {                    
                    PresentationElement presElem = getFirstSelected();  
                    
                    if (presElem.getElement() != null) {     
                        Element element = presElem.getElement();
                        
                        javax.swing.JOptionPane.showMessageDialog(null, element.getHumanName());
                        //Collection<Stereotype> sters = StereotypesHelper.getStereotypesHierarchy(element);
                        
                        //for (Stereotype ster : sters) {
                            //String[] sterName = ster.getHumanName().split(" ", 2);
                            
                            //if (sterName[1].equals("AbstractRequirement")) {
                        Collection<DirectedRelationship> rels = element.get_directedRelationshipOfTarget();

                        for (java.util.Iterator iter = rels.iterator(); iter.hasNext();) {
                            Element rel = (Element) iter.next();

                            if (rel.getHumanName().equals(ver.getName())) {
                                verFlag = true;
                                Abstraction absVer = (Abstraction) rel;
                                verCritBlock = absVer.getSource().iterator().next();
                                Collection<Element> ownedElems = verCritBlock.getOwnedElement();
                                
                                for (Element ownedElem : ownedElems) {
                                    ownedElemName = ownedElem.getHumanName().split(" ", 2);
                                    ownedElemTypeName = ownedElem.getHumanType().split(" ", 2);
                                    
                                    if (ownedElemTypeName[0].equals("Diagram")) {
                                        hasDiag = true;
                                        break;
                                    }
                                    if (ownedElemName[1].equals("Property output")) {
                                        outputValueFlag = true;
                                        tmpElem = ownedElem;
                                        break;
                                    }
                                }
                                
                                if (hasDiag) {
                                    messageFrame2("<html>Connected VerificationCritData element already owns a SysML Parametric Diagram!<br/></html>");
                                    return;
                                }
                            }
                        }
                        Collection<DirectedRelationship> rels2 = element.get_directedRelationshipOfTarget();
                        for (java.util.Iterator iter2 = rels2.iterator(); iter2.hasNext();) {
                            Element rel2 = (Element) iter2.next();
                            
                            if (rel2.getHumanName().equals(ref.getName())) {
                                refFlag = true;
                                Abstraction absRef = (Abstraction) rel2;
                                verCritFormula = absRef.getSource().iterator().next();
                                Collection<Constraint> con = verCritFormula.get_constraintOfConstrainedElement();
                            }
                        }

                        if (verFlag && refFlag && outputValueFlag) {
                            SessionManager.getInstance().createSession("Compute");
                            
                            try {
                                //a class diagram is created and added to a parent model element
                                diagram = ModelElementsManager.getInstance().createDiagram("SysML Parametric Diagram", (Namespace) verCritBlock);
                                //open a diagram
                                proj.getDiagram(diagram).open();
                            }
                            catch (ReadOnlyElementException er) {
                            }
//                                Collection<Element> ownedElems = verCritBlock.getOwnedElement();
//                                
//                                for (Element ownedElem : ownedElems) {
//                                    ownedElemName = ownedElem.getHumanName().split(" ", 2);

//                                    if (ownedElemName[1].equals("Property output")) {
//                                        tmpElem = ownedElem;
                            PresentationElementsManager presentationElementsManagerAux = PresentationElementsManager.getInstance();
                            DiagramPresentationElement diagramPresentationElementAux = proj.getDiagram(diagram);
                            //gia na fanei sto diagramma
                            ShapeElement shapeAux = null;

                            try {
                                shapeAux = presentationElementsManagerAux.createShapeElement(tmpElem, diagramPresentationElementAux);
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
//                                    }
//                                }
                            //gia na fanei to function/constraint
                            Property verCritBlockProp = proj.getElementsFactory().createPropertyInstance();
                            //Stereotype sterVerBlck = StereotypesHelper.getStereotype(proj, "ConstraintProperty");
                            verCritBlockProp.setOwner(verCritBlock);
                            verCritBlockProp.setType((Classifier) verCritFormula);
                            PresentationElementsManager presentationElementsManagerAux2 = PresentationElementsManager.getInstance();
                            DiagramPresentationElement diagramPresentationElementAux2 = proj.getDiagram(diagram);
                            //gia na fanei sto diagramma
                            ShapeElement shapeAux2 = null;

                            try {
                                shapeAux2 = presentationElementsManagerAux2.createShapeElement(verCritBlockProp, diagramPresentationElementAux2);
                            } catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Port port = proj.getElementsFactory().createPortInstance(); 
                            Stereotype sterPort = StereotypesHelper.getStereotype(proj, "ConstraintParameter");
                            port.setOwner(verCritFormula);
                            String[] prtName = ownedElemName[1].split(" ");
                            port.setName(prtName[1]);
                            Element prtType = ModelHelper.findElementWithPath("SysML::Libraries::PrimitiveValueTypes::String");
                            port.setType((Classifier) prtType);
                            //prt.setVisibility(VisibilityKindEnum.PUBLIC);
                            StereotypesHelper.addStereotype(port, sterPort);

                            try {
                                ModelElementsManager.getInstance().addElement(port, verCritFormula);
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            ShapeElement shapeAux3 = null;

                            try {
                                shapeAux3 = PresentationElementsManager.getInstance().createShapeElement(port, shapeAux2);
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            //Collection<Element> verEls = verBlck.getOwnedElement();
                            //for (Element el2 : verEls) {
                                //String[] elName2 = el2.getHumanName().split(" ", 2);
                                //if (elName2[1].equals("Port output")) {
                            Connector conn = proj.getElementsFactory().createConnectorInstance();
                            //Stereotype sterConn = StereotypesHelper.getStereotype(proj, "BindingConnector");
                            conn.setOwner(verCritBlock);                                                   
                            //set parent
                            ModelHelper.setClientElement(conn, tmpElem);
                            //set Child
                            ModelHelper.setSupplierElement(conn, port);
                            ConnectorEnd end1 = ModelHelper.getSecondMemberEnd(conn);
                            Stereotype end = SysMLProfile.getInstance(conn).getNestedConnectorEnd();
                            StereotypesHelper.addStereotype(end1, end);
                            StereotypesHelper.setStereotypePropertyValue(end1, end, SysMLProfile.NESTEDCONNECTOREND_PROPERTYPATH_PROPERTY, verCritBlockProp);
                            List<ConnectorEnd> ends = conn.getEnd();
                            PresentationElement c = diagramPresentationElementAux2.findPresentationElement(ends.get(0).getRole(), null);
                            PresentationElement s = diagramPresentationElementAux2.findPresentationElement(ends.get(1).getRole(), null); 

                            try {
                                PathElement pe = PresentationElementsManager.getInstance().createPathElement(conn, c, s);
                            } 
                            catch (ReadOnlyElementException ex) {
                                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            SessionManager.getInstance().closeSession();
                        }
                        else {
                            messageFrame2("<html>Criticality may not be connected with VerificationCritData or VerificationCritFormula element!<br/>"
                                              + "If there are connections, connected VerificationCritData element may not have \"output\" property defined!</html>");
                            return;
                        }
                        verFlag = false;
                        refFlag = false;
                        outputValueFlag = false;
//                            }
//                            verFlag = false;
//                            refFlag = false;
//                            outputValueFlag = false;
                            //}
                        //} 
                    }
                }
            }
            
            public void updateState() {
                setEnabled(false);
                Project project = Application.getInstance().getProject();                
                Profile remsProf = (Profile) StereotypesHelper.getProfile(project, "REMSProfile"); 
                boolean isHumanReq = false;
                boolean isConcern = false;
                boolean isReq = false;

                if (remsProf != null) {
                    
                    if (getFirstSelected() != null) {                    
                        PresentationElement presElem = getFirstSelected();  

                        if (presElem.getElement() != null) {     
                            Element elem = presElem.getElement();
                            Collection<Stereotype> sterHierarchy = StereotypesHelper.getStereotypesHierarchy(elem);
                            
                            for (Stereotype sterHier : sterHierarchy) {
                                String[] sterHierName = sterHier.getHumanName().split(" ", 2);
                            
                                if (sterHierName[1].equals("AbstractRequirement")) {
                                    isReq = true;
                                    break;
                                }
                            }
                            if (isReq) {
                               
                                for (Stereotype sterHier : sterHierarchy) {
                                    String[] sterHierName = sterHier.getHumanName().split(" ", 2);

                                    if (sterHierName[1].equals("HumanCriticality")) {
                                        isHumanReq = true;
                                        break;
                                    }
                                    if (sterHierName[1].equals("Concern")) {
                                        isConcern = true;
                                        break;
                                    }
                                }
                                if (isHumanReq) {
                                    setEnabled(false);
                                }
                                else if (isConcern) {
                                    setEnabled(false);
                                }
                                else {
                                    setEnabled(true);
                                }
                            }
                            else {
                                setEnabled(false);
                            }
                        }
                    }
                }
            }
        };
            
        
        ActionsConfiguratorsManager.getInstance().addDiagramContextConfigurator(DiagramTypeConstants.UML_CLASS_DIAGRAM, new DiagramContextAMConfigurator() {
            public void configure(ActionsManager manager, DiagramPresentationElement diagram, PresentationElement selected[], PresentationElement requestor) {  
                ActionsCategory act = new ActionsCategory(null, "Criticality Menu");
                List actionsInCategory = act.getActions();
                manager.addCategory(act);
                act.setActions(actionsInCategory);
                act.setNested(true);
                act.addAction(generate);
                act.addAction(createReq);
                act.addAction(createAuxReqBlck);
                act.addAction(createParam);
                
                ActionsCategory act2 = new ActionsCategory(null, "Configuration Menu");
                actionsInCategory = act2.getActions();
                manager.addCategory(act2);
                act2.setActions(actionsInCategory);
                act2.setNested(true);
                act2.addAction(setProps);
                act2.addAction(configure);
                act2.addAction(verify);
                
            }            
            
            public int getPriority() {
                return AMConfigurator.MEDIUM_PRIORITY;
            }
        });
    }
    
    public class val extends MDAction {
        
        public val(String id, String name) {
            super(id, name, null, null);
        }

        public void actionPerformed(ActionEvent e) {  
            Project proj = Application.getInstance().getProject();  
            Element pack = ModelHelper.findElementWithPath("CritValSuite");
            ValidationRunData runData = new ValidationRunData((com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package) pack, true, Collections.emptyList(), Annotation.getSeverityLevel(proj, Annotation.ERROR));
            ValidationRunHelper.run(runData, ActionsID.VALIDATION);
        }
    }

    
    public class check extends MDAction {
        
        public check(String id, String name) {
            super(id, name, null, null);
        }

        public void actionPerformed(ActionEvent e) {
            Project proj = Application.getInstance().getProject();
            for (ActionsCategory ac : ActionsProvider.getInstance().getMainToolbarActions().getCategories()) {
                if (ac.getName().startsWith("Validation")) {
                    for (NMAction ac2 : ac.getActions()) {
                        String[] ac2Name = ac2.getName().split(" ", 2);
                        if (ac2Name[0].equals("Run")) {
                            ;;
                        }
                        else {   
                            Element pack = ModelHelper.findElementWithPath("CritCheckSuite");
                            ValidationRunData runData = new ValidationRunData((com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package) pack, true, Collections.emptyList(), Annotation.getSeverityLevel(proj, Annotation.WARNING));
                            ValidationRunHelper.run(runData, ActionsID.VALIDATION);
                        }
                    }
                }
            }
        }
    }

    
    public class export extends MDAction {

        public export(String id, String name) {
            super(id, name, null, null);
        }

        public void actionPerformed(ActionEvent e) { 
            SessionManager.getInstance().createSession("Export");
            Project proj = Application.getInstance().getProject();                
            Profile prof = (Profile) StereotypesHelper.getProfile(proj, "REMSProfile");  
            Diagram diag = proj.getActiveDiagram().getDiagram(); 
            DiagramPresentationElement diagPresElem = proj.getDiagram(diag);
            List presElms = proj.getActiveDiagram().getPresentationElements();
            Collection<Element> colElms;
            Property prop;
            ValueSpecification vs;
            String valStr;
            Integer valInt;
            String[] propNm;
            
            String xmlFile = "Configuration.xml";
            //String csvFile = "Configuration.csv";
            //String xsdFile = "ConfigurationSchema.xml";
            //String filesDir = "ConfigurationDir";
            String workingDirectory = System.getProperty("user.home");
            //new File(workingDirectory + File.separator + filesDir).mkdirs();
            
            try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		//root elements
		Document doc = docBuilder.newDocument();
		org.w3c.dom.Element rootElement = doc.createElement("StructuralElement");
		doc.appendChild(rootElement);
            
                for (int i = presElms.size() - 1; i >= 0; --i) {
                    PresentationElement presElm = (PresentationElement) presElms.get(i);
                    Element elm = presElm.getElement();
                    String[] elmName = elm.getHumanName().split(" ", 2);
                    if (elm.getHumanType().equals("Layer")) {
                        //structural elements
                        org.w3c.dom.Element structElement = doc.createElement("Element");
                        rootElement.appendChild(structElement);
                        //org.w3c.dom.Element element = doc.createElement(elm.getHumanType());
                        //rootElement.appendChild(element);
                        //set attribute to staff element
                        Collection<Element> partProps = elm.getOwnedElement();
                        
                        for (Element partProp : partProps) {
                            String humanType = partProp.getHumanType();
                            
                            if (humanType.equals("Part Property")) {
                                org.w3c.dom.Element parts = doc.createElement("Part");
                                propNm = partProp.getHumanName().split(" ");
                                parts.appendChild(doc.createTextNode(propNm[2]));
                                structElement.appendChild(parts);
                            }
                        }
                        
                        Attr attrId = doc.createAttribute("id");
                        attrId.setValue(elm.getID());
                        structElement.setAttributeNode(attrId);
                        Attr attrName = doc.createAttribute("name");
                        attrName.setValue(elmName[1]);
                        structElement.setAttributeNode(attrName);
                        Attr attrSter = doc.createAttribute("stereotype");
                        attrSter.setValue(elm.getHumanType());
                        structElement.setAttributeNode(attrSter);
                        //shorten way
                        //staff.setAttribute("id", "1");
                        org.w3c.dom.Element properties = doc.createElement("Property");
                        structElement.appendChild(properties);
                        colElms = elm.getOwnedElement();
                        
                        for (Element colElm : colElms) {
                            
                            if (colElm.getHumanType().equals("Value Property")) {
                                
                                String[] propName = colElm.getHumanName().split(" ");
                                // property element
                                org.w3c.dom.Element property = doc.createElement(propName[2]);
                                prop = (Property) colElm;
                                vs = prop.getDefaultValue();
                                
                                if (vs instanceof LiteralString) {
                                    valStr = ((LiteralString) vs).getValue();
                                    //javax.swing.JOptionPane.showMessageDialog(null, "value: " + val);
                                    property.appendChild(doc.createTextNode(valStr));
                                    properties.appendChild(property);
                                }
                                else if (vs instanceof LiteralInteger) {
                                    valInt = ((LiteralInteger) vs).getValue();
                                    //javax.swing.JOptionPane.showMessageDialog(null, "value: " + val);
                                    property.appendChild(doc.createTextNode(valInt.toString()));
                                    properties.appendChild(property);
                                }
                            }
                        }
                    }
                    
                    if (elm.getHumanType().equals("Device") || elm.getHumanType().equals("DataAggregator")) {
                        //structural elements
                        org.w3c.dom.Element structElement = doc.createElement("Element");
                        rootElement.appendChild(structElement);
                        //org.w3c.dom.Element element = doc.createElement(elm.getHumanType());
                        //rootElement.appendChild(element);
                        //set attribute to staff element
                        Attr attrId = doc.createAttribute("id");
                        attrId.setValue(elm.getID());
                        structElement.setAttributeNode(attrId);
                        Attr attrName = doc.createAttribute("name");
                        attrName.setValue(elmName[1]);
                        structElement.setAttributeNode(attrName);
                        Attr attrSter = doc.createAttribute("stereotype");
                        attrSter.setValue(elm.getHumanType());
                        structElement.setAttributeNode(attrSter);
                        //shorten way
                        //staff.setAttribute("id", "1");
                        org.w3c.dom.Element properties = doc.createElement("Properties");
                        structElement.appendChild(properties);
                        colElms = elm.getOwnedElement();
                        
                        for (Element colElm : colElms) {
                            
                            if (colElm.getHumanType().equals("Value Property")) {
                                String[] propName = colElm.getHumanName().split(" ");
                                //property element
                                org.w3c.dom.Element property = doc.createElement(propName[2]);
                                prop = (Property) colElm;
                                vs = prop.getDefaultValue();
                                
                                if (vs instanceof LiteralString) {
                                    valStr = ((LiteralString) vs).getValue();
                                    property.appendChild(doc.createTextNode(valStr));
                                    properties.appendChild(property);
                                }
                                else if (vs instanceof LiteralInteger) {
                                    valInt = ((LiteralInteger) vs).getValue();
                                    property.appendChild(doc.createTextNode(valInt.toString()));
                                    properties.appendChild(property);
                                }
                            }
                        }
                    }
                }
		//write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(workingDirectory + File.separator + File.separator + xmlFile));
		transformer.transform(source, result);
            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerException tfe) {
                tfe.printStackTrace();
            }
            
            final JFrame fr = new JFrame();
            //Create main panels
            JPanel jp = new JPanel();
            //Set main panels' layout
            jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
            jp.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
            //Create subpanels
            JPanel jp1 = new JPanel();
            JPanel jp2 = new JPanel();
            //Subpanel 1 (details) and 2 (actions) layouts
            jp1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Description"), BorderFactory.createEmptyBorder(1,1,1,1)));
            jp2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Menu"), BorderFactory.createEmptyBorder(1,1,1,1)));

            JLabel txt = new JLabel("<html>The current system configuration has been exported to XML.<br/>File: \"" + xmlFile + "\"<br/>Folder: \"" + workingDirectory + File.separator + "\"</html>");
            txt.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
            txt.setIconTextGap(10);
            //txt.setIcon(null);
            //Add text in subpanel 1
            jp1.add(txt);

            JButton jb = new JButton("OK");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fr.setVisible(false);
                }
            });
            JButton jb2 = new JButton("Open folder");
            jb2.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    
                    if (Desktop.isDesktopSupported()) {
                        File dir = new File(workingDirectory + File.separator);
                        try {
                            Desktop.getDesktop().open(dir);
                        } catch (IOException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    fr.setVisible(false);
                }
            });
            JButton jb3 = new JButton("Open file");
            jb3.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    
                    if (Desktop.isDesktopSupported()) {
                        File dir2 = new File(workingDirectory + File.separator + xmlFile);
                        try {
                            Desktop.getDesktop().open(dir2);
                        } catch (IOException ex) {
                            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    fr.setVisible(false);
                }
            });
            //Add buttons in subpanel 2
            jp2.add(jb);
            jp2.add(jb2);
            jp2.add(jb3);
            //Add subpanels in main panel 1
            jp.add(jp1);
            jp.add(jp2);
            javax.swing.JOptionPane.showOptionDialog(fr, jp, "Export configuration", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
            
            SessionManager.getInstance().closeSession();
        }
    }
    
    
    public class update extends MDAction {

        public update(String id, String name) {
            super(id, name, null, null);
        }

        public void actionPerformed(ActionEvent e) { 
            SessionManager.getInstance().createSession("Update");
            Project project = Application.getInstance().getProject();                
            Profile profile = (Profile) StereotypesHelper.getProfile(project, "CostReqProfile");  
            TypedElement currentConstraintElement;
            Element relatedRequirement;
            Element blockRelatedRequirement;
            List<String> propertiesAndOwners = new ArrayList<String>();
            List<Element> verificationFormulas = new ArrayList<Element>();
            List<TypedElement> constraintProperties = new ArrayList<TypedElement>();
            List<Element> requirements = new ArrayList<Element>();
            Collection<DiagramPresentationElement> projectDiagrams = project.getDiagrams();
            HashMap<String, HashSet<String>> blockToReqs = new HashMap<String, HashSet<String>>();
            HashMap<String, HashMap<String, String>> blockToLevels = new HashMap();
            Stereotype satisfyStereotype = StereotypesHelper.getStereotype(project, "Satisfy");
            List<String> configureBlocks = new ArrayList<String>();
            HashMap<String, ArrayList<String>> blocksToPropNames = null;
            HashMap<String, HashMap<String, String>> blocksToPropNamesVals = new HashMap();
            List<Element> allBlocks = new ArrayList<Element>();
//            List<String> levels = new ArrayList<String>();
            //psaxnw ola ta diagrammata
            for (DiagramPresentationElement projectDiagram : projectDiagrams) {
                //pairnw ola ta elements pou iparxoun se kathe diagramma
                Collection<Element> presentationElements = projectDiagram.getElement().getOwner().getOwnedElement();

                for (Element presentationElement : presentationElements) {
                    //pairnw ta ierarxika stereotypes apo ola ta elements
                    Collection<Stereotype> elementsStereotypesHierarchy = StereotypesHelper.getStereotypesHierarchy(presentationElement);
                    
                    for (Stereotype elementStereotype : elementsStereotypesHierarchy) {
                        String[] elementStereotypeName = elementStereotype.getHumanName().split(" ", 2);
                        //vriskw an to ierarxiko stereotype einai blocl - etsi parinw mono ta block elements
                        if (elementStereotypeName[1].endsWith("Block")) {
                            //pairnw ta elements tou kathe block element
                            Collection<Element> elementOwnedElements = presentationElement.getOwnedElement();
                            //pairnw tis sindeseis tou kathe block
                            Collection<DirectedRelationship> blockRelationships = presentationElement.get_directedRelationshipOfSource(); 

                            for (java.util.Iterator relationshipsIter = blockRelationships.iterator(); relationshipsIter.hasNext();) {
                                Element blockRelationship = (Element) relationshipsIter.next();   
                                //an iparxei satisfy sindesi tou block me requirement
                                if (blockRelationship.getHumanName().equals(satisfyStereotype.getName())) {
                                    allBlocks.add(presentationElement);
                                    Abstraction blockRelationshipAbstraction = (Abstraction) blockRelationship;
                                    //pairnw to sindedemeno requirement
                                    blockRelatedRequirement = blockRelationshipAbstraction.getTarget().iterator().next();
                                    //vazw me mikro to prwto gramma tou block, p.x. dataAggregator
                                    String blockName = presentationElement.getHumanType().substring(0, 1).toLowerCase().concat(presentationElement.getHumanType().substring(1));
                                    
                                    if (!blockToReqs.containsKey(blockName)) { 
                                        blockToReqs.put(blockName, new HashSet<String>());
                                    }
                                    if (!blockToLevels.containsKey(blockName)) { 
                                        blockToLevels.put(blockName, new HashMap());
                                    }
                                    String blockReqName = blockRelatedRequirement.getHumanName().split(" ")[1];
                                    blockToReqs.get(blockName).add(blockReqName.substring(0, 1).toLowerCase().concat(blockReqName.substring(1)));
                                    Collection<Stereotype> sterHierarchy = StereotypesHelper.getStereotypesHierarchy(blockRelatedRequirement);
                                    String constraintName = blockRelatedRequirement.getHumanName().split(" ")[1];
                                    String levelName = constraintName.substring(0, 1).toUpperCase().concat(constraintName.substring(1)).concat("Level");
                                    
                                    for (Stereotype ster : sterHierarchy) {
                                        if (StereotypesHelper.getStereotypePropertyValue(blockRelatedRequirement, ster, "levels") != null) {
                                            blockToLevels.get(blockName).put(levelName, StereotypesHelper.getStereotypePropertyValue(blockRelatedRequirement, ster, "levels").get(0).toString());
                                        }
                                    }
                                }
                            }
                            for (Element elementOwnedElement : elementOwnedElements) {
                                for (java.util.Iterator relationshipsItr = blockRelationships.iterator(); relationshipsItr.hasNext();) {
                                    Element blckRelationship = (Element) relationshipsItr.next();   
                                    //an ta elements tou block einai value properties
                                    if (blckRelationship.getHumanName().equals(satisfyStereotype.getName()) && (elementOwnedElement.getHumanType().equals("Value Property"))) {
                                        String propertiesAndOwnersString = elementOwnedElement.getOwner().getHumanType() + "." + elementOwnedElement.getHumanName().split(" ")[2];
                                        propertiesAndOwnersString = propertiesAndOwnersString.substring(0, 1).toLowerCase().concat(propertiesAndOwnersString.substring(1));
                                        propertiesAndOwners.add(propertiesAndOwnersString);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    //pairnw ta formulas kai krataw se lista
                    if (presentationElement.getHumanType().equals("VerificationCritFormula")) {
                        verificationFormulas.add(presentationElement);
                    }
                    //pairnw ta ver data kai krataw se lista
                    if (presentationElement.getHumanType().equals("VerificationCritData")) {
                        Collection<Element> ownedElements = presentationElement.getOwnedElement();
                        //pairnw to cosntraint mesa sto data
                        for (Element ownedElement : ownedElements) {
                            if (ownedElement.getHumanType().equals("Constraint Property")) {
                                currentConstraintElement = (TypedElement) ownedElement;
                                constraintProperties.add(currentConstraintElement);
                            }
                        }
                        Stereotype verifyStereotype = StereotypesHelper.getStereotype(project, "Verify");
                        Collection<DirectedRelationship> verificationDataRelationships = presentationElement.get_directedRelationshipOfSource(); 

                        for (java.util.Iterator relationshipsIterator = verificationDataRelationships.iterator(); relationshipsIterator.hasNext();) {
                            Element verificationDataRelationship = (Element) relationshipsIterator.next();   

                            if (verificationDataRelationship.getHumanName().equals(verifyStereotype.getName())) {
                                Abstraction verifyRelationshipAbstraction = (Abstraction) verificationDataRelationship;
                                relatedRequirement = verifyRelationshipAbstraction.getTarget().iterator().next();
                                requirements.add(relatedRequirement);
                            }
                        }
                    }   
                }
            }
            Set<String> propertiesAndOwnersSet = new HashSet<String>(propertiesAndOwners);
            propertiesAndOwners.clear();
            propertiesAndOwners.addAll(propertiesAndOwnersSet);         
            
            Set<Element> verificationFormulasSet = new HashSet<Element>(verificationFormulas);
            verificationFormulas.clear();
            verificationFormulas.addAll(verificationFormulasSet);

            Set<TypedElement> constraintPropertiesSet = new HashSet<TypedElement>(constraintProperties);
            constraintProperties.clear();
            constraintProperties.addAll(constraintPropertiesSet);
            
            Set<Element> requirementsSet = new HashSet<Element>(requirements);
            requirements.clear();
            requirements.addAll(requirementsSet);
            
            for (String b : blockToReqs.keySet()) {
                configureBlocks.add(b);
            }

            blocksToPropNames = createProlog(project, blockToLevels, blockToReqs, propertiesAndOwners, verificationFormulas, constraintProperties, requirements);

            for (int j=0; j< configureBlocks.size(); j++) {
                Collections.replaceAll(configureBlocks, configureBlocks.get(j), "Var" + j);
            }

            String configurePredicateString = "configure(";
            configurePredicateString += Arrays.toString(configureBlocks.toArray());

            configurePredicateString += ")";

            String questionFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_question.pl";
            FileWriter questionFileWriter = null;
            File questionFile = new File(questionFileName);
            try {
                questionFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                questionFileWriter = new FileWriter(questionFile);
            } catch (IOException ex) {
                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                questionFileWriter.write(configurePredicateString);
            } catch (IOException ex) {
                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                questionFileWriter.flush();
                questionFileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }

            ArrayList<String> cmds = new ArrayList();
            cmds.add("java");
            cmds.add("-jar");
            cmds.add("C:\\Users\\chris\\Desktop\\REMSNewProject\\PrologApp2\\dist\\PrologApp2.jar");
            ProcessBuilder pb = new ProcessBuilder(cmds);

            try {
                Process p = pb.start();
                p.waitFor();
            } catch (IOException ioex) {
                ioex.printStackTrace();
            } catch (InterruptedException iex) {
                iex.printStackTrace();
            }
            String prologResultsFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_results.pl";
            HashMap<String, ArrayList<String>> blocksToPropVals = new HashMap();
            ArrayList<String> prologResults = new ArrayList<>();
            File prologResultsFile = new File(prologResultsFileName);
            Scanner prologResultsFileReader = null;
            try {
                prologResultsFileReader = new Scanner(prologResultsFile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }
            while (prologResultsFileReader.hasNext()) {
                String prologResultLine = prologResultsFileReader.nextLine();
                prologResultLine = prologResultLine.replace("(", ",").replace(")", "");
                String[] prologResultsLineElements = prologResultLine.split(",");
                String blockName = prologResultsLineElements[0];
                blocksToPropVals.put(blockName, new ArrayList());
                for (int prologResultsLineElementsIdx=1; prologResultsLineElementsIdx < prologResultsLineElements.length; prologResultsLineElementsIdx++) {
                    blocksToPropVals.get(blockName).add(prologResultsLineElements[prologResultsLineElementsIdx].replace(" ", "").replace("'", ""));
                }
            }
            prologResultsFileReader.close();

            for (String blockName: blocksToPropNames.keySet()) {
                blocksToPropNamesVals.put(blockName, new HashMap());
                int propIdx = 0;
                for (propIdx = 0; propIdx < blocksToPropNames.get(blockName).size(); propIdx++) {
                    String propName = blocksToPropNames.get(blockName).get(propIdx);
                    String propVal = blocksToPropVals.get(blockName).get(propIdx);
                    blocksToPropNamesVals.get(blockName).put(propName, propVal);
                }
            }
            for (Element blck : allBlocks) {
                Collection<Element> prologConfSelectedElementOwnedProps = blck.getOwnedElement();
                String[] prologConfSelectedElementOwnedPropName = null;
                Property property = null;

                List<Stereotype> sters = StereotypesHelper.getStereotypes(blck);
                String sterName = sters.get(0).getName();
                String sterBlockName = sterName.substring(0, 1).toLowerCase().concat(sterName.substring(1));
                if (blocksToPropNamesVals.containsKey(sterBlockName)) {
                    for (Element prologConfSelectedElementOwnedProp : prologConfSelectedElementOwnedProps) {
                        prologConfSelectedElementOwnedPropName = prologConfSelectedElementOwnedProp.getHumanName().split(" ");
                        if (prologConfSelectedElementOwnedProp.getHumanType().equals("Value Property")) {
                            property = (Property) prologConfSelectedElementOwnedProp; 
                            String propName = property.getHumanName().split(" ")[2];
                            if (blocksToPropNamesVals.get(sterBlockName).containsKey(propName)) {
                                ValueSpecification vs = ModelHelper.createValueSpecification(project, property.getType(), blocksToPropNamesVals.get(sterBlockName).get(propName), null);
                                property.setDefaultValue(vs);
                            }
                        }
                    }
                }
            }
                    
            final JFrame frame = new JFrame();
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
            panel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));  
            JPanel panel1 = new JPanel();
            JPanel panel2 = new JPanel();
            JPanel panel3 = new JPanel();
            
            panel1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
            panel2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
            panel3.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
            
            JLabel jl1 = new JLabel("<html>The following Decision Support<br/>"
                                        + "tasks have been COMPLETED!</html>");
            jl1.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
            jl1.setIconTextGap(10);
            panel1.add(jl1);
            
            JLabel message = new JLabel("<html>\"Update Prolog predicates\"<br/>"
                                            + "\"Update Prolog rules\"<br/>"
                                            + "\"Generate Prolog input/question\"<br/>"
                                            + "\"Execute Prolog\"<br/>"
                                            + "\"Generate results\"<br/>"
                                            + "\"Integrate results to model\"<br/></html>"); 
  
            panel2.add(message);
            JLabel message2 = new JLabel("A partial solution has been returned!");
            panel2.add(message2);
            
            JButton jpOK = new JButton("OK");
            jpOK.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {       
                    frame.setVisible(false);
                }
            });
            
            panel3.add(jpOK);
            
            panel.add(panel1);
            panel.add(panel2);
            panel.add(panel3);
            javax.swing.JOptionPane.showOptionDialog(frame, panel, "Decision Support System", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
            
            SessionManager.getInstance().closeSession();
        }
    }
    
    public NMAction getSeparatedActions() {
        MDActionsCategory cat = new MDActionsCategory("", "");
        MDActionsCategory cat2 = new MDActionsCategory("Criticalities Menu", "Criticalities Menu");
        cat2.setNested(true);
        cat2.addAction(new check(null, "Validate criticalities"));
        cat2.addAction(new val(null, "Verify criticalities"));
        cat.addAction(cat2);
        cat.addAction(new update(null, "Update Prolog file"));
        cat.addAction(new export(null, "Export configuration"));
        //cat.addAction(new enterBounds(null, "Enter Bounds"));
        return cat;
    }
    
//    void remove_elements_of_type(Collection<Element> oPs, PresentationElement dPE) {
//        javax.swing.JOptionPane.showMessageDialog(null, "mpika remove");
//        SessionManager.getInstance().createSession("remove element");
//        if (!oPs.isEmpty()) {
//            for (Element ownedProp : oPs) {
//                String[] ownedPropName = ownedProp.getHumanName().split(" ");
//                javax.swing.JOptionPane.showMessageDialog(null, ownedPropName[0]);
//                if (ownedPropName.length>0) {
//                    if (ownedPropName[0].equals("Property")) {
//                        javax.swing.JOptionPane.showMessageDialog(null, "yo");
//
//                    //try {
//                        oPs.remove(ownedProp);
//                            ownedProp.dispose();
//                        //ModelElementsManager.getInstance().removeElement(ownedProp);
//                    //} catch (ReadOnlyElementException e1) {
//                        //ntStackTrace();
//                       //javax.swing.JOptionPane.showMessageDialog(null, e1.toString());
//                   // }
//                        javax.swing.JOptionPane.showMessageDialog(null, "afairesa");
//                    }
//                }
//            }
//            SessionManager.getInstance().closeSession();
//        }
//        
//    }
    
    HashMap<String, ArrayList<String>> createProlog (Project project, HashMap<String, HashMap<String, String>> blockToLevels, HashMap<String, HashSet<String>> blockToReqs, List<String> propertiesAndOwners, List<Element> verificationFormulas, List<TypedElement> constraintProperties, List<Element> requirements) {
        Element refinedRequirement;
        String prologOutput = "";
        FileWriter prologFileWriter = null;
        Map<String, HashSet<String>> multValues = new HashMap();
        HashSet<String> reqValues = new HashSet<String>();
        HashSet<String> levelValues = new HashSet<String>();
        HashMap<String, HashSet<String>> reqToReqValues = new HashMap();
        HashSet<String> levels = null;
        ArrayList<String> orderedLevels = null;
//        HashMap<String, ArrayList<String>> compPropNames = new HashMap();
//        String workingDirectory = System.getProperty("user.home");
        String prologFileName = "C:/Program Files/Cameo Systems Modeler/plugins/REMSPlugin/resources/prolog_rules.pl";
//        File prologFile = new File(workingDirectory + File.separator + File.separator + prologFileName);
        File prologFile = new File(prologFileName);
        try {
            prologFile.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            prologFileWriter = new FileWriter(prologFile);
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Element verificationFormula : verificationFormulas) {
            Stereotype refineStereotype = StereotypesHelper.getStereotype(project, "Refine");
            Collection<DirectedRelationship> verificationFormulaRelationships = verificationFormula.get_directedRelationshipOfSource(); 

            for (java.util.Iterator formulaRelationshipsIterator = verificationFormulaRelationships.iterator(); formulaRelationshipsIterator.hasNext();) {
                Element verificationFormulaRelationship = (Element) formulaRelationshipsIterator.next();   

                if (verificationFormulaRelationship.getHumanName().equals(refineStereotype.getName())) {
                    Abstraction refineRelationshipAbstraction = (Abstraction) verificationFormulaRelationship;
                    refinedRequirement = refineRelationshipAbstraction.getTarget().iterator().next();

                    for (Element requirement : requirements) {
                        if (requirement.getHumanName().equals(refinedRequirement.getHumanName())) {
                            Collection<Stereotype> sterHierarchy = StereotypesHelper.getStereotypesHierarchy(requirement);
                            
                            levels = new HashSet<String>();
                            for (Stereotype ster : sterHierarchy) {       
                                levels.addAll(new HashSet<String>(StereotypesHelper.getStereotypePropertyValue(requirement, ster, "levels")));
                            }
                            orderedLevels = new ArrayList<String>();
                            orderedLevels.addAll(levels);
                            
                            for (TypedElement constraintProperty : constraintProperties) {
                                prologOutput = "";
                                if (verificationFormula.getHumanName().equals(constraintProperty.getType().getHumanName())) {
                                    Collection<Constraint> constraints = verificationFormula.get_constraintOfConstrainedElement();

                                    for (Constraint constraint : constraints) {
                                        ValueSpecification vs = constraint.getSpecification();
//                                            javax.swing.JOptionPane.showMessageDialog(null, "constraint: " + ModelHelper.getValueSpecificationValue(vs));
                                        String constraintSpecificationString = (String) ModelHelper.getValueSpecificationValue(vs);
                                        String[] constraintSpecificationTokens = constraintSpecificationString.replace("(",",").replace(")",",").replace("\n","").replace(" ","").split(",");      
            //                            String constraintName = "req";
                                        String constraintName = requirement.getHumanName().split(" ")[1];
                                        constraintName = constraintName.substring(0, 1).toLowerCase().concat(constraintName.substring(1));
                                        String token = null;
                                        boolean newIf = false;
                                        HashSet<String> properties = new HashSet<String>();
                                        ArrayList<String> orderedProperties = new ArrayList<String>();

                                        for (int i = 0; i < constraintSpecificationTokens.length; i++) {
                                            token = constraintSpecificationTokens[i];

                                            if (token.equals("")) {
                                                continue;
                                            }
                                            if (token.startsWith("output") || (token.startsWith("if"))) {
                                                newIf = true;
                                            }
                                            else {
                                                if (newIf) {
                                                    String operator = "==";

                                                    if (token.contains("==")) {
                                                        operator = "==";
                                                    }
                                                    else if (token.contains(">=")) {
                                                        operator = ">=";
                                                    }
                                                    else if (token.contains(">")) {
                                                        operator = ">";
                                                    }
                                                    else if (token.contains("<=")) {
                                                        operator = "<=";
                                                    }
                                                    else if (token.contains("<")) {
                                                        operator = "<";
                                                    }
                                                    String[] predElements = token.split(operator);
                                                    predElements[0] = predElements[0].substring(0, 1).toUpperCase() + predElements[0].substring(1);
                                                    predElements[1] = predElements[1].substring(0, 1).toUpperCase() + predElements[1].substring(1);
                                                    properties.add(predElements[0]);
                                                    newIf = false;
                                                }
                                            }
                                        }
                                        // initialize tree
                                        int predNodeId = 0;
                                        TreeNode<HashMap<String, String>> predTree = new TreeNode<HashMap<String, String>>(new HashMap<>(), predNodeId);
                                        TreeNode<HashMap<String, String>> returnNode;
                                       // initialize tokenization condition
                                        String currentState = "if";
                                        String operator;
                                        TreeNode<HashMap<String, String>> curPredNode = predTree;
                                        HashMap<String, String> newPredHashMap;
                                        HashMap<String, String> curPredHashMap;
                                        HashMap<String, String> savePredHashMap;
                                        // initialize branchStack
                                        Stack branchStack = new Stack();
//                                            for (int i = 0; i < constraintSpecificationTokens.length; i++) {
//                                                javax.swing.JOptionPane.showMessageDialog(null, "TOKEN : " + constraintSpecificationTokens[i]);
//                                            }
                                        for (int i = 0; i < constraintSpecificationTokens.length; i++) {
                                            token = constraintSpecificationTokens[i];

                                            if (token.equals("")) {
                                                continue;
                                            }
                                            if (currentState.equals("else")) {
                                                returnNode = (TreeNode<HashMap<String, String>>) branchStack.pop();
                                                curPredNode = returnNode.children.get(0);
                                                curPredHashMap = (HashMap<String, String>) curPredNode.getData().clone();
                                                operator = curPredHashMap.get("op");
                                                if (operator.equals("==")) { 
                                                    operator = "\\==";
                                                }
                                                else if (operator.equals(">=")) { 
                                                    operator = "<";
                                                }
                                                else if (operator.equals(">")) { 
                                                    operator = "=<";
                                                }
                                                else if (operator.equals("=<")) { 
                                                    operator = ">";
                                                }
                                                else if (operator.equals("<")) {
                                                    operator = ">=";
                                                }
                                                curPredHashMap.put("op", operator);
                                                curPredHashMap.remove("Output");
                                                predNodeId += 1;
                                                returnNode.addChild(curPredHashMap, predNodeId);
//                                                    branchStack.push(returnNode.parent);
                                                curPredNode = predTree.findTreeNode(predNodeId);
                                            }
                                            if (token.startsWith("output") || (token.startsWith("if"))) {
                                                currentState = "if";
                                                continue;
                                            }
                                            if (currentState.equals("if")) {
                                                // extract operators and predicate elements
                                                operator = "==";
                                                if (token.contains("==")) {
                                                    operator = "==";
                                                }
                                                else if (token.contains(">=")) {
                                                    operator = ">=";
                                                }
                                                else if (token.contains(">")) {
                                                    operator = ">";
                                                }
                                                else if (token.contains("<=")) {
                                                    operator = "<=";
                                                }
                                                else if (token.contains("<")) {
                                                    operator = "<";
                                                }
                                                String[] predElements = token.split(operator);
                                                predElements[0] = predElements[0].substring(0, 1).toUpperCase() + predElements[0].substring(1);
                                                predElements[1] = predElements[1].substring(0, 1).toUpperCase() + predElements[1].substring(1);

                                                if (operator.equals("<=")) {
                                                    String leftSymbol = operator.substring(0, 1);
                                                    String rightSymbol = operator.substring(1, 2);
                                                    operator = rightSymbol + leftSymbol;
                                                }
                                                // construct new predicate element
                                                newPredHashMap = new HashMap<String, String>();
                                                newPredHashMap.put("left", predElements[0]);
                                                newPredHashMap.put("op", operator);
                                                newPredHashMap.put("right", predElements[1]);
                                                predNodeId += 1;
                                                curPredNode.addChild(newPredHashMap, predNodeId);
                                                branchStack.push(curPredNode);
                                                curPredNode = predTree.findTreeNode(predNodeId);
//                                                    prologOutput += "IF" + curPredNode.data + "\n";
                                                currentState = "then";
                                                continue;
                                            }
                                            if (currentState.equals("then")) {
                                                curPredHashMap = (HashMap<String, String>) curPredNode.getData().clone();
                                                curPredHashMap.put("Output", token.replace("\"", "'"));
                                                curPredNode.setData(curPredHashMap);
//                                                    prologOutput +=  "THEN" + curPredNode.data + " " + curPredNode.id + "\n";
                                                currentState = "else";
                                                continue;
                                            }
                                            if (currentState.equals("else")) {
                                                curPredHashMap = (HashMap<String, String>) curPredNode.getData().clone();
                                                curPredHashMap.put("Output", token.replace("\"", "'"));
                                                curPredNode.setData(curPredHashMap);
//                                                    prologOutput += "ELSE" + curPredNode.data + " " + curPredNode.id + "\n";
                                                currentState = "else";
                                                continue;
                                            }
                                        }
                                        orderedProperties.addAll(properties);
                                        Collections.sort(orderedProperties);
                                        HashMap<String, String> treePropToValues;
                                        ArrayList<HashMap<String, String>> pathConditions;
                                        // print predicates
                                        String exactPredPrologOutput = "";
                                        String varPredPrologOutput = "";
                                        for (int nodeIndex: predTree.elementsIndex.keySet()) {
                                            curPredNode = predTree.findTreeNode(nodeIndex);
                                            curPredHashMap = curPredNode.getData();

                                            if (curPredNode.isLeaf()) {
                                                //System.out.println("LEAF: " + curPredNode.id);
                                                savePredHashMap = curPredHashMap;
                                                ArrayList<TreeNode<HashMap<String, String>>> predecessors = curPredNode.getPredecessors();
                                                treePropToValues = new HashMap<String, String>();
                                                pathConditions = new ArrayList<HashMap<String, String>>();
                                                //System.out.println("PRED: " + predecessors);
                                                for (String prop: orderedProperties) {
                                                    treePropToValues.put(prop, null);
                                                }
                                                for (int i = predecessors.size(); i-- > 0; ) {
                                                    curPredNode = predecessors.get(i);
                                                    curPredHashMap = curPredNode.getData();
                                                    String key = curPredHashMap.get("left");
                                                    operator = curPredHashMap.get("op");
                                                    String value = curPredHashMap.get("right");
                                                    HashMap <String, String> newPathCond = new HashMap<String, String>();
                                                    newPathCond.put("left", key);
                                                    newPathCond.put("op", operator);
                                                    newPathCond.put("right", value);
                                                    pathConditions.add(newPathCond);

                                                    if (operator.equals("==")) {
                                                        treePropToValues.put(key, curPredHashMap.get("right"));
                                                    }
                                                    else if (operator.equals(">=")) {
                                                        treePropToValues.put(key, "" + (Integer.parseInt(curPredHashMap.get("right")) + 1));
                                                    }
                                                    else if (operator.equals(">")) {
                                                        treePropToValues.put(key, "" + (Integer.parseInt(curPredHashMap.get("right")) + 1));
                                                    }
                                                    else if (operator.equals("=<")) {
                                                        treePropToValues.put(key, "" + (Integer.parseInt(curPredHashMap.get("right")) - 1));
                                                    }
                                                    else if (operator.equals("<")) {
                                                        treePropToValues.put(key, "" + (Integer.parseInt(curPredHashMap.get("right")) - 1));
                                                    }
                                                    else if (operator.equals("\\==") && treePropToValues.get(key) == null) {
                                                        treePropToValues.put(key, "'NA'");
                                                    }  
                                                }
                                                // Print exact predicates
                                                exactPredPrologOutput += constraintName + "(";

                                                for (String prop: orderedProperties) {
                                                    if (treePropToValues.get(prop) == null) {
                                                        exactPredPrologOutput += "'NA', ";
                                                    }
                                                    else {
                                                        exactPredPrologOutput += treePropToValues.get(prop) + ", ";
                                                    }
                                                }
                                                exactPredPrologOutput += savePredHashMap.get("Output");
                                                exactPredPrologOutput += ").\n";

                                                // Print variable predicates
                                                String reqValue = "";
                                                varPredPrologOutput += constraintName + "(";
                                                reqValue += constraintName + "(";
                                                for (String propString : propertiesAndOwners) {
                                                    if (!multValues.containsKey(propString.split("\\.")[0])) {
                                                        multValues.put(propString.split("\\.")[0], new HashSet());
                                                    }
                                                    multValues.get(propString.split("\\.")[0]).add(propString.split("\\.")[1].substring(0, 1).toUpperCase().concat(propString.split("\\.")[1].substring(1)));
                                                }
                                                for (String prop: orderedProperties) {
                                                    for (String propString : propertiesAndOwners) {
                                                        if (prop.toLowerCase().equals(propString.split("\\.")[1].toLowerCase())) {
//                                                                varPredPrologOutput += propString.split("\\.")[0] + "(" + prop + ")" + ", ";
//                                                                reqValue += propString.split("\\.")[0] + "(" + prop + ")" + ", ";
                                                            varPredPrologOutput += prop + ", ";
                                                            reqValue += prop + ", ";
//                                                                if (!multValues.containsKey(propString.split("\\.")[0])) {
//                                                                    multValues.put(propString.split("\\.")[0], new HashSet());
//                                                                }
//                                                                multValues.get(propString.split("\\.")[0]).add(prop);
                                                            break;
                                                        }
                                                    }
                                                }

                                                String capConstraintName = constraintName.substring(0, 1).toUpperCase().concat(constraintName.substring(1));
                                                varPredPrologOutput += capConstraintName + "Level" + ") :- ";
                                                levelValues.add(capConstraintName + "Level");
                                                reqValue += capConstraintName + "Level" + ")";
                                                reqValues.add(reqValue);

                                                if (!reqToReqValues.containsKey(constraintName)) {
                                                    reqToReqValues.put(constraintName, new HashSet<String>());
                                                }
                                                reqToReqValues.get(constraintName).add(reqValue);
//                                                    int commaIndex = 0;
                                                for (HashMap<String, String> cond: pathConditions) {
                                                    for (String propString : propertiesAndOwners) {
                                                        if (cond.get("left").toLowerCase().equals(propString.split("\\.")[1].toLowerCase())) {
//                                                                varPredPrologOutput += propString.split("\\.")[0] + "(" + cond.get("left") + ")" + cond.get("op") + cond.get("right");
                                                            varPredPrologOutput += cond.get("left") + cond.get("op") + cond.get("right");
                                                            break;
                                                        }
                                                    }
//                                                        if (commaIndex < (pathConditions.size() - 1)) {
                                                    varPredPrologOutput += ", ";
//                                                        }        
//                                                        commaIndex++;
                                                }
                                                varPredPrologOutput += capConstraintName + "Level==" + savePredHashMap.get("Output");
                                                varPredPrologOutput += ".\n";
                                            }
                                        }
                                        prologOutput += exactPredPrologOutput + varPredPrologOutput;
                                        prologOutput = prologOutput.replace("\"", "'");
//                                            javax.swing.JOptionPane.showMessageDialog(null, prologOutput + "\n");
                                    }
                                    prologOutput += "\n";
                                    try {
                                        // write to the file
                                        prologFileWriter.write(prologOutput);
                                    } catch (IOException ex) {
                                        Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }                 
                        }
                    }
                }
            }           
        }
        String componentReqPredString = "";
        for (String s:multValues.keySet()) {
            componentReqPredString += s + "(";
            int propKeySetCount = 0;

            ArrayList<String> orderedMultValues = new ArrayList<String>(multValues.get(s));
            Collections.sort(orderedMultValues);
            for (String p:orderedMultValues) {
                componentReqPredString += p;
                propKeySetCount ++;

                if (propKeySetCount < multValues.get(s).size()) {
                    componentReqPredString += ", ";
                }
            }
            componentReqPredString += ")";
            componentReqPredString += " :-\n";

            int blockToReqsKeySetCount = 0;

            for (String reqS : blockToReqs.get(s)) {
                int reqsKeySetCount = 0;

                for (String val : reqToReqValues.get(reqS)) {
                    componentReqPredString += val;
                    reqsKeySetCount++;

                    if (reqsKeySetCount < reqToReqValues.get(reqS).size()) {
                        componentReqPredString += ",\n";
                    }
                }
                blockToReqsKeySetCount++;

                if (blockToReqsKeySetCount < blockToReqs.get(s).size()) {
                    componentReqPredString += ",\n";
                }
            }
            componentReqPredString += ".";
            componentReqPredString += "\n\n";
        }
        try {
            prologFileWriter.write(componentReqPredString);
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // satisfy level rules
        String satisfyLevelPredString = "";
        for (String s:multValues.keySet()) {
            satisfyLevelPredString += s + "SatisfyLevel(" + s + "(";
            int propKeySetCount = 0;

            ArrayList<String> orderedMultValues = new ArrayList<String>(multValues.get(s));
            Collections.sort(orderedMultValues);
            for (String p:orderedMultValues) {
                satisfyLevelPredString += p;
                propKeySetCount ++;

                if (propKeySetCount < multValues.get(s).size()) {
                    satisfyLevelPredString += ", ";
                }
            }
            satisfyLevelPredString += "), ";
            int blockToLevelsKeySetCount = 0;

            ArrayList<String> orderedBlockLevelNames = new ArrayList<String>(blockToLevels.get(s).keySet());
            Collections.sort(orderedBlockLevelNames);
            for (String levelS : orderedBlockLevelNames) {
                blockToLevelsKeySetCount++;
                satisfyLevelPredString += "'" + blockToLevels.get(s).get(levelS) + "'";
                if (blockToLevelsKeySetCount < blockToLevels.get(s).size()) {
                    satisfyLevelPredString += ", ";
                }
            }
            satisfyLevelPredString += ").";
            satisfyLevelPredString += "\n\n";
        }
        try {
            prologFileWriter.write(satisfyLevelPredString);
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        String configurePredicateString = "configure(";
        int confKeySetCount = 0;
        configurePredicateString += "[";

        for (String s:multValues.keySet()) {
            configurePredicateString += s + "("; 
            int propKeySetCount = 0;

            ArrayList<String> orderedMultValues = new ArrayList<String>(multValues.get(s));
            Collections.sort(orderedMultValues);
            for (String p:orderedMultValues) {
                configurePredicateString += p;
                propKeySetCount ++;

                if (propKeySetCount < multValues.get(s).size()) {
                    configurePredicateString += ", ";
                }
            }
            configurePredicateString += ")";
            confKeySetCount += 1;
            if (confKeySetCount < multValues.size()) {
                configurePredicateString += ", ";
            }

        }
        configurePredicateString += "]";

//        List<String> levelValuesList = new ArrayList<String>(levelValues); 
//        Collections.sort(levelValuesList); 
//        int levelCount = 0;
//
//        for (String l: levelValuesList) {
//            configurePredicateString += l;
//            levelCount++;
//
//            if (levelCount < levelValues.size()){
//                configurePredicateString += ", ";
//            }
//        }
        configurePredicateString += ") :-\n";
        
        int blockCount = 0;
        for (String b:blockToLevels.keySet()) {
            configurePredicateString += b + "SatisfyLevel(" + b + "(";
            int propKeySetCount = 0;
            ArrayList<String> orderedMultValues = new ArrayList<String>(multValues.get(b));
            Collections.sort(orderedMultValues);
            for (String p:orderedMultValues) {
                configurePredicateString += p;
                propKeySetCount ++;

                if (propKeySetCount < multValues.get(b).size()) {
                    configurePredicateString += ", ";
                }
            }
            configurePredicateString += "), ";
            
            int blockToLevelsKeySetCount = 0;
            ArrayList<String> orderedBlockLevelNames = new ArrayList<String>(blockToLevels.get(b).keySet());
            Collections.sort(orderedBlockLevelNames);
            for (String levelS : orderedBlockLevelNames) {
                blockToLevelsKeySetCount++;
                configurePredicateString += levelS;
                if (blockToLevelsKeySetCount < blockToLevels.get(b).size()) {
                    configurePredicateString += ", ";
                }
            }
            configurePredicateString += ")";
            blockCount++;
//            if (blockCount < blockToLevels.size()) {
            configurePredicateString += ",\n";
//            }
        }
 
        int reqValueCounter = 0;

        for (String r:reqValues) {
            configurePredicateString += r;
            reqValueCounter ++;
            if (reqValueCounter < reqValues.size()) {
                configurePredicateString += ",\n";
            }
        }
        configurePredicateString += ".";

        try {
            prologFileWriter.write(configurePredicateString);
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            prologFileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(REMSPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HashMap<String, ArrayList<String>> blocksToPropNames = new HashMap();
        for (String blockName:multValues.keySet()) {
            ArrayList<String> orderedMultValues = new ArrayList<String>(multValues.get(blockName));
            Collections.sort(orderedMultValues);
            blocksToPropNames.put(blockName, new ArrayList());
            for (String orderedMultValue: orderedMultValues) {
                blocksToPropNames.get(blockName).add(orderedMultValue.substring(0, 1).toLowerCase().concat(orderedMultValue.substring(1)));
            }
        }
        
        return blocksToPropNames;
    }

    
    void messageFrame3 (String message) {
        //create frame
        final JFrame jFrame = new JFrame();
        JPanel jpn = new JPanel();
        jpn.setLayout(new BoxLayout(jpn, BoxLayout.PAGE_AXIS));
        jpn.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn1 = new JPanel();
        JPanel jpn3 = new JPanel();

        jpn1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf = new JLabel(message);
        jpn1.add(inf);

        JButton jpFin = new JButton("OK");
        jpFin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false);
            }
        });
        jpn3.add(jpFin);

        jpn.add(jpn1);
        jpn.add(jpn3);

        //if parameter is isVal, then show respective message
        javax.swing.JOptionPane.showOptionDialog(jFrame, jpn, "Criticality-VerificationCritData connection - Details", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
    
    //methodos gia na emfanizontai ta minimata sto sxediasti
    void messageFrame2 (String message) {
        //create frame
        final JFrame jFrame = new JFrame();
        JPanel jpn = new JPanel();
        jpn.setLayout(new BoxLayout(jpn, BoxLayout.PAGE_AXIS));
        jpn.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn1 = new JPanel();
        JPanel jpn3 = new JPanel();

        jpn1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf = new JLabel(message);
        jpn1.add(inf);

        JButton jpFin = new JButton("OK");
        jpFin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false);
            }
        });
        jpn3.add(jpFin);

        jpn.add(jpn1);
        jpn.add(jpn3);

        //if parameter is isVal, then show respective message
        javax.swing.JOptionPane.showOptionDialog(jFrame, jpn, "Criticality level computation - Details", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
    
    void messageFrame (String message) {
        final JFrame fr1 = new JFrame();
        JPanel jpn1 = new JPanel();
        jpn1.setLayout(new BoxLayout(jpn1, BoxLayout.PAGE_AXIS));
        jpn1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        JPanel jpn11 = new JPanel();
        JPanel jpn61 = new JPanel();

        jpn11.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"), BorderFactory.createEmptyBorder(1,1,1,1)));
        jpn61.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Action(s)"), BorderFactory.createEmptyBorder(1,1,1,1)));

        JLabel inf1 = new JLabel(message);
        inf1.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
        inf1.setIconTextGap(10);
        jpn11.add(inf1);

        JButton jpFin1 = new JButton("OK");
        jpFin1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fr1.setVisible(false);
            }
        });
        jpn61.add(jpFin1);

        jpn1.add(jpn11);
        jpn1.add(jpn61);

        javax.swing.JOptionPane.showOptionDialog(fr1, jpn1, "Criticality connections error", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
    }
    
    // Close Method - Return
    public boolean close() {
        return true;
    }
    // isSupported Method - Return
    public boolean isSupported() {
        return true;
    }
}
