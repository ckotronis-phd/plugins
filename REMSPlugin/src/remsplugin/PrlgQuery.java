package remsplugin;


import com.ugos.jiprolog.engine.*;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;
/**
 *
 * @author Chris
 */
public class PrlgQuery implements JIPEventListener {

    /**
     * @param args the command line arguments
     */
    
    
    private int m_nQueryHandle;
    //private int m_nQueryHandle2;
    private boolean end = false;
    
    String tmp = "PrologResults.csv";
    String workingDirectory = System.getProperty("user.home");
    String tmpFilePath = "";

    // main
//    public static void main(String args[])
//    {
//    	PrlgQuery app = new PrlgQuery();
//
//        String str1 = "Frequently";
//        String str2 = "RealTime";
//        String str3 = "NonImportant";
//        try {
//            app.start(str1, str2, str3);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }

    public synchronized void start(String test1, String test2, String test3) throws IOException
    {
        // New instance of prolog engine
        JIPEngine jip = new JIPEngine();

        // add listeners
        jip.addEventListener(this);

        // consult file
        try
        {
            // consult file
            jip.consultFile("familyrelationships.pl");
        }
        catch(JIPSyntaxErrorException ex)
        {
            ex.printStackTrace();
        }

        //JIPTerm query1 = null;
        JIPTerm query2 = null;

        // parse query
        try
        {
            //query1 = jip.getTermParser().parseTerm("numberOfCores(realtime, Y)");
            String query = "configure(X, Y, Z," + "'" + test1 + "'" + ", " + "'" + test2 + "'" + ", " + "'" + test3 + "')";
            query2 = jip.getTermParser().parseTerm(query);
            //"configure(X, Y, Z,'Frequently', 'RealTime', 'NonImportant')");
        }
        catch(JIPSyntaxErrorException ex)
        {
            ex.printStackTrace();
            System.exit(0);
        }

        // open Query
        synchronized(jip)
        {
            // It's better to have the first call under syncronization
            // to avoid that listeners is called before the method
            // openQuery returns the handle.
            m_nQueryHandle = jip.openQuery(query2);
            //m_nQueryHandle2 = jip.openQuery(query2);
        }

        if(!end)
        {
        	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    // open event occurred
	@Override
    public void openNotified(JIPEvent e)
    {
        // syncronization is required to avoid that listeners is
        // called before the method openQuery returns the handle.
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("open");
            }
        }
    }

    // more event occurred
	@Override
    public void moreNotified(JIPEvent e)
    {
        // syncronization is required to avoid that listeners is
        // called before the method openQuery returns the handle.
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("more");
            }
        }
    }

    // A solution event occurred
	@Override
    public void solutionNotified(JIPEvent e)
    {
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("solution:");
                System.out.println(e.getTerm());
                
                
//                tmpFilePath = workingDirectory + File.separator + tmp;
//                File file = new File(workingDirectory + File.separator + tmp);
//                PrintWriter pw = null;
//                try {
//                    pw = new PrintWriter(file);
//                } catch (FileNotFoundException ex) {
//                    ex.printStackTrace();
//                }
//                StringBuilder sb = new StringBuilder();
                BufferedWriter out = null;
                try {
                    out = new BufferedWriter(new FileWriter(workingDirectory + File.separator + tmp));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                JIPTerm solution = e.getTerm();
                JIPVariable[] vars = solution.getVariables();
                HashSet tmpSet = new HashSet();
                
                for (JIPVariable var : vars) {
                    
                    if (!var.isAnonymous()) {
                        System.out.println(var.getName() + " = " + var.toString(e.getSource()) + " ");
                        System.out.println();
                        tmpSet.add(var.toString(e.getSource()));
//                            sb.append(var.toString(e.getSource()));
//                            sb.append("\r\n");
//                            pw.write(sb.toString());
                    }
                }
                String[] commaStrs;
                Iterator it = tmpSet.iterator(); // why capital "M"?
                while(it.hasNext()) {
                    commaStrs = it.next().toString().split("\\s*[()]\\s*");
                    for(String commaStr : commaStrs) { 
                        try {
                            out.write(commaStr);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        try {
                            out.append("\r");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                try {
                    out.close();
                    //pw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                e.getSource().nextSolution(e.getQueryHandle());

            }
        }
    }

    // A Term has been notified with notify/2
	@Override
    public void termNotified(JIPEvent e)
    {
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("term " + e.getTerm());
            }
        }
    }

    // The end has been reached because there wasn't more solutions
	@Override
    public synchronized void endNotified(JIPEvent e)
    {
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("end");

                // get the source of the query
                JIPEngine jip = e.getSource();

                // close query
                jip.closeQuery(m_nQueryHandle);
            }
        }

        // notify end
        notify();
    }

	@Override
    public synchronized void closeNotified(JIPEvent e)
    {
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("close");
            }
        }

        // notify end
        notify();
    }

    // An error (exception) has been raised up by prolog engine
	@Override
    public synchronized void errorNotified(JIPErrorEvent e)
    {
        synchronized(e.getSource())
        {
            if(m_nQueryHandle == e.getQueryHandle())
            {
                System.out.println("Error:");
                System.out.println(e.getError());

                // get the source of the query
                JIPEngine jip = e.getSource();

                // close query
                jip.closeQuery(m_nQueryHandle);
            }
        }

        // notify end
        notify();
    }
}