
package remsplugin;

import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationAction;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.utils.MDLog;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;

import java.awt.event.ActionEvent;
import java.util.Collection;

public class AnnotationSampleAction extends MDAction implements AnnotationAction {
    private Element elm;
    
    String[] options = { "OK", "Suggested Solution/s"};
    
    public AnnotationSampleAction(Element elm) {
        super("REQUIREMENT_IS_NOT_VERIFIED", "The requirement is not verified! Click for details.", null, null);
        //javax.swing.JOptionPane.showMessageDialog(null, "eimai sto annotation constructor");
        //super("ANNOTATION_SAMPLE_ACTION", "Annotation Sample Action", null, null);
        this.elm = elm;
    }

    //Executes the action.
    //@param e event caused execution.
    public void actionPerformed(ActionEvent e) {
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        
        try {
            javax.swing.JOptionPane.showMessageDialog(null, "yolo");
        }
        catch (Exception exc) {
            MDLog.getGUILog().error("", exc);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }

//    @Override
//    public void execute(Collection<com.nomagic.magicdraw.annotation.Annotation> clctn) {
//        if (clctn == null || clctn.isEmpty())
//        {
//            return;
//        }
//        SessionManager sm = SessionManager.getInstance();
//        sm.createSession("show message");
//        try {
//        }
//        catch (Exception e)
//        {
//            MDLog.getGUILog().error("execute error", e);
//            sm.cancelSession();
//        }
//        finally
//        {
//            sm.closeSession();
//        }
//    }
    
    //Executes the action on specified targets.
    //@param annotations action targets.
    public void execute(Collection<Annotation> annotations) {
        if (annotations == null || annotations.isEmpty()) {
            return;
        }
        SessionManager sm = SessionManager.getInstance();
        sm.createSession("show message");
        try {
        }
        catch (Exception e) {
            MDLog.getGUILog().error("", e);
            sm.cancelSession();
        }
        finally {
            sm.closeSession();
        }
    }
    

//    @Override
//    public boolean canExecute(Collection<com.nomagic.magicdraw.annotation.Annotation> clctn) {
//        return true;
//    }
    
    public boolean canExecute(Collection<Annotation> annotations) {
        return true;
    }
    
//    @Override
//    public void updateState()
//    {
//        setEnabled(true);
//    }
}
