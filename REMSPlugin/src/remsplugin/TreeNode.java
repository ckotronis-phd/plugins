/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remsplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

// See https://github.com/gt4dev/yet-another-tree-structure/tree/master/java/src/com/tree
public class TreeNode<T> {

    public T data;
    public int id;
    public TreeNode<T> parent;
    public List<TreeNode<T>> children;

    public boolean isRoot() {
        return parent == null;
    }
    
    public T getData() {
        return this.data;
    }
    
    public void setData(T data) {
        this.data = data;
    }

    public boolean isLeaf() {
        return children.size() == 0;
    }
    
    public ArrayList<TreeNode<T>> getPredecessors() {
        ArrayList<TreeNode<T>> predecessors = new ArrayList<TreeNode<T>>(); 
        TreeNode<T> curNode = this;
        while (!curNode.isRoot()) {
            predecessors.add(curNode);
            curNode = curNode.parent;
        }
        return predecessors;
    }

    public HashMap<Integer, TreeNode<T>> elementsIndex;

    public TreeNode(T data, int id) {
        this.data = data;
        this.id = id;
        this.children = new LinkedList<TreeNode<T>>();
        this.elementsIndex = new HashMap<Integer, TreeNode<T>>();
        this.elementsIndex.put(id, this);
    }

    public TreeNode<T> addChild(T child, int id) {
        TreeNode<T> childNode = new TreeNode<T>(child, id);
        childNode.parent = this;
        this.children.add(childNode);
        this.registerChildForSearch(childNode);
        return childNode;
    }

    public int getLevel() {
        if (this.isRoot())
            return 0;
        else
            return parent.getLevel() + 1;
    }

    private void registerChildForSearch(TreeNode<T> node) {
        elementsIndex.put(node.id, node);
        if (parent != null)
            parent.registerChildForSearch(node);
    }

    public TreeNode<T> findTreeNode(int id) {
        if (this.elementsIndex.containsKey(id)) {
            return this.elementsIndex.get(id);
        }
        return null;
    }

    @Override
    public String toString() {
        return data != null ? data.toString() : "[data null]";
    }

}
