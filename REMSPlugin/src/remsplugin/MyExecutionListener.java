
package remsplugin;

import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.annotation.Annotation;
import com.nomagic.magicdraw.annotation.AnnotationManager;
import com.nomagic.magicdraw.simulation.execution.SimulationExecution;
import com.nomagic.magicdraw.simulation.execution.SimulationExecutionListener;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Operation;
import com.nomagic.uml2.ext.magicdraw.commonbehaviors.mdbasicbehaviors.Behavior;
import fUML.Semantics.Classes.Kernel.FeatureValue;
import fUML.Semantics.Classes.Kernel.Object_;
import fUML.Semantics.Classes.Kernel.StructuredValue;
import fUML.Semantics.CommonBehaviors.BasicBehaviors.ParameterValueList;
import fUML.Semantics.CommonBehaviors.Communications.SignalInstance;
import java.util.Collection;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Profile;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Abstraction;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Association;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.DirectedRelationship;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import java.util.List;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Relationship;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;


class MyExecutionListener extends SimulationExecutionListener { //extends ExecutionAdapter {
    private List<NMAction> actions;
    Project proj;
    Profile prof;
    Element currEl;
    Property prop; 
    Element reqSourceEl;
    Set<String> seenEls;
    
    public MyExecutionListener(Project proj, Profile prof, Element reqSourceEl) {
        this.proj = proj;
        this.prof = prof;
        this.prop = null;
        this.reqSourceEl = reqSourceEl;
        this.seenEls = new HashSet<String>();
    }

    @Override
    public void configLoaded(Element elmnt) {
        
    }

    @Override
    public void executionStarted(SimulationExecution se) {
        
    }

    @Override
    public void elementActivated(Element elmnt, Collection<?> clctn) {
        String currTargetVal = null;
        String currSourceVal = null;
        Element reqTargetEl = null;
        Abstraction verAbs = null;
        Abstraction satAbs = null;
        Annotation annotation1 = null;
        String[] critName = null;
        
        //javax.swing.JOptionPane.showMessageDialog(null, elmnt.getHumanName());
        
        if (elmnt.getHumanName().equals("Value Property output") && !this.seenEls.contains(elmnt.getHumanName())) {
            //javax.swing.JOptionPane.showMessageDialog(null, "vrika output!!!!");
            Property elmntProp = (Property) elmnt;
            
            for(Object value : clctn) {
                ValueSpecification vS = ModelHelper.createValueSpecification(this.proj, elmntProp.getType(), value.toString(), null);
                elmntProp.setDefaultValue(vS);
                currSourceVal = value.toString();
            }
            this.prop = elmntProp;
            
            Stereotype ver = StereotypesHelper.getStereotype(proj, "Verify");
            Stereotype sat = StereotypesHelper.getStereotype(proj, "Satisfy");
            List<String> tagList = null;
            Collection<DirectedRelationship> dirRel = this.reqSourceEl.get_directedRelationshipOfSource();
            
            for (java.util.Iterator dirRelIt = dirRel.iterator(); dirRelIt.hasNext();) {
                Element verRel = (Element) dirRelIt.next(); 
                
                if (verRel.getHumanName().equals(ver.getName())) {
                    verAbs = (Abstraction) verRel;
                    reqTargetEl = verAbs.getTarget().iterator().next();
                    critName = reqTargetEl.getHumanName().split(" ", 2);
                    Collection<Stereotype> sters = StereotypesHelper.getStereotypesHierarchy(reqTargetEl);
                    
                    for (Stereotype parentSter : sters) {
                        if (parentSter.getName().equals("GradedQuantReq")) {
                            tagList = StereotypesHelper.getStereotypePropertyValue(reqTargetEl, parentSter, "levels");
                            break;
                        }
                    }
                    
                    for (String tag : tagList) {
                        //javax.swing.JOptionPane.showMessageDialog(null, "my tag is " + tag);
                        currTargetVal = tag;
                    }
                    Collection<DirectedRelationship> dirSatRel = reqTargetEl.get_directedRelationshipOfTarget();
                    
                    for (java.util.Iterator dirSatRelIt = dirSatRel.iterator(); dirSatRelIt.hasNext();) {
                        Element satRel = (Element) dirSatRelIt.next(); 
                        
                        if (satRel.getHumanName().equals(sat.getName())) {
                            satAbs = (Abstraction) satRel;
                            annotation1 = new Annotation(Annotation.getSeverityLevel(this.proj, Annotation.ERROR), "annotation", "kokkinizei to satisfy", satAbs);
                        }
                    }
                }
            }
            //javax.swing.JOptionPane.showMessageDialog(null, "my prop is " + currSourceVal);

            if (!currTargetVal.equals(currSourceVal)) {
                //javax.swing.JOptionPane.showMessageDialog(null, "den einai ok oi duo times!!");
                
//                final JFrame frame = new JFrame();
//                //Create main panels
//                JPanel jp = new JPanel();
//                //Set main panels' layout
//                jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
//                jp.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                //Create subpanels
//                JPanel jp1 = new JPanel();
//                JPanel jp2 = new JPanel();
//                JPanel jp3 = new JPanel();
//                //Subpanel 1 (details) and 2 (actions) layouts
//                jp1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                jp2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                jp3.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//
//                JLabel txt = new JLabel("<html>The <strong>" + critName[1] + "</strong> criticality <strong>is not</strong> verified!<br/>Desired and real criticality levels are depicted below.</html>");
//                txt.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
//                txt.setIconTextGap(10);
//                //txt.setIcon(null);
//                //Add text in subpanel 1
//                jp1.add(txt);
//
//                JLabel targetTxt = new JLabel("\"" + currTargetVal + "\"");
//                JLabel sourceTxt = new JLabel("\"" + currSourceVal + "\"");
//                
////                final ButtonGroup groupLoSG = new ButtonGroup();
////                groupLoSG.add(LoSAG);
////                groupLoSG.add(LoSBG);
////                groupLoSG.add(LoSCG);
////                groupLoSG.add(LoSDG);
////                groupLoSG.add(LoSEG);
////                groupLoSG.add(LoSFG);
//
//                Box bx = new Box(BoxLayout.X_AXIS);
//                bx.setBorder(BorderFactory.createTitledBorder(null, "Desired", TitledBorder.CENTER, TitledBorder.TOP));
//
//                //bx.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                bx.add(targetTxt);
//                
//                jp2.add(bx);
//                
//                jp2.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                
//                Box bx2 = new Box(BoxLayout.X_AXIS);
//                bx2.setBorder(BorderFactory.createTitledBorder(null, "Real", TitledBorder.CENTER, TitledBorder.TOP));
//
//                //bx.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                bx2.add(sourceTxt);
//                
//                jp2.add(bx2);
//
//                JButton jpFinish = new JButton("OK");
//                jpFinish.addActionListener(new ActionListener() {
//                    public void actionPerformed(ActionEvent e) {
//                        frame.setVisible(false);
//                    }
//                });
//                //Add buttons in subpanel 2
//                jp3.add(jpFinish);
//                //Add subpanels in main panel 1
//                jp.add(jp1);
//                jp.add(jp2);
//                jp.add(jp3);
//
//                javax.swing.JOptionPane.showOptionDialog(frame, jp, "Criticality evaluation", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                            
                AnnotationManager manager = AnnotationManager.getInstance();
                Annotation annotation2 = new Annotation(Annotation.getSeverityLevel(this.proj, Annotation.ERROR), "sample", "iparxei lathos", reqTargetEl, Collections.singletonList(new AnnotationSampleAction(reqTargetEl)));
                manager.add(annotation1);
                manager.add(annotation2);
                manager.update();
            }
            else {
                //javax.swing.JOptionPane.showMessageDialog(null, "ola poppa!!!!");
//                final JFrame frame = new JFrame();
//                //Create main panels
//                JPanel jp = new JPanel();
//                //Set main panels' layout
//                jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
//                jp.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                //Create subpanels
//                JPanel jp1 = new JPanel();
//                JPanel jp2 = new JPanel();
//                JPanel jp3 = new JPanel();
//                //Subpanel 1 (details) and 2 (actions) layouts
//                jp1.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                jp2.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//                jp3.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
//
//                JLabel txt = new JLabel("<html>The <strong>" + critName[1] + "</strong> criticality <strong>is</strong> verified!<br/>Desired and real criticality levels are depicted below.</html>");
//                txt.setIcon(javax.swing.UIManager.getIcon("OptionPane.warningIcon"));
//                txt.setIconTextGap(10);
//                //txt.setIcon(null);
//                //Add text in subpanel 1
//                jp1.add(txt);
//
//                JLabel targetTxt = new JLabel("\"" + currTargetVal + "\"");
//                JLabel sourceTxt = new JLabel("\"" + currSourceVal + "\"");
//                
////                final ButtonGroup groupLoSG = new ButtonGroup();
////                groupLoSG.add(LoSAG);
////                groupLoSG.add(LoSBG);
////                groupLoSG.add(LoSCG);
////                groupLoSG.add(LoSDG);
////                groupLoSG.add(LoSEG);
////                groupLoSG.add(LoSFG);
//
//                Box bx = new Box(BoxLayout.X_AXIS);
//                bx.setBorder(BorderFactory.createTitledBorder(null, "Desired", TitledBorder.CENTER, TitledBorder.TOP));
//
//                //bx.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                bx.add(targetTxt);
//                
//                jp2.add(bx);
//                
//                jp2.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                
//                Box bx2 = new Box(BoxLayout.X_AXIS);
//                bx2.setBorder(BorderFactory.createTitledBorder(null, "Real", TitledBorder.CENTER, TitledBorder.TOP));
//
//                //bx.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
//                bx2.add(sourceTxt);
//                
//                jp2.add(bx2);
//
//                JButton jpFinish = new JButton("OK");
//                jpFinish.addActionListener(new ActionListener() {
//                    public void actionPerformed(ActionEvent e) {
//                        frame.setVisible(false);
//                    }
//                });
//                //Add buttons in subpanel 2
//                jp3.add(jpFinish);
//                //Add subpanels in main panel 1
//                jp.add(jp1);
//                jp.add(jp2);
//                jp.add(jp3);
//
//                javax.swing.JOptionPane.showOptionDialog(frame, jp, "Criticality evaluation", JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
                
            }
        }
        this.seenEls.add(elmnt.getHumanName());
    }

   @Override
    public void elementDeactivated(Element elmnt, Collection<?> clctn) {
        
    }

    @Override
    public void eventTriggered(SignalInstance si) {
        
    }

    @Override
    public void operationCalled(Operation oprtn, ParameterValueList pvl, Object_ objct_, Object_ objct_1, boolean bln) {
        
    }

    @Override
    public void behaviorCalled(Behavior bhvr, ParameterValueList pvl, Object_ objct_, Object_ objct_1, boolean bln) {
        
    }

    @Override
    public void objectCreated(Object_ objct_, Object_ objct_1) {
        
    }

    @Override
    public void executionTerminated(SimulationExecution se) {
   
    }

    @Override
    public void valueChange(StructuredValue sv, FeatureValue fv, Object o, Object o1) {
        
    }

    @Override
    public void busyStatusChange(StructuredValue sv, Object o, Object o1) {
        
    }
}
